            <section class="padHead organization">
                <div class="banner-org">
                    <div class="inner">
                        <h2><?php echo $getOrg['organization_name'] ?></h2>
                        <div class="date">Year of establishment: <?php echo $getOrg['year_established'] ?></div>
                        <div class="wiggle-white"></div>
                        <div class="banner-logo"><img src="<?php echo HOST.$getOrg['organization_photo'] ?>" width="203px" height="201px" /></div>
                    </div>
                </div>

                <div class="inner org">
                    <div class="pad over">
                        <div class="left">
                            <div class="buttons">
                                <a href="#">Education and Employment</a>
                                <a href="#">Private Sector</a>
                                <a href="#">Philippines</a>
                            </div>
                            <img src="<?php echo TEMPLATE_DOMAIN_IMAGES; ?>org-banner.png" />

                            <div class="padNe">

                                <p class="content-title">About the Organization</p>
                                <p><?php echo nl2br($getOrg['organization_description']) ?></p>

                                <p class="content-title">Mission and Vision</p>
                                <p><?php echo nl2br($getOrg['mission_vision']) ?></p>

                                <p class="content-title">More Information</p>
                                <p><?php echo nl2br($getOrg['more_information']) ?></p>

                            </div>
                        </div>

                        <div class="right">
                            <ul>
                                <li>
                                    <?php echo $getOrg['contact_person'] ?><br />
                                    <?php echo $getOrg['position'] ?>
                                </li>
                                <li>
                                    organization@info.com
                                </li>
                                <li>
                                    <?php echo $getOrg['phone_number'] ?>
                                </li>
                                <li>
                                    <a href="#"><?php echo $getOrg['website'] ?></a><br /><br />
                                    <a href="#"><i class="fa fa-facebook"></i>facebook/orgname</a><br /><br />
                                    <a href="#"><i class="fa fa-twitter"></i>twitter/orgname</a>
                                </li>
                                <li class="over">
                                    <span class="download"></span>
                                    <a class="downloadTxt">Download</a>
                                </li>
                            </ul>
                        </div>
                    </div>


                    <?php if(count($getProjects)>0){ ?>
                    <div class="results organization">
                        <div class="result-title">Projects</div>
                        <div class="wiggle-black"></div>

                        <div class="result-container">
                            <?php foreach ($variable as $key => $value) { ?>
                                <div class="result-list">
                                    <a href="#" class="media">
                                        <div class="img"><img src="<?php echo TEMPLATE_DOMAIN_IMAGES; ?>th.jpg" /></div>
                                        <div class="bd">
                                            <div class="org-name"><?php echo $value['project_name'] ?></div>
                                            <div class="org-details f16">
                                                <span class="bold">Country:</span> <?php echo getCountry($value['country_id']) ?><br />
                                                <span class="bold">Focus Area:</span> <?php echo getFocusArea($value['focus_area']) ?>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>


                        </div>
                        <div class="result-pagination textCenter f16 bold">
                            <a href="#"><span>1</span></a>
                            <a href="#"><span>2</span></a>
                            <a href="#"><span>3</span></a>
                            <a href="#"><span>4</span></a>
                            <a href="#"><span>5</span></a>
                        </div>
                    </div>
                    <?php } ?>

                </div>
            </section>
