<?php
/*
#***
@CLASSNAME: Db;
@AUTHOR: Anis uddin Ahmad <anisniit@gmail.com>;
@DATE: 2010-03-15 ;
@DESCRIPTION: A simple wrapper for PDO. Inspired by the sweet PDO wrapper from http://www.fractalizer.ru/frpost_120/php-pdo-wrapping-and-making-sweet/;
@LOG: August 10, 2010 added Class and methods Comment;
#***
*/
class Database
{
    private static $_pdoObject = NULL;

    protected static $_fetchMode = PDO::FETCH_ASSOC;
    protected static $_connectionStr = NULL;
    protected static $_driverOptions = array();

    private static $_username = NULL;
    private static $_password = NULL;

	/* 
	METHOD:PUBLIC
	@METHODNAME: 
		STATIC METHOD setConnectionInfo($schema, $username = null, $password = null, $database = 'mysql', $hostname = 'localhost');
	@PARAM: 
		String $schema, String $username, String $password, String $database, String $hostname;
	@RETURN: 
		void;
	@DESCRIPTION: 
		Set connection information;
	@CALL: 
		<pre>
		ex:
		Db::setConnectionInfo('basecamp','dbuser', 'password', 'mysql', 'http://mysql.abcd.com')
		</pre>
		
	***/
    public static function setConnectionInfo($schema, $username = null, $password = null, $database = 'mysql', $hostname = 'localhost')
    {
        if($database == 'mysql') {
            self::$_connectionStr = "mysql:dbname=$schema;host=$hostname";
            self::$_username      = $username;
            self::$_password      = $password;
        } else if($database == 'sqlite'){
            // For sqlite, $schema is the file path
            self::$_connectionStr = "sqlite:$schema";
        }

        // Making the connection blank
        // Will connect with provided info on next query execution
        self::$_pdoObject = null;
    }
	/* 
	METHOD:PUBLIC
	@METHODNAME: 
		STATIC METHOD execute($sql, $params = array());
	@PARAM: 
		 string  $sql (SQL statement), 
		 array   $params (A single value or an array of values);
	@RETURN: 
		integer number of effected rows;
	@DESCRIPTION: 
		Execute a statement and returns number of effected rows, Should be used for query which doesn't return resultset;
	@CALL: 
		<pre>
		ex:
		Db::execute($sql, $params)
		</pre>
		
	***/
    public static function execute($sql, $params = array())
    {
        $statement = self::_query($sql, $params);
		return $statement->rowCount();
    }
	/* 
	METHOD:PUBLIC
	@METHODNAME: 
		STATIC METHOD getValue($sql, $params = array());
	@PARAM: 
		 string  $sql    SQL statement, 
		 array   $params A single value or an array of values;
	@RETURN: 
		mixed array/boolean;
	@DESCRIPTION: 
		Execute a statement and returns a single value;
	@CALL: 
		<pre>
		ex:
		Db::getValue($sql, $params)
		</pre>
		
	***/
    public static function getValue($sql, $params = array())
    {
        $statement = self::_query($sql, $params);
        return $statement->fetchColumn(0);
    }

	/* 
	METHOD:PUBLIC
	@METHODNAME: 
		STATIC METHOD getRow($sql, $params = array());
	@PARAM: 
		string $sql SQL statement, 
		array $params A single value or an array of values;
	@RETURN: 
		array A result row;
	@DESCRIPTION: 
		Execute a statement and returns the first row;
	@CALL: 
		<pre>
		ex:
		Db::getRow($sql, $params)
		</pre>
		
	***/
    public static function getRow($sql, $params = array())
    {
        $statement = self::_query($sql, $params);
        return $statement->fetch(self::$_fetchMode);
    }

	/* 
	METHOD:PUBLIC
	@METHODNAME: 
		STATIC METHOD getResult($sql, $params = array());
	@PARAM: 
		string $sql SQL statement, 
		array $params A single value or an array of values;
	@RETURN: 
		array Result rows;
	@DESCRIPTION: 
		Execute a statement and returns row(s) as 2D array;
	@CALL: 
		<pre>
		ex:
		Db::getResult($sql, $params)
		</pre>
		
	***/
    public static function getResult($sql, $params = array())
    {
        $statement = self::_query($sql, $params);
        return $statement->fetchAll(self::$_fetchMode);
    }

    public static function getLastInsertId($sequenceName = "")
    {
        return self::$_pdoObject->lastInsertId($sequenceName);
    }

    public static function setFetchMode($fetchMode)
    {
        self::_connect();
        self::$_fetchMode = $fetchMode;
    }

    public static function getPDOObject()
    {
        self::_connect();
        return self::$_pdoObject;
    }

    public static function beginTransaction()
    {
        self::_connect();
        self::$_pdoObject->beginTransaction();
    }

    public static function commitTransaction()
    {
        self::$_pdoObject->commit();
    }

    public static function rollbackTransaction()
    {
        self::$_pdoObject->rollBack();
    }

    public static function setDriverOptions(array $options)
    {
        self::$_driverOptions = $options;
    }

    private static function _connect()
    {
        if(self::$_pdoObject != null){
            return;
        }
        
        if(self::$_connectionStr == null) {
            throw new PDOException('Connection information is empty. Use Db::setConnectionInfo to set them.');
        }

        self::$_pdoObject = new PDO(self::$_connectionStr, self::$_username, self::$_password, self::$_driverOptions);
    }
	/* 
	METHOD:PUBLIC
	@METHODNAME: 
		STATIC METHOD _query($sql, $params = array());
	@PARAM: 
		string  $sql SQL statement, 
		array   $params Parameters. A single value or an array of values;
	@RETURN: 
		Object Statement;
	@DESCRIPTION: 
		Prepare and returns a PDOStatement;
	@CALL: 
		<pre>
		ex:
		Db::_query($sql, $params)
		</pre>
		
	***/
    private static function _query($sql, $params = array())
    {
        if(self::$_pdoObject == null) {
            self::_connect();
        }
        $statement = self::$_pdoObject->prepare($sql, self::$_driverOptions);
        if (! $statement) {
            $errorInfo = self::$_pdoObject->errorInfo();
            throw new PDOException("Database error jon [{$errorInfo[0]}]:<br/><strong>{$errorInfo[2]}</strong><br/>, driver error code is $errorInfo[1]");
        }

        $paramsConverted = (is_array($params) ? ($params) : (array ($params )));

        if ((! $statement->execute($paramsConverted)) || ($statement->errorCode() != '00000')) {
            $errorInfo = $statement->errorInfo();
            throw new PDOException("Database error [{$errorInfo[0]}]:<br/><strong>{$errorInfo[2]}</strong>,<br/> driver error code is $errorInfo[1]".$sql);
        }

        return $statement;
    }
}
