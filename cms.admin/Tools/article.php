<?php
	function curPageURL() {
	 $pageURL = 'http';
	 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	 $pageURL .= "://";
	 if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	 } else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	 }
	 return $pageURL;
	}
	
	$catslug = $_GET['category_slug'];
	
	$oldURL = curPageURL();
	$wwwURL = str_replace("beta", "www", $oldURL);
	$newURL = str_replace("www.spot.ph", "spot.ph", $oldURL);
	$newURL = str_replace("spot.ph", "m.spot.ph", $newURL);
	?>
  
  <script language="javascript" type="text/javascript">
		var getAjaxClicks = function (url) {
		//_em.trackAjaxPageview(url);
		//_gaq.push(['_trackPageview', url]);
		}
	</script>
		
	<?php
		
	include("config.php");	
	
	require "config/variables.php";
	require "config/comment_settings.php";
	require "functions/utilities.function.1.2.4.php";
	
	require LIB_PATH."facebook-php-sdk/src/facebook.php"; 
	
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	//FB LIKE
	$fburl = "http://api.facebook.com/restserver.php?method=links.getStats&urls=".urlencode($wwwURL);
	$fbxml = file_get_contents($fburl);
	$fbxml = simplexml_load_string($fbxml);
	$fbshares =  $fbxml->link_stat->share_count;
	$fblikes =  $fbxml->link_stat->like_count;
	$fbcomments = $fbxml->link_stat->comment_count;
	$fbtotal = $fbxml->link_stat->total_count;
	$max = max($shares,$likes,$comments);
	
	//TWEET COUNT
	if(filter_var($oldURL, FILTER_VALIDATE_URL)) {
		$baseURL = 'http://urls.api.twitter.com/1/urls/count.json?url=';
		$json = file_get_contents($baseURL . $wwwURL);
		$obj = json_decode($json);
		$tweetcount = $obj->{'count'};
	}	
	
	//+1 COUNT
	$curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, "https://clients6.google.com/rpc");
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"' . $wwwURL . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
    $curl_results = curl_exec ($curl);
    curl_close ($curl);
    $json = json_decode($curl_results, true);
    $plusonecount = intval( $json[0]['result']['metadata']['globalCounts']['count'] );
	
	?>
    
    <?php
	
	//UPDATE SHARE
	
	$allsharecount = intval($fbshares) + intval($fblikes) + intval($tweetcount) + $plusonecount;
	
	if ($allsharecount != 0) $updateshare = $articles->update_share($_GET['id'], $allsharecount);
	
	?>
	
	<!--<script language="javascript" type="text/javascript">
	var getAjaxClicks = function (url) {
	_em.trackAjaxPageview(url);
	_gaq.push(['_trackPageview', url]);
	}
	</script-->
	
	<?php
	
	/*$GLOBALS['facebook'] = new Facebook(array(
		'appId'  => '112086159923',
		'secret' => 'e5af62b84b8553b8b7f0633e6cbce43d',
	));*/
	
	$page = isset($_GET["page"]) ? (int)$_GET["page"] : 1 ;
	
	if (isset($_GET['id']))
	{ 
		$_GET['id'] = $articles->clean_variable($_GET['id'], 1); 
	}
	$_GET['category_slug'] = $articles->clean_variable($_GET['category_slug'], 2);
	$post_name = $articles->clean_variable($_GET['article_post_name'], 2);
	
	
	//**************** USER MANAGEMENT - START ****************\\
	
	include("chklog.php");
	
	//***************** USER MANAGEMENT - END *****************\\
	
	//*********************** MAIN **********************\\
	
	$artiid = trim($_GET['id']);
	
	if($artiid == "")
	{
		if(isset($_GET['article_post_name']))
		{
		
			$article = $articles->get_post("", "", $post_name);
			$page = 1;
		
			if(is_array($article)) {
				
				$category = $articles->get_category_slug("", $article[0]["ID"]);
				
				if($category[0]["ID"]==19329 || $category[0]["ID"]==19330 || $category[0]["ID"]==23985 || $category[0]["ID"]==19331 || $category[0]["ID"]==22235) {
					$category_slug = "blog";
				} else {
					$category_slug = $category[0]["category_slug"];
				}
				
				header("location: ".ROOT."/".$category_slug."/".$article[0]["ID"]."/".$article[0]["article_post_name"]."/".$page);
			} else {
				header("location: ".ROOT."/404");
			}
			exit();
			
		} else {
			header("location: ".ROOT."/404");
		}
	}
	
	$articles->preview 		= (isset($_GET["preview"]) && $_GET["preview"] == true ? true : false);
	$result 										= $articles->get_post($_GET["category_slug"], "", $post_name);
	$tags												= @$articles->get_tags($_GET["category_slug"], $result[0]["ID"]);
	
	if(is_array($tags) && (!empty($tags) || !is_null($tags))) {
		$tag_id = "";
		foreach($tags as $key => $value) {
			$tag_id[$key] = $value["tag_id"];
		}
		$related_article = "";
		if(is_array($tag_id)) {
			$related_article = $articles->get_related($_GET["category_slug"], $result[0]["ID"], $tag_id);
			$related_article = $articles->multi_unique($related_article);
		}
	}
	
	$article_category = $articles->get_category_slug($_GET["category_slug"], $result[0]["ID"], true);
	$article_cuisine = $articles->get_cuisinename($result[0]["ID"]);
	
	$comment_count = $articles->get_comment($result[0]["ID"], $_GET["category_slug"]);
	if ($_GET["category_slug"] == 'establishment') {
		if ($result[0]["article_type"] == 1) $subcat = $article_cuisine;	
		else $subcat = $articles->get_subcategory($result[0]["ID"], $article_category[0]['ID'], 3);	
	}
	else {
		$subcat = $articles->get_subcategory($result[0]["ID"], $article_category[0]['ID']);	
	}
	
	$userauthor = $articles->get_username($result[0]["article_user"]);
	$userauthor = $userauthor[0]['user_firstname'].' '.$userauthor[0]['user_lastname'];
	
	$prevart = $articles->get_prevnext($result[0]["article_postdate"], $article_category[0]['ID'], 1);
	$nextart = $articles->get_prevnext($result[0]["article_postdate"], $article_category[0]['ID'], 0);
	
	if ($article_category[0]['ID'] == 19331) {
		$popcat = "News+Features";
		$popular = $main->getpopulararticle(11132, $date2week, $datenow, 8);		
		$shares = $main->getsharearticle(11132, $date2week, $datenow, 8);
	} elseif ($article_category[0]['ID'] == 23985 || $article_category[0]['ID'] == 23989) {
		$popcat = "Eat+Drink";
		$popular = $main->getpopulararticle(1622, $date2week, $datenow, 8);
		$shares = $main->getsharearticle(1622, $date2week, $datenow, 8);
	} elseif ($article_category[0]['ID'] == 19329 || $article_category[0]['ID'] == 23987 || $article_category[0]['ID'] == 23988) {
		$popcat = "Entertainment";
		$popular = $main->getpopulararticle(1623, $date2week, $datenow, 8);
		$shares = $main->getsharearticle(1623, $date2week, $datenow, 8);
	} else {	
		$popcat = $article_category[0]['category_name'];
		$popular = $main->getpopulararticle($article_category[0]['ID'], $date2week, $datenow, 8);
		$shares = $main->getsharearticle($article_category[0]['ID'], $date2week, $datenow, 8);
	}
	
	#gallery embed
	$galleryembedid = $result[0]["article_gallery_id"];
	
	#mood	
	$moodlist = $main->get_mood();
	$moodscore = $articles->get_moodscore($result[0]["ID"]);
	$moodtotal = $articles->countmood($result[0]["ID"]);	
	$moodmax = $articles->maxmood($result[0]["ID"]);	
	
	if($_GET["category_slug"] != "establishment" || $_GET["category_slug"] != "event" || $_GET["category_slug"] != "movie") {
		
		$page_title 				= $result[0]['article_title'];		
		$post_name 			= $result[0]['article_post_name'];
		$article_content 	= html_entity_decode($result[0]['article_content']);
		
		# embedded youtube
		$regex = "#([[]youtube[]])(.*)([[]/youtube[]])#e";
		$output = preg_replace($regex,"('<div align=\"center\"><object width=\"425\" height=\"350\" data=\"$2\" type=\"application/x-shockwave-flash\"><param name=\"src\" value=\"$2\" /></object></div>')", $article_content);
		
		# embedded image caption
		$regex2 = "#([[]caption.*caption=\")(.*)(\"[]])(.*)([[]/caption[]])#";
		$output = preg_replace($regex2,"<div align=\"center\" style=\"clear: both\">$4</div><div align=\"center\" style=\"clear: both\">$2</div>", $output);
			
		# embedded old image gallery
		$regex5 = '#(\[svgallery name=\")(.*)(\"\])#';
		$output = preg_replace($regex5,
				"<div align=\"center\"><object width=\"475\" height=\"500\">
						<param name=\"movie\" value=\"http://www.spot.ph/sv_gallery/simpleviewer.swf?galleryURL=http://www.spot.ph/sv_gallery_xml.php?gname=$2\"></param>
				<param name=\"allowFullScreen\" value=\"true\"></param>
							<param name=\"allowscriptaccess\" value=\"always\"></param>
							<param name=\"bgcolor\" value=\"343434\"></param>
							<embed src=\"http://www.spot.ph/sv_gallery/simpleviewer.swf?galleryURL=http://www.spot.ph/sv_gallery_xml.php?gname=$2\"
	type=\"application/x-shockwave-flash\" allowscriptaccess=\"always\" allowfullscreen=\"true\" width=\"475\" height=\"500\"
	bgcolor=\"343434\"></embed></object></div>", $output);
		
		$output = str_replace("http://www.youtube.com/watch?v=", "http://www.youtube.com/v/", $output);
		$output = str_replace("<!-- nextpage -->", "<!--nextpage-->", $output);
		
		# explode pagination
		$whole_content 	= explode("<!--nextpage-->", $output);
		
		if(is_array($whole_content))
		{
			$the_content = strip_tags($whole_content[($page-1)], '<table><tr><td><p><a><b><font><b><strong><i><em><span><img><li><ol><ul><iframe><embed><object><param><center><div><br/><br><nextpage><video><source>');
			$regex4 = "#([[]poll[]])(.*)([[]/poll[]])#e";
			$the_content = preg_replace($regex4,"('<div align=\"center\" style=\"margin: 0px auto;\"><script type=\"text/javascript\" src=\"".ROOT."/poll.inc.php?pollid=$2\"><![CDATA[//Poll]]></script></div>')", $the_content);
		}
		else
		{
			$the_content = strip_tags($output, '<p><a><b><font><b><strong><i><em><img><li><ol><ul><iframe><embed><object><param><center><div><br/><br><nextpage><video><source>');
			$regex4 = "#([[]poll[]])(.*)([[]/poll[]])#e";
			$the_content = preg_replace($regex4,"('<div align=\"center\" style=\"margin: 0px auto;\"><script type=\"text/javascript\" src=\"".ROOT."/poll.inc.php?pollid=$2\"><![CDATA[//Poll]]></script></div>')", $the_content);
		}
		
		# pagination
		$pagination = "";	
		
		if(count($whole_content) > 1){
			if($page > 1) $pagination = '<a href="'.ROOT.'/'.($_GET["category_slug"] != "" ? $_GET["category_slug"] : "featured").'/'.$result[0]["ID"].'/'.$result[0]["article_post_name"].'/'.($page-1).(isset($_GET["cpage"]) && (int)$_GET["cpage"] ? "/".$_GET["cpage"] : "").(isset($_GET["preview"]) ? "?preview=true" : "").'" class="roboto blacktext mediumtext2 nodecor">Prev Page</a>&nbsp;&nbsp;&nbsp;';
			for($i=1; $i<=count($whole_content); $i++) {
				if(isset($page) && (int)$page == $i) {
					$pagination .= '<div class = "pageactive whitetext mediumtext2">'.$i.'</div>';
				} else {
					$pagination .= '<a href="'.ROOT.'/'.($_GET["category_slug"] != "" ? $_GET["category_slug"] : "featured").'/'.$result[0]["ID"].'/'.$result[0]["article_post_name"].'/'.$i.(isset($_GET["cpage"]) && (int)$_GET["cpage"] ? "/".$_GET["cpage"] : "").(isset($_GET["preview"]) ? "?preview=true" : "").'" class="nodecor"><div class = "pagelink whitetext mediumtext2">'.$i.'</div></a>';
				}
			}
			if($page < count($whole_content)) $pagination .= '&nbsp;&nbsp;&nbsp;<a href="'.ROOT.'/'.($_GET["category_slug"] != "" ? $_GET["category_slug"] : "featured").'/'.$result[0]["ID"].'/'.$result[0]["article_post_name"].'/'.($page+1).(isset($_GET["cpage"]) && (int)$_GET["cpage"] ? "/".$_GET["cpage"] : "").(isset($_GET["preview"]) ? "?preview=true" : "").'" class="roboto blacktext mediumtext2 nodecor">Next Page</a>';
		} else {
			$pagination = "";
		}
		
		
		$dfrom = date("Y-m-d", strtotime("-2 weeks"));
		$dnow = date('Y-m-d');
	
	}
	
	if($_GET["category_slug"] == "event")
	{
		$event_schedule = $articles->get_event_schedules($_GET["article_post_name"], $result[0]["ID"]);
	}
	
	# article views
	switch($_GET["category_slug"])
	{
		case "establishment" :		$type = 2; 		$article_category = ""; 					$article = false; break;
		case "movie" : 				$type = 4; 		$article_category = ""; 					$article = false; break;
		case "event" : 				$type = 3; 		$article_category = ""; 					$article = false; break;
		default : 						$type = 1; 		$category = $article_category; 			$article = true; break;
	}
	
	/*RELATED ESTABLISHMENT */
	if ($result[0]["article_type"] == 1) $related_establishment = $articles->get_related_resto($subcat[0]['ID'], $result[0]["ID"], 0, 6);
	else $related_establishment = $articles->get_related_esta($subcat[0]['category_id'], $result[0]["ID"], 0, 6);
	
	/* OTHER BRANCHES */
	$other_branch = $articles->get_other_branch($result[0]['article_title'], $result[0]["ID"], 0, 6);	
	
	/* NEARBY */
	$nearby = $articles->get_nearby($result[0]['city_id'], $result[0]["ID"], 0, 6);	
	
	$article_category = is_array($article_category) ? $article_category : NULL;
	$category_slug = $_GET["category_slug"];
	
	@$articles->increment_page_view2($result[0]["ID"]);
	
	# assigned values
	$article_page = true;
	$page_title = @stripslashes(strip_tags($result[0]["article_title"]));
	$art_thumbnail = strip_tags($result[0]["article_thumbnail"]);
	$art_image = strip_tags($result[0]["article_image"]);
	$category = @$articles->get_category_slug($_GET["category_slug"], $result[0]["ID"]);
	$arttags = @$tags;
	$article = $article;
	$npo = $npo;
	$maintain = 0;
	
	$nowdate = $now = date("Y-m-d");
	$result = $result;
	$tags = $tags;
	//$related_article = $_GET["category_slug"]=="establishment" ? "" : $related_article;
	$article_type = $articles->get_article_type($_GET["category_slug"]);
	
	$preview = (isset($_GET["preview"]) && $_GET["preview"]==true ? true : false);
	$current_url = "http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	
	if ($category_slug == 'establishment') { 
		$adid = 'directory';
		$social_cat = 'establishment';
		$thisisdir = 1;
	} elseif ($category_slug == 'movie') {
		$adid = 'movies';
		$yesentertainment = 1;
		$social_cat = 'movies';
	} elseif ($category_slug == 'event') {
		$adid = 'events';
		$social_cat = 'event';
	} else {
		if ($article_category[0]['ID'] == 1622) {
			$adid = 'eat_drink';
			$yeseat = 1;
			$social_cat = 'eatdrink';
		} elseif ($article_category[0]['ID'] == 1623) {
			$adid = 'entertainment';
			$yesentertainment = 1;
			$social_cat = 'entertainment';
		} elseif ($article_category[0]['ID'] == 11132) {
			$adid = 'news_features';
			$social_cat = 'newsfeatures';
		} elseif ($article_category[0]['ID'] == 1625) {
			$adid = 'people_parties';
			$social_cat = 'peopleparties';
		} elseif ($article_category[0]['ID'] == 1624) {
			$adid = 'shopping_services';
			$social_cat = 'shopping';
		} elseif ($article_category[0]['ID'] == 11527) {
			$adid = 'news_features';
			$social_cat = 'newsfeatures';
		} elseif ($article_category[0]['ID'] == 17479) {
			$adid = 'news_features';
			$social_cat = 'the-feed';
		} elseif ($article_category[0]['ID'] == 11526) {
			$adid = 'events';
			$social_cat = 'event';
		} elseif ($article_category[0]['ID'] == 19331) {
			$adid = 'news_features';
			$social_cat = 'newsfeatures';
		} elseif ($article_category[0]['ID'] == 19330) {
			$adid = 'eat_drink';
			$social_cat = 'eatdrink';
		} elseif ($article_category[0]['ID'] == 23985) {
			$adid = 'eat_drink';
			$social_cat = 'eatdrink';
		} elseif ($article_category[0]['ID'] == 19329) {
			$adid = 'entertainment';
			$social_cat = 'entertainment';
		} elseif ($article_category[0]['ID'] == 23989) {
			$adid = 'eat_drink';
			$social_cat = 'eatdrink';
		} elseif ($article_category[0]['ID'] == 23987) {
			$adid = 'entertainment';
			$social_cat = 'entertainment';
		} elseif ($article_category[0]['ID'] == 23988) {
			$adid = 'entertainment';
			$social_cat = 'entertainment';
		} elseif ($article_category[0]['ID'] == 22235) {
			$adid = 'eat_drink';
			$social_cat = 'eatdrink';
		} elseif ($article_category[0]['ID'] == 23979) {
			$adid = 'entertainment';
			$social_cat = 'entertainment';
		} elseif ($article_category[0]['ID'] == 6849) {
			$adid = 'others';
			$social_cat = 'article';
		} elseif ($article_category[0]['ID'] == 26961) {
			$adid = 'promo';
			$social_cat = 'promo';
		} elseif ($article_category[0]['ID'] == 23973) {
			$adid = 'galleries';
			$social_cat = 'gallery';
		} else {
			$adid = 'others';
			$social_cat = 'article';
		}
	}
						
	if ($result[0]['article_postdate']	 > '2012-11-19 01:00:00')
	{
			$social_url = str_replace("beta", "www", $oldURL);
	}
	else
	{
			$social_url = str_replace("beta", "www", $oldURL);
	}
	
	switch($_GET["category_slug"])
	{
			case "establishment": 		$type = 2; break;
			case "event": 					$type = 3; break;
			case "movie": 				$type = 4; break;
			case "my-list": 				$type = 7; break;
			case "poll": 					$type = 21; break;
			default: 						$type = 1; break;
	}
	
	$globals['comments']['config'] = array (
		'type_id' 					=> $type, #article type int
		'post_id'						=> $_GET['id'] ,
		'title'							=> strip_tags($result[0]['article_title']) ,
		'category'					=> $_GET['category_slug'] ,
		'content'						=> 'string blurb' ,
		'thumbnail' 				=> strip_tags($result[0]['article_thumbnail']),
		'link'							=>  'http://www.spot.ph/'.$_GET['category_slug'].'/'.$_GET['id'].'/'.$_GET['article_post_name'] ,
		'comment_perpage'		=> 20 // int
	);
	
	global $sroot;
	
?>

<?php include("arthtml.php"); ?>

			
<script>
	// Declaration of global variables for javascript
	var PUBLICDOMAIN = "<?php echo $config['publicdomain']; ?>";
	var IMAGEPATH = "<?php echo images; ?>";
	var CURRENT_URL = "<?php echo $config['publicdomain'].$_SERVER['REQUEST_URI']; ?>";
	<?php if ($globals['comments']['config']) { ?>
	var CAPTCHA = "<?php echo $globals['comments']['file']['captcha']; ?>";
	/* Initialize double post flag */
	var preventDoublePostFlag = false;
	<?php } ?>
</script>

<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js" type="text/javascript"></script>
<script src="http://beta.spot.ph/js/spot_comments.v1.js?v=1"></script>

<script src="http://beta.spot.ph/js/jquery.comments.<?php echo $globals['comments']['version']['js_comment']; ?>.js" type="text/javascript"></script>
<script src="http://beta.spot.ph/js/jquery.pagination.<?php echo $globals['comments']['version']['js_pagination']; ?>.js" type="text/javascript"></script>
