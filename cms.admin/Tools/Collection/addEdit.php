<?php 
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php'); 
$conn->checklog();
include(ADMIN_TEMPLATE_PATH.'header.php'); 

?>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/tinymce/jquery.tinymce.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/wysiwyg.js"></script>
<?php

$row_cat = $conn->get_array_rs("select * from ".$maintable_prefix."_category WHERE cat_parent = 0 ORDER BY cat_id ASC");

$row_size = $conn->get_array_rs("select * from ".$maintable_prefix."_sizes ORDER BY id ASC");

$cat = array();

if(isset($_GET['id'])){

    $id = $_GET['id'];

    if(trim($id)!=""){

        $size = array();

        $row_content = $conn->array_rs_single("select * from ".$maintable_prefix."_products WHERE prod_id = '$id' ");

        $row_cat_data = $conn->get_array_rs("select cat_id from ".$maintable_prefix."_product_cat WHERE prod_id = '$id'");
        
        foreach ($row_cat_data as $key => $value) {
            foreach ($value as $val) {
                $cat[] = $val;
            }
        }

        $row_size_data = $conn->get_array_rs("select size from ".$maintable_prefix."_products_sizes WHERE prod_id = '$id'");
        
        if(count($row_size_data)>0){
            foreach ($row_size_data as $key => $value) {
                foreach ($value as $val) {
                    $sizeArr[] = $val;
                }
            }
        } else {
            $sizeArr =  array();
        }
        

        $type = "Edit";      
    } else {
        $type = "Add";
    }

} else {
     $type = "Add";
     $sizeArr = array();
}


?>
<div class="mainwrapper">
    
    <?php
        include(ADMIN_TEMPLATE_PATH.'mainHead.php');
        include(ADMIN_TEMPLATE_PATH.'navigation.php');
    ?>


    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="<?php echo SITE_ADMIN_DOMAIN; ?>dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Products</li>
        </ul>
        
        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <h1>Products[<?php echo $type; ?>]</h1>
            </div>
        </div><!--pageheader-->
        
        <div class="maincontent">
            <div class="maincontentinner">
            <?php echo $_SESSION['message']; ?>
            <div class="widgetbox box-inverse">
                <h4 class="widgettitle"><?php echo $type; ?> Product</h4>
                <div class="widgetcontent nopadding">
                    <form class="stdform stdform2" method="post" name="form1" id="form1" action="<?php echo SITE_ADMIN_DOMAIN; ?>Tools/Collection/process.php" enctype="multipart/form-data">
                        <input type="hidden" name="prod_id" id="prod_id" value="<?php echo $row_content['prod_id'] ?>" />
                        <input type="hidden" name="actions" id="actions" value="<?php echo $type; ?>" />
                            <p>
                                <label>Name*</label>
                                <span class="field"><input type="text" name="prod_name" id="prod_name" class="input-xxlarge" value="<?php echo $row_content['prod_name'] ?>" /></span>
                            </p>

                            <p>
                                <label>Category*</label>
                                <span class="field" style="width: 300px; height: 200px; overflow: visible;  overflow-y: auto;">
                                        <?php foreach ($row_cat as $key => $value) { ?>
                                            <span class=""><input type="checkbox" name="cat_id[]" id="cat_id" style="opacity: 0;" value="<?php echo $value['cat_id'] ?>" <?php if(in_array($value['cat_id'], $cat)){ echo 'checked="checked"'; } ?> ></span><?php echo $value['cat_name'] ?><br>
                                        <?php 
                                            $row_catSub = $conn->get_array_rs("select * from ".$maintable_prefix."_category WHERE cat_parent = '".$value['cat_id']."'"); 
                                            if(count($row_catSub)>0){
                                                foreach ($row_catSub as $valve) {
                                                
                                        ?>
                                            <span style="padding-left: 20px;"><input type="checkbox" name="cat_id[]" id="cat_id" style="opacity: 0;" value="<?php echo $valve['cat_id'] ?>" <?php if(in_array($valve['cat_id'], $cat)){ echo 'checked="checked"'; } ?>></span><?php echo $valve['cat_name'] ?><br>
                                                <?php 
                                                    $row_catSub_s = $conn->get_array_rs("select * from ".$maintable_prefix."_category WHERE cat_parent = '".$valve['cat_id']."'"); 
                                                    if(count($row_catSub_s)>0){
                                                        foreach ($row_catSub_s as $valve2) {

                                                ?>
                                                <span style="padding-left: 40px;"><input type="checkbox" name="cat_id[]" id="cat_id" style="opacity: 0;" value="<?php echo $valve2['cat_id'] ?>" <?php if(in_array($valve2['cat_id'], $cat)){ echo 'checked="checked"'; } ?>></span><?php echo $valve2['cat_name'] ?><br>
                                                <?php 
                                                        }
                                                    }

                                                ?>
                                        <?php
                                                }
                                            }
                                        } ?>
                                </span>
                            </p>

                            <p>
                                <label>Sizes</label>
                                <span class="field">
                                    <?php 
				    $i = 1;
                                    foreach ($row_size as $key => $size) { 

                                        ?>
                                        <span style="display: block;"><input type="checkbox" name="sizes[<?php echo $i; ?>]" id="sizes" value="<?php echo $size['size_name'] ?>" <?php echo (in_array($size['size_name'],$sizeArr)) ? 'checked="checked"' : ''; ?> /> <?php echo $size['size_name'] ?>
                                            <?php 
                                            $getQuantity = $conn->array_rs_single("select quantity from ".$maintable_prefix."_products_sizes WHERE prod_id = '".$id."' AND size = '".$size['size_name']."'"); 
                                            $qtY = ($getQuantity['quantity']!='D') ? $getQuantity['quantity'] : 0;
                                            ?>
                                            <input type="text" name="pQty[<?php echo $i; ?>]" id="pQty" class="input-small" value="<?php echo $qtY; ?>" placeholder="Quantity" />
                                        </span><br />
                                    <?php 
				    	$i++;
				    } ?>
                                </span>
                            </p>

                            <p>
                                <label>Price*</label>
                                <span class="field"><input type="text" name="prod_price" id="prod_price" class="input-large" value="<?php echo $row_content['prod_price'] ?>" /></span>
                            </p>

                            <p>
                                <label>Stock*</label>
                                <span class="field"><input type="text" name="prod_stock" id="prod_stock" class="input-large" value="<?php echo $row_content['prod_stock'] ?>" /></span>
                            </p>

                            <p>
                                <label>OnSale*</label>
                                <span class="field">
                                    <select name="prod_sale" id="prod_sale">
                                        <option value="0" <?php echo ($row_content['prod_sale']==0) ? 'selected="selected"' : '';  ?>>No</option>
                                        <option value="1" <?php echo ($row_content['prod_sale']==1) ? 'selected="selected"' : '';  ?>>Yes</option>
                                    </select>
                                </span>
                            </p>

                            <p>
                                <label>Sale Price</label>
                                <span class="field"><input type="text" name="prod_price_sale" id="prod_price_sale" class="input-xxlarge" value="<?php echo $row_content['prod_price_sale'] ?>" /></span>
                            </p>

                            <p>
                                <label>Display Out Of Stock</label>
                                <span class="field">
                                    <select name="prod_outofstock" id="prod_outofstock">
                                        <option value="0" <?php echo ($row_content['prod_outofstock']==0) ? 'selected="selected"' : '';  ?>>No</option>
                                        <option value="1" <?php echo ($row_content['prod_outofstock']==1) ? 'selected="selected"' : '';  ?>>Yes</option>
                                    </select>
                                </span>
                            </p>

                            <p>
                                <label>Details</label>

                                <textarea id="elm1" name="prod_details" rows="15" cols="80" style="width: 80%" class="tinymce">
                                    <?php echo html_entity_decode($row_content['prod_details']) ?>
                                </textarea>
                            </p>

                            <p>
                                <label>Note</label>
                                <span class="field"><input type="text" name="prod_note" id="prod_note" class="input-xxlarge" value="<?php echo $row_content['prod_note'] ?>" /></span>
                            </p>

                            <p>
                                <label>Popular</label>
                                <span class="field">
                                    <select name="prod_popular" id="prod_popular">
                                        <option value="0" <?php echo ($row_content['prod_popular']==0) ? 'selected="selected"' : '';  ?>>No</option>
                                        <option value="1" <?php echo ($row_content['prod_popular']==1) ? 'selected="selected"' : '';  ?>>Yes</option>
                                    </select>
                                </span>
                            </p>

                            <p>
                                <label>Rated</label>
                                <span class="field">
                                    <select name="prod_rated" id="prod_rated">
                                        <option value="0" <?php echo ($row_content['prod_rated']==0) ? 'selected="selected"' : '';  ?>>No</option>
                                        <option value="1" <?php echo ($row_content['prod_rated']==1) ? 'selected="selected"' : '';  ?>>Yes</option>
                                    </select>
                                </span>
                            </p>

                            <p>
                                <label>Featured(Show On Homepage)</label>
                                <span class="field">
                                    <select name="prod_feature" id="prod_feature">
                                        <option value="0" <?php echo ($row_content['prod_feature']==0) ? 'selected="selected"' : '';  ?>>No</option>
                                        <option value="1" <?php echo ($row_content['prod_feature']==1) ? 'selected="selected"' : '';  ?>>Yes</option>
                                    </select>
                                </span>
                            </p>

                            
                            <p>
                                <label>&nbsp;</label>
                                <span class="field">
                                    <input type="button" class="btn" id="addProd" value="Add Image">
                                </span>
                            </p>
                            
                            <div id="pD">
                                <?php 
                                    $row_prod = $conn->get_array_rs("select * from ".$maintable_prefix."_products_images WHERE prod_id = '".$_GET['id']."' ");
                                    $ctr = 1;
                                    if(isset($_GET['id']) && count($row_prod)>0){

                                        foreach ($row_prod as $key => $valP) {
                                ?>
                                        <div id="productBox" style="border-top: 1px solid #ddd;">
                                            <p>
                                                <label>Alt <span class="titleCount"><?php echo $ctr; ?></span></label>
                                                <span class="field"><input type="text" name="prod_alt[]" id="prod_alt" class="input-xxlarge" value="<?php echo $valP['prod_alt'] ?>" /></span>
                                            </p>
                                            <p>
                                                <label>Thumbnail</label>
                                                <span class="field">
                                                    <input type="file" name="prod_thumbs[]" class="input-xlarge" value="" /><br />
                                                    <?php 
                                                        if($valP['prod_thumbs']!=""){
                                                            echo '<img src="'.SITE_DOMAIN.$valP['prod_thumbs'].'" />';
                                                        }
                                                    ?>
                                                    <input type="hidden" name="hprod_thumbs[]" value="<?php echo $valP['prod_thumbs'] ?>" />
                                                </span>
                                            </p>
                                            <p>
                                                <label>Large Image</label>
                                                <span class="field">
                                                    <input type="file" name="prod_image[]" class="input-xlarge" value="" /><br />
                                                    <?php 
                                                        if($valP['prod_image']!=""){
                                                            echo '<img src="'.SITE_DOMAIN.$valP['prod_image'].'" />';
                                                        }
                                                    ?>
                                                    <input type="hidden" name="hprod_image[]" value="<?php echo $valP['prod_image'] ?>" />
                                                </span>
                                            </p>
                                            <p>
                                                <label>&nbsp;</label>
                                                <span class="field"><input type="button" class="btn deleteProd"  value="Delete Product"></span>
                                            </p>
                                        </div>
                                <?php
                                        $ctr++;
                                        }

                                ?>
                                <?php } else { ?>
                                    <div id="productBox" style="border-top: 1px solid #ddd;">
                                        <p>
                                            <label>Alt <span class="titleCount">1</span></label>
                                            <span class="field"><input type="text" name="prod_alt[]" id="prod_alt" class="input-xxlarge" value="<?php echo $row_content['prod_alt'] ?>" /></span>
                                        </p>
                                        <p>
                                            <label>Thumbnail</label>
                                            <span class="field"><input type="file" name="prod_thumbs[]" class="input-xlarge" value="" /></span>
                                        </p>
                                        <p>
                                            <label>Large Image</label>
                                            <span class="field"><input type="file" name="prod_image[]" class="input-xlarge" value="" /></span>
                                        </p>
                                        <p>
                                            <label>&nbsp;</label>
                                            <span class="field"><input type="button" class="btn deleteProd" value="Delete Product"></span>
                                        </p>
                                    </div>
                                <?php } ?>
                            </div>
                                                    
                            <p class="stdformbutton">
                                <input type="submit" class="btn btn-primary" value="Submit Button">
                                <!-- <button class="btn btn-primary">Submit Button</button> -->
                                <button type="reset" class="btn">Reset Button</button>
                            </p>
                    </form>
                </div><!--widgetcontent-->
            </div><!--widget-->                
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->

</div><!--mainwrapper-->
<script type="text/javascript">
    var m = 1;
    jQuery("#addProd").click(function() {
        m++;
        jQuery("#productBox")
            .last()
            .clone()
            .appendTo(jQuery("#pD"))
            .find(".titleCount").html(m);
        return false;
        
    });

    jQuery(".deleteProd").click(function(){
        jQuery(this).parent().parent().parent().remove();
    });
</script>
<?php include(ADMIN_TEMPLATE_PATH.'footer.php'); ?>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>