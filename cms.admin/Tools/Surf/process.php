<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";

if($_POST['actions']=="Add"){
	extract($_POST);

	if (trim($news_title)== ""){
		$msg .= "Tittle is required.<br />";
		$err++;
	}

	if (trim($elm1)== ""){
		$msg .= "Text is required.<br />";
		$err++;
	}

	$datex = explode('-', $news_publish_date);

	if ($err==0){
		//update products table
		$data = array('news_title'=>htmlspecialchars($news_title,ENT_QUOTES),
			'news_slug'=>makeUrlFriendly(strtolower($news_title)),
			'news_category'=>$news_category,
			'news_meta_description'=>$news_meta_description,
			'news_meta_keywords'=>$news_meta_keywords,
			'news_publish_date'=>$news_publish_date,
			'news_tag'=>$news_tag,
			'news_year'=>$datex[0],
			'news_month'=>$datex[1],
			'news_day'=>$datex[2],
			'news_blurb'=>$news_blurb,
			'news_content'=>htmlspecialchars($elm1,ENT_QUOTES),
			'news_status'=>$news_status,
			'news_added_by'=>$admin,
			'news_date_added'=>strtotime("now"));
		$conn->insert($data,$maintable_prefix.'_surf','');	
		$id = $conn->get_last_id($maintable_prefix.'_surf','news_id');		

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["news_image"])) && ($_FILES['news_image']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['news_image']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["news_image"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/news/'.time().'-'.$filename;
			  $filename = '/images/news/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['news_image']['tmp_name'],$newname))) {

					$data = array('news_image'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_surf',"news_id = '".$id."'");

				} else {

				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["news_image"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}				


		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["news_thumbs"])) && ($_FILES['news_thumbs']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['news_thumbs']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["news_thumbs"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/news/'.time().'-'.$filename;
			  $filename = '/images/news/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['news_thumbs']['tmp_name'],$newname))) {

					$data = array('news_thumbs'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_surf',"news_id = '".$id."'");

				} else {

				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["news_thumbs"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}		

		$_SESSION['message'] = '<h4 class="widgettitle title-success">New news added. '.$news_title.'<br />'.$msg.'</h4>';
		header("Location: index.php");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);

	if (trim($news_title)== ""){
		$msg .= "Tittle is required.<br />";
		$err++;
	}

	if (trim($elm1)== ""){
		$msg .= "Text is required.<br />";
		$err++;
	}

	if ($err==0){

				$d = explode(' ', $news_publish_date);
		$datex = explode('-', $d[0]);

		extract($_POST);
		//update products table 
		$data = array('news_title'=>htmlspecialchars($news_title,ENT_QUOTES),
			'news_slug'=>makeUrlFriendly(strtolower($news_title)),
			'news_category'=>$news_category,
			'news_meta_description'=>$news_meta_description,
			'news_meta_keywords'=>$news_meta_keywords,
			'news_publish_date'=>$news_publish_date,
			'news_tag'=>$news_tag,
			'news_year'=>$datex[0],
			'news_month'=>$datex[1],
			'news_day'=>$datex[2],
			'news_blurb'=>$news_blurb,
			'news_content'=>htmlspecialchars($elm1,ENT_QUOTES),
			'news_status'=>$news_status,
			'news_edited_by'=>$admin,
			'news_date_edited'=>strtotime("now"));
		$conn->update_tbl($data,$maintable_prefix.'_surf',"news_id = '".$news_id."'");			

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["news_image"])) && ($_FILES['news_image']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['news_image']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["news_image"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/news/'.time().'-'.$filename;
			  $filename = '/images/news/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['news_image']['tmp_name'],$newname))) {

					$data = array('news_image'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_surf',"news_id = '".$news_id."'");

				} else {

				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["news_image"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}				


		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["news_thumbs"])) && ($_FILES['news_thumbs']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['news_thumbs']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["news_thumbs"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/news/'.time().'-'.$filename;
			  $filename = '/images/news/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['news_thumbs']['tmp_name'],$newname))) {

					$data = array('news_thumbs'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_surf',"news_id = '".$news_id."'");

				} else {

				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["news_thumbs"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}		

		
		$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$news_title.' Edited.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?id=".$news_id);	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?id=".$news_id);	
	}
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){
		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_surf where news_id ='".$id."'");
		echo '1';
	} else {
		echo '0';
	}

}