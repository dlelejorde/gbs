<?php
//database connections and functions

class MySQL_Conn {
	var $UN="";
	var $PW="";
	var $DBHOST="";
	var $DBNAME="";


	function DBC0nn($DB_un="",$DB_pw="",$DB_name="",$host="") {
		if (!empty($DB_un)) $this->UN = $DB_un;
		if (!empty($DB_pw)) $this->PW = $DB_pw;
		if (!empty($DB_name)) $this->DBNAME = $DB_name;
		if (!empty($host)) $this->DBHOST = $host;

		$this->link = mysql_connect($this->DBHOST, $this->UN, $this->PW);
		mysql_select_db($this->DBNAME);

		return $this->link;
	}


	function login($username,$password){
		$username_new = mysql_real_escape_string($username);
		$md5 = md5($password);
		$row = $this->array_rs_single("select * from ".USERSTBL." where username  = '$username_new' and password  = '$md5'");
		if(!isset($row['username'])){
			$_SESSION['uSuper'] = 0;
			return false;
		} else{

			$_SESSION['uID'] = $row['username'];
			$_SESSION['uSuper'] = $row['userlevel'];
			return true;
		}
	}



	function checklog(){
		$user_id = $_SESSION['uID'];
		//$user_access = $_SESSION['uSuper'];
		if($user_id != ""){
			return;
		} else{
			header("Location: ".SITE_ADMIN_DOMAIN."index.php");
			session_destroy();
			exit();
		}
	}



	function result_to_array(&$result) {
		if ($result) {
			$arrResultData=array(); $rCnt=0;
			while ($row = mysql_fetch_assoc($result)) {
				for ($i = 0; $i < mysql_num_fields($result); $i++) {
					$tbl_fieldname=mysql_field_name($result, $i);
					$arrResultData[$rCnt][$tbl_fieldname] = $this->revert_quotes($row[$tbl_fieldname]);
				}
				$rCnt++;
			}
			$retVal=$arrResultData;
		} else {
			$retVal = "DB Error: ".mysql_error();
		}

		return $retVal;
	}

	function get_array_rs($query) {
		$result=mysql_query($query);
		return $this->result_to_array($result);
	}

	function array_rs_single($query) {
		if (!empty($query)) {
			$result=mysql_query($query);
			if (!empty($result) && mysql_num_rows($result)>0)
				$retval=mysql_fetch_assoc($result);
			else
				$retval = "DB Error: ".mysql_error();
		} else {
			$retval = "DB Error: Empty Query String.";
		}

		return $retval;
	}

	/* $ins_FnV = Insert Fields and Value
		Example: 	$arrInsert[fieldname1] = "value";
	*/
	function insert($ins_FnV, $tablename, $allowed_tags='') {
		if (!is_array($ins_FnV)) return "Error: Not valid Inserts";

		$tbl_fields=$this->get_fields($tablename);
		$arrTMP_=$this->match_field_out($ins_FnV,$tablename);

		$fields=""; $values="";
		foreach($arrTMP_ as $ik => $iv) {
			if (!empty($fields)) $fields.=","; $fields.="`".$ik."`";
			if (!empty($values)) $values.=","; $values.= $iv;
		}
		//echo "INSERT INTO ".$tablename."($fields) VALUES($values);";
		mysql_query("INSERT INTO ".$tablename."($fields) VALUES($values);",$this->link);
		return mysql_error($this->link);
	}

	//same as function insert()
	function update_tbl($ins_FnV, $tablename, $condition) {
		if (!is_array($ins_FnV)) return "Error: Not valid Inserts";
		if (empty($condition)) return "Error: null conditions are now allowed";

		$tbl_fields=$this->get_fields($tablename);
		$arrTMP_=$this->match_field_out($ins_FnV,$tablename);

		if (is_array($arrTMP_) && (count($arrTMP_)>0)) {
			$sets="";
			foreach($arrTMP_ as $ik => $iv) {
				if (!empty($sets)) $sets.=",";
				$sets.="`".$ik."`=".$iv;
			}
			$sql="UPDATE ".$tablename." set ".$sets." where ".$condition."; \n"; //print_r($sql);
			mysql_query($sql,$this->link);
			$retval=mysql_error($this->link);
		} else {
			$retval="DB Error: No field updated.";
		}

		return $retval;
	}

	//For multiple records update
	function update_multiple($rs_update) {
		if (!is_array($rs_update)) return "Error: Not a valid record type to update";

		$sql="";
		for($rx=0; $rx<count($rs_update); $rx++) {
			if (!isset($rs_update[$rx][fields])) {
				$err="Error: Field values are not present at array index[$rx]"; break;
			}
			if (!is_array($rs_update[$rx][fields])) {
				$err="Error: Fields are not in array form at array index[$rx]"; break;
			}
			if (!isset($rs_update[$rx][dbtable])) {
				$err="Error: DB Table is not present for array index[$rx]"; break;
			}
			if (!isset($rs_update[$rx][condition])) {
				$err="Error: SQL condition is not present for array index[$rx]"; break;
			}

			$sets="";
			foreach($rs_update[$rx][fields] as $ik => $iv) {
				if (!empty($sets)) $sets.=",";
				$sets.="`".$ik."`=".$iv;
			}
			$sql.="UPDATE ".$rs_update[$rx][dbtable]." set ".$sets." where ".$rs_update[$rx][condition]."; \n";
		}

		if (isset($err)) {
			return $err;
		} else {
			mysql_query($sql,$this->link);
			return mysql_error($this->link);
		}
	}

	function get_fields($DBTable) {
		//echo "SHOW COLUMNS FROM ".$DBTable;
		$qry=mysql_query("SHOW COLUMNS FROM ".$DBTable,$this->link);

		$retval=$this->result_to_array($qry);
		return $retval;
	}

	function put_quote($fieldtype,$value) {
		$ftype=preg_split("/[\s\(\)]/",$fieldtype);
		switch(strtolower($ftype[0])) {
			case 'char':
			case 'time':
			case 'tinyblob':
			case 'tinytext':
			case 'blob':
			case 'text':
			case 'mediumtext':
			case 'mediumblob':
			case 'longblob':
			case 'longtext':
			case 'enum':
			case 'set':
			case 'datetime':
			case 'date':
			case 'varchar':
				if (!empty($value))
					$retval=$this->quoted_format(stripslashes($value));
				else
					$retval="''";
				break;
			default:
				if (empty($value))
					$retval="0";
				else
					$retval=$value;
				break;
		}

		return $retval;
	}

	function clean_quotes($data) {
		$match=array("'",'"'); $replacement=array("�","�");
		$retval=str_replace($match,$replacement,$data);
		return $retval;
	}

	function revert_quotes($data) {
		$match=array("�","�"); $replacement=array("'",'"');
		$retval=str_replace($match,$replacement,$data);
		return $retval;
	}

	function s_quote($string) {
		if (($string{0}=="'") && ($string{strlen($string)-1}=="'"))
			$string=$this->clean_quotes(stripslashes($string));
		else
			$string="'".$this->clean_quotes(stripslashes($string))."'";
		return $string;
	}

	function quoted_format($val='',$separator=null) {
		if (!is_null($separator)) {
			$arrVal=explode($separator,$val); $retval="";
			foreach($arrVal as $value) {
				if (!empty($retval)) $retval.=$separator;
				$retval.=$this->s_quote($value);
			}
		} else $retval=$this->s_quote($val);

		return $retval;
	}

	function match_field_out($arr_data,$db_table,$quoted_out=1) {
		$tbl_fields=$this->get_fields($db_table);
		for ($i = 0; $i < count($tbl_fields); $i++) {
			if (isset($arr_data[$tbl_fields[$i][Field]])) {
				if ($quoted_out) {
					//echo $arr_data[$tbl_fields[$i][Field]];
					$retval[$tbl_fields[$i][Field]] = $this->put_quote($tbl_fields[$i][Type],$arr_data[$tbl_fields[$i][Field]]);
				} else {
					//echo $arr_data[$tbl_fields[$i][Field]];
					$retval[$tbl_fields[$i][Field]] = $arr_data[$tbl_fields[$i][Field]];
				}
			}
		}
		return $retval;
	}

	function execute_sql($query) {
		mysql_query($query,$this->link);
		return mysql_error($this->link);
	}

	function count_records($query) {
		$query = mysql_query($query,$this->link);
		$count = mysql_num_rows($query);
		if (!$count){
			return 0;
		} else {
			return $count;
		}
	}

	function cleanQuery($string) {
		if(get_magic_quotes_gpc()) {
			$string = stripslashes($string);
		}
		if (phpversion() >= '4.3.0') {
			$string = mysql_real_escape_string($string);
		} else {
			$string = mysql_escape_string($string);
		}

		return $string;
	}

	function get_last_id($db_table,$field){
		$query = $this->array_rs_single("select $field from $db_table order by $field desc ");
		return $query[$field];
	}


	function verifypassword($user_id, $password, $new, $retype) {
		$sql_rsGetMyPassword = "SELECT * FROM account WHERE acct_id = '$user_id'";
		$result_rsGetMyPassword = $this->array_rs_single($sql_rsGetMyPassword);
		$mypassword = $result_rsGetMyPassword['acct_password'];

		$password = md5($password);

		if ($password == $mypassword) {
			if ($new == $retype) {
				return $isverified = 1;
			} else {
				$_SESSION[msg2] = "New Password and Retype password didn't match.";
				return $isverified = 0;
			}
		} else {
			$_SESSION[msg2] = "Old password didn't match.";
			return $isverified = 0;
		}

	}

	function pagination($query, $display, $curpage){
				$rscount = $this->get_array_rs($query);
				$rowcount = count($rscount);

				$totalrows = $rowcount;
				$start = ($pagenum!='') ? (($display * $pagenum) - $display) : (($display * $curpage) - $display);

				 $limit = "LIMIT $start, $display";

				 return $limit."-".$totalrows."-".$display."-".$curpage."-".$start;
	}

	function pagi_text($totalrows,$display,$curpage,$start,$nav){

				$totallinks = ceil($totalrows/$display);
				if ($totallinks<1){
					$next = "";
					$prev = "";
					$pagination = "";
					$page = "";
				}
				else{
					$nextpage = ($curpage>0) ? $curpage+1 : '';
					$prevpage = $curpage-1;
					$first = "<a href=".$_SERVER['PHP_SELF']."?".$nav."&page=1>First</a>";
					$next = "<a href=".$_SERVER['PHP_SELF']."?".$nav."&page=$nextpage>Next</a>";
					$prev = "<a href=".$_SERVER['PHP_SELF']."?".$nav."&page=$prevpage>Previous</a>";
					$last = "<a href=".$_SERVER['PHP_SELF']."?".$nav."&page=$totallinks>Last</a>";

					if (($start+$display)>=$totalrows){
						$next = "<span style=\"color: grey\">Next</span>";
					}
					if ($prevpage==0){
						$prev = "<span style=\"color: grey\">Previous</span>";
					}
					if ($curpage==$totallinks){
						$last = "<span style=\"color: grey\">Last</span>";
					}
					if ($curpage==1){
						$first = "<span style=\"color: grey\">First</span>";
					}

					$showpage = (($start+$display) > $totalrows) ? $totalrows : ($start+$display);

					$start = $start+1;
					$showing = "Viewing <strong>$start</strong> to <strong>".$showpage."</strong>&nbsp;(<strong>".$totalrows."</strong> Total)";
					$page = "$first&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$prev&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$next&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$last";
					return $page.",".$showing.",".$start;
				}

	}

	function pagi_text2($totalrows,$display,$curpage,$start,$nav,$selected){

				$totallinks = ceil($totalrows/$display);
				if ($totallinks<1){
					$next = "";
					$prev = "";
					$pagination = "";
					$page = "";
				}
				else{
					$nextpage = ($curpage>0) ? $curpage+1 : '';
					$prevpage = $curpage-1;
					$number = "";
					for ($x2=1;$x2<=$totallinks;$x2++){
						if ($selected==$x2){
							$number .="<font color=\"#f5ce73\"><strong>".$x2."</strong></font>&nbsp;&nbsp;&nbsp;&nbsp;";
						} else {
							$number .="<a href=".$_SERVER['PHP_SELF']."?".$nav."&page=".$x2." style=\"text-decoration: none; color: #884b20;\">".$x2."</a>&nbsp;&nbsp;&nbsp;&nbsp;";

						}
					}
					$first = "<a href=".$_SERVER['PHP_SELF']."?".$nav."&page=1 style=\"text-decoration: none; color: #884b20;\">First</a>";
					$next = "<a href=".$_SERVER['PHP_SELF']."?".$nav."&page=$nextpage style=\"text-decoration: none; color: #884b20;\">next</a>";
					$prev = "<a href=".$_SERVER['PHP_SELF']."?".$nav."&page=$prevpage style=\"text-decoration: none; color: #884b20;\">prev</a>";
					$last = "<a href=".$_SERVER['PHP_SELF']."?".$nav."&page=$totallinks style=\"text-decoration: none; color: #884b20;\">Last</a>";

					if (($start+$display)>=$totalrows){
						$next = "<span >next</span>";
					}
					if ($prevpage==0){
						$prev = "<span >prev</span>";
					}
					if ($curpage==$totallinks){
						$last = "<span >Last</span>";
					}
					if ($curpage==1){
						$first = "<span >First</span>";
					}

					$showpage = (($start+$display) > $totalrows) ? $totalrows : ($start+$display);

					$start = $start+1;
					$showing = "Viewing <strong>$start</strong> to <strong>".$showpage."</strong>&nbsp;(<strong>".$totalrows."</strong> Total)";
					$page = "$prev&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;$number|&nbsp;$next";
					return $page.",".$showing.",".$start;
				}

	}

	function pagi_text3($totalrows,$display,$curpage,$start,$nav,$selected){

				$totallinks = ceil($totalrows/$display);
				if ($totallinks<1){
					$next = "";
					$prev = "";
					$pagination = "";
					$page = "";
				}
				else{
					$nextpage = ($curpage>0) ? $curpage+1 : '';
					$prevpage = $curpage-1;
					$number = "";
					for ($x2=1;$x2<=$totallinks;$x2++){
						if ($selected==$x2){
							$number .="<font color=\"#f5ce73\"><strong>".$x2."</strong></font>&nbsp;&nbsp;&nbsp;&nbsp;";
						} else {
							$number .="<a href=".$_SERVER['PHP_SELF']."?".$nav."&page=".$x2." style=\"text-decoration: none; color: #884b20;\">".$x2."</a>&nbsp;&nbsp;&nbsp;&nbsp;";

						}
					}
					//$first = "<a href=".$_SERVER['PHP_SELF']."?".$nav."&nav=1 style=\"text-decoration: none; color: #884b20;\">First</a>";
					$next = '<div class="btn">
							<div class="btn-left"></div>
							<div class="btn-mid"><a href="'.$_SERVER['PHP_SELF'].'?'.$nav.'&nav='.$nextpage.'">Next Page</a></div>
							<div class="btn-right"></div>
						</div>';
					$prev = '<div class="btn">
							<div class="btn-left"></div>
							<div class="btn-mid"><a href="'.$_SERVER['PHP_SELF'].'?'.$nav.'&nav='.$prevpage.'">Previous Page</a></div>
							<div class="btn-right"></div>
						</div>';
					//$next = "<a href=".$_SERVER['PHP_SELF']."?".$nav."&nav=$nextpage style=\"text-decoration: none; color: #884b20;\">next</a>";
					//$prev = "<a href=".$_SERVER['PHP_SELF']."?".$nav."&nav=$prevpage style=\"text-decoration: none; color: #884b20;\">prev</a>";
					//$last = "<a href=".$_SERVER['PHP_SELF']."?".$nav."&nav=$totallinks style=\"text-decoration: none; color: #884b20;\">Last</a>";

					if (($start+$display)>=$totalrows){
						$next = '';
					}
					if ($prevpage==0){
						$prev = '';
					}
					/*if ($curpage==$totallinks){
						$last = "<span >Last</span>";
					}
					if ($curpage==1){
						$first = "<span >First</span>";
					}*/

					$showpage = (($start+$display) > $totalrows) ? $totalrows : ($start+$display);

					$start = $start+1;
					$showing = "Viewing <strong>$start</strong> to <strong>".$showpage."</strong>&nbsp;(<strong>".$totalrows."</strong> Total)";
					$page = $next." ".$prev;
					return $page.",".$showing.",".$start;
				}

	}


}
?>
