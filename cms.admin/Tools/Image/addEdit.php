<?php 
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php'); 
$conn->checklog();
include(ADMIN_TEMPLATE_PATH.'header.php'); 
?>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/tinymce/jquery.tinymce.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/wysiwyg.js"></script>
<?php

$row_cat = $conn->get_array_rs("select * from ".$maintable_prefix."_gallery order by gallery_id desc");

if(isset($_GET['id'])){

    $id = $_GET['id'];

    if(trim($id)!=""){

        $row_content = $conn->array_rs_single("select * from ".$maintable_prefix."_photos WHERE photo_id = '$id' ");

        $type = "Edit";      
    } else {
        $type = "Add";
    }

} else {
     $type = "Add";
}


?>
<div class="mainwrapper">
    
    <?php
        include(ADMIN_TEMPLATE_PATH.'mainHead.php');
        include(ADMIN_TEMPLATE_PATH.'navigation.php');
    ?>


    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="<?php echo SITE_ADMIN_DOMAIN; ?>dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Flashbox</li>
            
            <li class="right">
                <a href="" data-toggle="dropdown" class="dropdown-toggle"><i class="icon-tint"></i> Color Skins</a>
                <ul class="dropdown-menu pull-right skin-color">
                    <li><a href="default">Default</a></li>
                    <li><a href="navyblue">Navy Blue</a></li>
                    <li><a href="palegreen">Pale Green</a></li>
                    <li><a href="red">Red</a></li>
                    <li><a href="green">Green</a></li>
                    <li><a href="brown">Brown</a></li>
                </ul>
            </li>
        </ul>
        
        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <!--h5>Categories</h5-->
                <h1>Image - [<?php echo $type; ?>]</h1>
            </div>
        </div><!--pageheader-->
        
        <div class="maincontent">
            <div class="maincontentinner">
            <?php echo $_SESSION['message']; ?>
            <div class="widgetbox box-inverse">
                <h4 class="widgettitle"><?php echo $type; ?> Image</h4>
                <div class="widgetcontent nopadding">
                    <form class="stdform stdform2" method="post" name="form1" action="<?php echo SITE_ADMIN_DOMAIN; ?>Tools/Image/process.php" enctype="multipart/form-data">
                        <input type="hidden" name="photo_id" id="photo_id" value="<?php echo $row_content['photo_id'] ?>" />
                        <input type="hidden" name="actions" id="actions" value="<?php echo $type; ?>" />
                            <p>
                                <label>Category *</label>
                                <span class="field"><select name="gallery_id" id="selection2" class="uniformselect">
                                    <?php foreach ($row_cat as $value) { ?>
                                        <option value="<?php echo $value['gallery_id']; ?>" <?php if($row_content['gallery_id']==$value['gallery_id']){ echo 'selected="selected"'; } ?>><?php echo $value['gallery_name']; ?></option>
                                    <?php } ?>
                                </select></span>
                            </p>                 
                            <p>
                                <label>Name*</label>
                                <span class="field"><input type="text" name="photo_name" id="photo_name" class="input-xxlarge" value="<?php echo $row_content['photo_name'] ?>" /></span>
                            </p>
                            <p>
                                <label>Image *</label>
                                <span class="field">
                                <input type="file" name="photo_url" class="input-xlarge" value="" />
                                <br />
                                <?php 
                                    if($row_content['photo_url']!=""){
                                        echo '<img src="'.SITE_DOMAIN.$row_content['photo_url'].'" />';
                                    }
                                ?>
                                </span>
                            </p>
                            <p>
                                <label>Thumbs <small>size (200px x 200px)</small></label>
                                <span class="field">
                                <input type="file" name="photo_thumbs" class="input-xlarge" value="" />
                                <br />
                                <?php 
                                    if($row_content['photo_thumbs']!=""){
                                        echo '<img src="'.SITE_DOMAIN.$row_content['photo_thumbs'].'" />';
                                    }
                                ?>
                                </span>
                            </p>
                            <p>
                                <label>Order </label>
                                <span class="field"><input type="text" name="photo_order" id="photo_order" class="input-xxlarge" value="<?php echo $row_content['photo_order'] ?>" /></span>
                            </p>
                            <p>
                                <label>Status</label>
                                <span class="field"><select name="photo_status" id="selection2" class="uniformselect">
                                    <option value="0">Choose One</option>
                                    <option value="0" <?php if($row_content['photo_status']=='0'){ echo 'selected="selected"'; } ?>>Draft</option>
                                    <option value="1" <?php if($row_content['photo_status']=='1'){ echo 'selected="selected"'; } ?>>Publish</option>
                                </select></span>
                            </p>                    
                            <p class="stdformbutton">
                                <input type="submit" class="btn btn-primary" value="Submit Button">
                                <!-- <button class="btn btn-primary">Submit Button</button> -->
                                <button type="reset" class="btn">Reset Button</button>
                            </p>
                    </form>
                </div><!--widgetcontent-->
            </div><!--widget-->                
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->

</div><!--mainwrapper-->
<?php include(ADMIN_TEMPLATE_PATH.'footer.php'); ?>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>