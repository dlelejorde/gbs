<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";

if($_POST['actions']=="Add"){
	extract($_POST);

	if (trim($content_title)== ""){
		$msg .= "Tittle is required.<br />";
		$err++;
	}

	if (trim($content_text)== ""){
		$msg .= "Text is required.<br />";
		$err++;
	}

	if ($err==0){
		//update products table
		$data = array('content_title'=>htmlspecialchars($content_title,ENT_QUOTES),
			'content_page'=>makeUrlFriendly(strtolower($content_title)),
			'content_blurb'=>$content_blurb,
			'content_text'=>htmlspecialchars($elm1,ENT_QUOTES),
			'content_status'=>$content_status,
			'content_added_by'=>$admin,
			'content_date_added'=>strtotime("now"));
		$conn->insert($data,$maintable_prefix.'_content','');			
		$_SESSION['message'] = '<h4 class="widgettitle title-success">New content added. '.$content_title.'</h4>';
		header("Location: index.php?nav=content");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=content");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);

	if (trim($content_title)== ""){
		$msg .= "Tittle is required.<br />";
		$err++;
	}

	if (trim($elm1)== ""){
		$msg .= "Text is required.<br />";
		$err++;
	}

	if ($err==0){

		$slug = makeUrlFriendly(strtolower($content_title));

		extract($_POST);
		//update products table
		$data = array('content_title'=>htmlspecialchars($content_title,ENT_QUOTES),
				'content_page'=>$slug,
				'content_meta_description'=>$content_meta_description,
				'content_meta_keywords'=>$content_meta_keywords,
				'content_text'=>htmlspecialchars($elm1,ENT_QUOTES),
				'content_edited_by'=>$admin,
				'content_date_edited'=>strtotime("now"));
		$conn->update_tbl($data,$maintable_prefix.'_content',"content_id = '".$content_id."'");			
		$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$content_title.' Edited.</h4>';
		header("Location: addEdit.php?nav=content&id=".$slug);	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=content&id=".$slug);	
	}
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){
		$del_qry = $conn->execute_sql("delete from tbl_content where content_id ='".$id."'");
		echo '1';
	} else {
		echo '0';
	}

}