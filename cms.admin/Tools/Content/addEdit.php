<?php 
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php'); 
$conn->checklog();
include(ADMIN_TEMPLATE_PATH.'header.php'); 
?>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/tinymce/jquery.tinymce.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/wysiwyg.js"></script>
<?php
if(isset($_GET['id'])){

    $id = $_GET['id'];

    if(trim($id)!=""){

        $row_content = $conn->array_rs_single("select * from ".$maintable_prefix."_content WHERE content_page = '$id'");

        $type = "Edit";      
    } else {
        $type = "Add";
    }

} else {
     $type = "Add";
}


?>
<div class="mainwrapper">
    
    <?php
        include(ADMIN_TEMPLATE_PATH.'mainHead.php');
        include(ADMIN_TEMPLATE_PATH.'navigation.php');
    ?>

    <?php if($type=="Edit"){ ?>
    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="<?php echo SITE_ADMIN_DOMAIN; ?>dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Content</li>
            
            <li class="right">
                <a href="" data-toggle="dropdown" class="dropdown-toggle"><i class="icon-tint"></i> Color Skins</a>
                <ul class="dropdown-menu pull-right skin-color">
                    <li><a href="default">Default</a></li>
                    <li><a href="navyblue">Navy Blue</a></li>
                    <li><a href="palegreen">Pale Green</a></li>
                    <li><a href="red">Red</a></li>
                    <li><a href="green">Green</a></li>
                    <li><a href="brown">Brown</a></li>
                </ul>
            </li>
        </ul>
        
        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <!--h5>Categories</h5-->
                <h1><?php echo $row_content['content_title'] ?></h1>
            </div>
        </div><!--pageheader-->
        
        <div class="maincontent">
            <div class="maincontentinner">
            <?php echo $_SESSION['message']; ?>
            <div class="widgetbox box-inverse">
                <h4 class="widgettitle"><?php echo $type; ?> Content</h4>
                <div class="widgetcontent nopadding">
                    <form class="stdform stdform2" method="post" name="form1" action="<?php echo SITE_ADMIN_DOMAIN; ?>Tools/Content/process.php">
                        <input type="hidden" name="content_id" id="content_id" value="<?php echo $row_content['content_id'] ?>" />
                        <input type="hidden" name="actions" id="actions" value="<?php echo $type; ?>" />
                            <p>
                                <label>Title*</label>
                                <span class="field"><input type="text" name="content_title" id="content_title" class="input-xxlarge" value="<?php echo $row_content['content_title'] ?>" /></span>
                            </p>
                            
                            <p>
                                <label>Meta Description <small>For SEO purposes.</small></label>
                                <span class="field"><input type="text" name="content_meta_description" id="content_meta_description" class="input-xxlarge" value="<?php echo $row_content['content_meta_keywords'] ?>" /></span>
                            </p>

                            <p>
                                <label>Meta Keywords <small>For SEO purposes.</small></label>
                                <span class="field"><input type="text" name="content_meta_keywords" id="content_meta_keywords" class="input-xxlarge" value="<?php echo $row_content['content_meta_keywords'] ?>" /></span>
                            </p>
                            
                            <p>
                                <label>Text* <small>Category landing page main content.</small></label>
                                <span class="field">
                                    <textarea id="elm1" name="elm1" rows="15" cols="80" style="width: 80%" class="tinymce">
                                        <?php echo $row_content['content_text'] ?>
                                    </textarea>
                                </span>
                            </p>
                                                    
                            <p class="stdformbutton">
                                <input type="submit" class="btn btn-primary" value="Submit Button">
                                <!-- <button class="btn btn-primary">Submit Button</button> -->
                                <button type="reset" class="btn">Reset Button</button>
                            </p>
                    </form>
                </div><!--widgetcontent-->
            </div><!--widget-->                
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->
    <?php } ?>
</div><!--mainwrapper-->
<?php include(ADMIN_TEMPLATE_PATH.'footer.php'); ?>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>