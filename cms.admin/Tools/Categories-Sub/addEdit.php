<?php 
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php'); 
$conn->checklog();
include(ADMIN_TEMPLATE_PATH.'header.php'); 

if(isset($_GET['id'])){

    $id = (int)$_GET['id'];

    if($id>0){

        $row_content = $conn->array_rs_single("select * from tbl_channels WHERE channels_id = '$id'");

        $type = "Edit";      
    } else {
        $type = "Add";
    }

} else {
     $type = "Add";
}


?>
<div class="mainwrapper">
    
    <?php
        include(ADMIN_TEMPLATE_PATH.'mainHead.php');
        include(ADMIN_TEMPLATE_PATH.'navigation.php');
    ?>

    
    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="<?php echo SITE_ADMIN_DOMAIN; ?>dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Sub Categories</li>
            
            <li class="right">
                <a href="" data-toggle="dropdown" class="dropdown-toggle"><i class="icon-tint"></i> Color Skins</a>
                <ul class="dropdown-menu pull-right skin-color">
                    <li><a href="default">Default</a></li>
                    <li><a href="navyblue">Navy Blue</a></li>
                    <li><a href="palegreen">Pale Green</a></li>
                    <li><a href="red">Red</a></li>
                    <li><a href="green">Green</a></li>
                    <li><a href="brown">Brown</a></li>
                </ul>
            </li>
        </ul>
        
        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <!--h5>Categories</h5-->
                <h1>Sub Categories[<?php echo $type; ?>]</h1>
            </div>
        </div><!--pageheader-->
        
        <div class="maincontent">
            <div class="maincontentinner">
            <?php echo $_SESSION['message']; ?>
            <div class="widgetbox box-inverse">
                <h4 class="widgettitle"><?php echo $type; ?> Category</h4>
                <div class="widgetcontent nopadding">
                    <form class="stdform stdform2" method="post" name="form1" action="<?php echo SITE_ADMIN_DOMAIN; ?>Tools/Categories-Sub/process.php">
                        <input type="hidden" name="channels_id" id="channels_id" value="<?php echo $row_content['channels_id'] ?>" />
                        <input type="hidden" name="actions" id="actions" value="<?php echo $type; ?>" />
                             <p>
                                <label>Parent Category</label>
                                <span class="field">
                                    <select name="channels_parent_id" id="channels_parent_id" class="uniformselect">
                                        <option value="0">Choose One</option>
                                        <?php 
                                            $getParent = $conn->get_array_rs("select * from tbl_channels WHERE channels_parent_id = 0 order by channels_id desc");
                                            foreach ($getParent as $value) {
                                        ?>
                                            <option value="<?php echo $value['channels_id']; ?>" <?php if($row_content['channels_parent_id']== $value['channels_id']){ echo 'selected="selected"'; } ?>><?php echo $value['channels_name']; ?></option>
                                        <?php
                                            }
                                        ?>

                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>Category Name <small>Text that will appear on manu bar.</small></label>
                                <span class="field"><input type="text" name="channels_name" id="channels_name" class="input-xxlarge" value="<?php echo $row_content['channels_name'] ?>" /></span>
                            </p>
                            
                            <p>
                                <label>Category Title <small>Text that will appear on title bar.</small></label>
                                <span class="field"><input type="text" name="channels_title" id="channels_title" class="input-xxlarge" value="<?php echo $row_content['channels_title'] ?>" /></span>
                            </p>
                            
                            <p>
                                <label>Category Description <small>Category landing page Meta Description.</small></label>
                                <span class="field"><textarea cols="80" rows="5" name="channels_description" id="channels_description" class="span5"><?php echo $row_content['channels_description'] ?></textarea></span>
                            </p>

                            <p>
                                <label>Category Keywords <small>Category landing page Meta Keywords.</small></label>
                                <span class="field"><textarea cols="80" rows="5" name="channels_keywords" id="channels_keywords" class="span5"><?php echo $row_content['channels_keywords'] ?></textarea></span>
                            </p>
                            
                            <p>
                                <label>Status</label>
                                <span class="field"><select name="channels_status" id="selection2" class="uniformselect">
                                    <option value="0">Choose One</option>
                                    <option value="0" <?php if($row_content['channels_status']=='0'){ echo 'selected="selected"'; } ?>>Draft</option>
                                    <option value="1" <?php if($row_content['channels_status']=='1'){ echo 'selected="selected"'; } ?>>Publish</option>
                                </select></span>
                            </p>
                                                    
                            <p class="stdformbutton">
                                <input type="submit" class="btn btn-primary" value="Submit Button">
                                <!-- <button class="btn btn-primary">Submit Button</button> -->
                                <button type="reset" class="btn">Reset Button</button>
                            </p>
                    </form>
                </div><!--widgetcontent-->
            </div><!--widget-->                
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->
    
</div><!--mainwrapper-->
<?php include(ADMIN_TEMPLATE_PATH.'footer.php'); ?>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>