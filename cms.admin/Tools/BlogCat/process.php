<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";

if($_POST['actions']=="Add"){
	extract($_POST);

	if (trim($cat_name)== ""){
		$msg .= "name is required.<br />";
		$err++;
	}


	/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
	if((!empty($_FILES["cat_image"])) && ($_FILES['cat_image']['error'] == 0)) {
		
		//Check if the file is JPEG image and it's size is less than 4mb

		$filename = basename($_FILES['cat_image']['name']);
		$ext = substr($filename, strrpos($filename, '.') + 1);

		if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["cat_image"]["size"] < 4194304)) {

		//Determine the path to which we want to save this file
		  $newname = SITE_PATH.'images/'.time().'-'.$filename;
		  $filename = '/images/'.time().'-'.$filename;
		  //Check if the file with the same name is already exists on the server
		  if (!file_exists($newname)) {
			//Attempt to move the uploaded file to it's new place
			if ((move_uploaded_file($_FILES['cat_image']['tmp_name'],$newname))) {

				$cat_image = $filename;

			} else {
			   $msg .= "Error: A problem occurred during file upload!";
			   $err++;
			}
		  } else {
			 $msg .= "Error: File ".$_FILES["cat_image"]["name"]." already exists";
			 $err++;
		  }
		} else {
		 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
		 $err++;
		}
		
	} else {
		$msg .= "Inage is required.<br />";
		$err++;
	}				


	if ($err==0){

		//insert flashbox table
		$data = array('cat_name'=>htmlspecialchars($cat_name,ENT_QUOTES),
			'cat_slug'=>makeUrlFriendly(strtolower($cat_name)),
			'cat_image'=>$cat_image,
			'cat_added_by'=>$admin,
			'cat_date_added'=>strtotime("now"));
		$conn->insert($data,$maintable_prefix.'_media_category','');		

		$_SESSION['message'] = '<h4 class="widgettitle title-success">New image added. '.$cat_name.'</h4>';
		header("Location: index.php?nav=caterers");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=caterers");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);


	if (trim($cat_name)== ""){
		$msg .= "Name is required.<br />";
		$err++;
	}


	if ($err==0){

		//update flashbox table
		$data = array('cat_name'=>htmlspecialchars($cat_name,ENT_QUOTES),
			'cat_slug'=>makeUrlFriendly(strtolower($cat_name)),
			'cat_edited_by'=>$admin,
			'cat_date_edited'=>strtotime("now"));
		$conn->update_tbl($data,$maintable_prefix.'_media_category',"cat_id = '".$cat_id."'");		

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["cat_image"])) && ($_FILES['cat_image']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['cat_image']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["cat_image"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/'.time().'-'.$filename;
			  $filename = '/images/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['cat_image']['tmp_name'],$newname))) {

					$cat_image = $filename;
					$data = array('cat_image'=>$cat_image);
					$conn->update_tbl($data,$maintable_prefix.'_media_category',"cat_id = '".$cat_id."'");

				} else {
				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["cat_image"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
		} else {

		}				
		
		$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$cat_name.' Edited.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=bc&id=".$cat_id);	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=bc&id=".$cat_id);	
	}
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){

		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_media_category where cat_id ='".$id."'");
		echo '1';
	} else {
		echo '0';
	}

}