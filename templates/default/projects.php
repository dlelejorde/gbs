            <section class="padHead projects sectionLanding">

                <div class="fullInner">
                    <div class="orgImg"><img src="<?php echo TEMPLATE_DOMAIN_IMAGES; ?>landing-banner.jpg" /></div>
                    <div class="orgDetails">
                        <div class="fTitle">PROJECTS</div>
                        <p>
                            Holisticly target cross-platform web-readiness for user-centric leadership. Phosfluorescently orchestrate open-source growth strategies before highly efficient systems. 
                        </p>
                    </div>

                </div>
                

                <div class="inner org">

                    <div class="padTB">

                        <div class="results projects">
                            <!--div class="result-title">Projects</div>
                            <div class="wiggle-black"></div-->
                    
                            <div class="result-container">
                                <div class="result-list">
                                    <a href="#" class="media">
                                        <div class="img"><img src="images/th.jpg" /></div>
                                        <div class="bd">
                                            <div class="org-name">Organization Name</div>
                                            <div class="org-details f16">
                                                <span class="bold">Project:</span>  Insert client name here<br />
                                                <span class="bold">Country:</span> Insert terms here<br />
                                                <span class="bold">Focus Area:</span> Environment Water and Climate Change
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="result-list">
                                    <a href="#" class="media">
                                        <div class="img"><img src="images/th.jpg" /></div>
                                        <div class="bd">
                                            <div class="org-name">Organization Name</div>
                                            <div class="org-details f16">
                                                <span class="bold">Project:</span>  Insert client name here<br />
                                                <span class="bold">Country:</span> Insert terms here<br />
                                                <span class="bold">Focus Area:</span> Environment Water and Climate Change
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="result-list">
                                    <a href="#" class="media">
                                        <div class="img"><img src="images/th.jpg" /></div>
                                        <div class="bd">
                                            <div class="org-name">Organization Name</div>
                                            <div class="org-details f16">
                                                <span class="bold">Project:</span>  Insert client name here<br />
                                                <span class="bold">Country:</span> Insert terms here<br />
                                                <span class="bold">Focus Area:</span> Environment Water and Climate Change
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="result-list">
                                    <a href="#" class="media">
                                        <div class="img"><img src="images/th.jpg" /></div>
                                        <div class="bd">
                                            <div class="org-name">Organization Name</div>
                                            <div class="org-details f16">
                                                <span class="bold">Project:</span>  Insert client name here<br />
                                                <span class="bold">Country:</span> Insert terms here<br />
                                                <span class="bold">Focus Area:</span> Environment Water and Climate Change
                                            </div>
                                        </div>
                                    </a>
                                </div>

                            </div>
                            <div class="result-pagination textCenter f16 bold">
                                <a href="#"><span>1</span></a>
                                <a href="#"><span>2</span></a>
                                <a href="#"><span>3</span></a>
                                <a href="#"><span>4</span></a>
                                <a href="#"><span>5</span></a>
                            </div>
                        </div>

                    </div>

                </div>
            </section>

