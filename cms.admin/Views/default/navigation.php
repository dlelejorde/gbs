    <div class="leftpanel">

        <div class="leftmenu">
            <ul class="nav nav-tabs nav-stacked">
                <li class="nav-header">Navigation</li>
                <li><a href="<?php echo ADMIN_TOOLS_DOMAIN; ?>FocusArea/index.php?nav=focusarea"><span class="iconfa-th-list"></span>Focus Area</a></li>
                <li><a href="<?php echo ADMIN_TOOLS_DOMAIN; ?>Sector/index.php"><span class="iconfa-th-list"></span>Sector</a></li>
                <li><a href="<?php echo ADMIN_TOOLS_DOMAIN; ?>Organization/index.php"><span class="iconfa-th-list"></span>Organization</a></li>
                <li><a href="<?php echo ADMIN_TOOLS_DOMAIN; ?>Projects/index.php"><span class="iconfa-th-list"></span>Projects</a></li>
                <li><a href="<?php echo ADMIN_TOOLS_DOMAIN; ?>Users/index.php"><span class="iconfa-book"></span>Registered Users</a></li>
            </ul>
        </div><!--leftmenu-->

    </div><!-- leftpanel -->
