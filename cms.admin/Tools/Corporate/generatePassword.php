<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";

$id = $_POST['id'];
$password = generatePassword();


$data = array('inq_generate_password'=>$password);
$conn->update_tbl($data,$maintable_prefix.'_request_password',"inq_id = '".$id."'");	

$row_content = $conn->array_rs_single("select * from ".$maintable_prefix."_request_password WHERE inq_id = '$id' ");


$recipient = $row_content['inq_email'];

$subject = "CommonThread Request Password: Generated Password for ".$row_content['inq_name'];

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n" .
'From: CommonThread' . "\r\n" .
'X-Mailer: PHP/' . phpversion();

$messages = "
<html>
    <head>
        <title> Generated Password for ".$row_content['inq_name']."</title>
    </head>
    <body>
       	Your password is ".$password."<br /><br />
       	Use this password to access the corporate page.<br /><br />

       	Thank you!
    </body>
</html>";

mail($recipient, $subject, $messages, $headers);


$recipient2 = 'ramilevangelista2009@gmail.com, kelvst@gmail.com, contact@onethreadinternational.com';

$subject2 = "CommonThread Request Password: Generated Password for ".$row_content['inq_name'];

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n" .
'From: CommonThread' . "\r\n" .
'X-Mailer: PHP/' . phpversion();

$messages2 = "
<html>
    <head>
        <title>Generated Password for ".$row_content['inq_name']."</title>
    </head>
    <body>
        <p>
            Hi Admin!<br /><br />

            ".$row_content['inq_name']."'s generated password is ".$password."<br /><br />
        </p>
    </body>
</html>";

mail($recipient2, $subject2, $messages2, $headers);


echo 'Password Generated. The requester will receive his/her password.';


function generatePassword($length = 8, $letters = '1234567890QWERTYUIOPASDFGHJKLZXCVBNM'){
    $s = '';

    $lettersLength = strlen($letters)-1;

    for($i = 0 ; $i < $length ; $i++)
    {
        $s .= $letters[rand(0,$lettersLength)];
    }

    return $s;
} 