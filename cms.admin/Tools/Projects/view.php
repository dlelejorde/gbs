<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

include(ADMIN_TEMPLATE_PATH.'header.php');

$id = $_GET['id'];

$row_content = $conn->array_rs_single("select * from ".$maintable_prefix."_projects WHERE project_id = '$id'");
?>
<div class="mainwrapper">

    <?php
        include(ADMIN_TEMPLATE_PATH.'mainHead.php');
        include(ADMIN_TEMPLATE_PATH.'navigation.php');
    ?>


    <div class="rightpanel">

        <ul class="breadcrumbs">
            <li><a href="<?php echo SITE_ADMIN_DOMAIN; ?>dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Projects</li>
        </ul>

        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <!--h5>Categories</h5-->
                <h1>Projects</h1>
            </div>
        </div><!--pageheader-->

        <div class="maincontent">
            <div class="maincontentinner">
            <?php echo $_SESSION['message']; ?>
            <div class="widgetbox box-inverse">
                <h4 class="widgettitle"><?php echo $type; ?> Logo</h4>
                <div class="widgetcontent nopadding">
                	<form class="stdform stdform2" method="post" name="form1"  enctype="multipart/form-data">
		                <p>
		                    <label>Organization Name</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo getMemberData($row_content['user_id'], 'organization_name'); ?>" /></span>
		                </p>
		                <p>
		                    <label>Project Name</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['project_name'] ?>" /></span>
		                </p>
                        <p>
                            <label>Focus Area</label>
                            <span class="field"><input type="text" class="input-xlarge" value="<?php echo getTableSingleData('yfa_focus_area', 'id', $row_content['focus_area'], 'fa_name') ?>" /></span>
                        </p>
                        <p>
                            <label>Country</label>
                            <span class="field"><input type="text" class="input-xlarge" value="<?php echo getCountryName($row_content['country_id']) ?>" /></span>
                        </p>
		                <p>
		                    <label>Term From</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['term_from'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Term To</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['term_to'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Value From</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['value_from'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Value To</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['value_to'] ?>" /></span>
		                </p>

		                <p>
		                    <label>About</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['about'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Contact Person</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['contact_person'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Contact Email</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['contact_email'] ?>" /></span>
		                </p>
                        <p>
                            <label>Position</label>
                            <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['position'] ?>" /></span>
                        </p>
                        <p>
                            <label>Phone Number</label>
                            <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['phone_number'] ?>" /></span>
                        </p>

	                </form>
                </div><!--widgetcontent-->
            </div><!--widget-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
<?php include(ADMIN_TEMPLATE_PATH.'footer.php'); ?>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>
