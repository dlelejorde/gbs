<?php 
/************************************************************************************** 
* Class: Pager 
**************************************************************************************/ 
class Pager { 
	/*********************************************************************************** 
	 * int findStart (int limit) 
	 * Returns the start offset based on $_GET['page'] and $limit 
	 ***********************************************************************************/ 
	
	function getPage(){
		$urlpieces = explode("/", $_SERVER['REQUEST_URI']);;
		$requestURL = '';
		for($x=0; $x < count($urlpieces);$x++){															
			if($urlpieces[$x]=='page'){	break;	}
			if(trim($urlpieces[$x]) !=''){ $requestURL .= $urlpieces[$x].'/'; }										
		}
		return $requestURL;
	}
	function findStart($limit) { 
		if ((!isset($_GET['page'])) || ($_GET['page'] == "1")) { 
			$start = 0; $_GET['page'] = 1; 
		} else { 
			$start = ($_GET['page']-1) * $limit; 
		} 
		return $start; 
	} 
	/*********************************************************************************** 
	 * int findPages (int count, int limit) 
	 * Returns the number of pages needed based on a count and a limit 
	 ***********************************************************************************/ 
	function findPages($count, $limit) { 
		$pages = (($count % $limit) == 0) ? $count / $limit : floor($count / $limit) + 1; 
		return $pages; 
	} 
	/*********************************************************************************** 
	* string pageList (int curpage, int pages) 
	* Returns a list of pages in the format of "� < [pages] > �" 
	***********************************************************************************/ 
	function pageList($getvars, $pages, $domain, $url) {
		$curpage = $getvars['page'];
		$requestPage = $domain.$url;

		foreach ($getvars as $key => $val) {
			if($key != 'page') $get_vars .="&$key=$val";
		}

		$pageFormat = '/page/';
		$page_list  = ""; 
	
		/* Print the first and previous page links if necessary */ 
		if (($curpage != 1) && ($curpage)){
			//$page_list .= '  <a href="'.$requestPage.$pageFormat.'1'.'" title="First Page" class="navleft"  ><img src="'.TEMPLATES_URI_BBC.'images/left-arrow.png" /></a> '; 
		}else{
			//$page_list .= '  <span title="First Page" class="navleft"><img src="'.TEMPLATES_URI_BBC.'images/left-arrow.png" /></span> '; 
		}

		if (($curpage-1) > 0){
			$page_list .= '  <a href="'.$requestPage.$pageFormat.($curpage-1).'" title="Previous Page" class="prev" >Previous</a> '; 
		}else{
			$page_list .= '  <span title="Previous Page" class="prev">Previous</span> '; 
		}
	
		/* Print the numeric page list; make the current page unlinked and bold */ 
		if($pages <= 5){
			for ($i=1; $i<=$pages; $i++){ 
				if($i == $curpage){ $page_list .= '<strong  class="page_num">'.$i.'</strong>'; }
				else{ $page_list .= '<a href="'.$requestPage.$pageFormat.$i.'" title="Page '.$i.'"  class="page_num">'.$i.'</a>'; }
				$page_list .= " "; 
			}
		}else{
			if(($curpage >= 3) && ($curpage <= ($pages-2))){			
				for ($i=($curpage-2); $i<=($curpage+2); $i++){ 
					if ($i == $curpage){ $page_list .= '<strong  class="page_num">'.$i.'</strong>'; }
					else{ $page_list .= '<a href="'.$requestPage.$pageFormat.$i.'" title="Page '.$i.'"  class="page_num">'.$i.'</a>'; }
					$page_list .= " "; 
				}
			}
			if($curpage < 3){
				for ($i=1; $i<=5; $i++){ 
					if ($i == $curpage){ $page_list .= '<strong  class="page_num">'.$i.'</strong>'; }
					else{ $page_list .= '<a href="'.$requestPage.$pageFormat.$i.'" title="Page '.$i.'"  class="page_num">'.$i.'</a>'; }
					$page_list .= " "; 
				}
			}
			if($curpage > ($pages-2)){			
				for ($i=($pages-4); $i<=$pages; $i++){ 
					if ($i == $curpage){ $page_list .= '<strong  class="page_num">'.$i.'</strong>'; }
					else{ $page_list .= '<a href="'.$requestPage.$pageFormat.$i.'" title="Page '.$i.'"  class="page_num">'.$i.'</a>'; }
					$page_list .= " "; 
				}
			}
		}
		/* Print the Next and Last page links if necessary */ 
		if (($curpage+1) <= $pages){
			$page_list .= '  <a href="'.$requestPage.$pageFormat.($curpage+1).'" title="Next Page" class="next"  >Next</a> '; 
		}else{
			$page_list .= '  <span title="Next Page" class="next">Next</span> '; 
		}
	
		if (($curpage != $pages) && ($pages != 0)){  
			//$page_list .= '  <a href="'.$requestPage.$pageFormat.$pages.'" title="Last Page" class="navright"  ><img src="'.TEMPLATES_URI_BBC.'images/right-arrow.png" /></a> '; 
		}else{
			//$page_list .= '  <span title="Last Page" class="navright"><img src="'.TEMPLATES_URI_BBC.'images/right-arrow.png" /></span> '; 
		}

		return $page_list; 
	} 
} 
?> 