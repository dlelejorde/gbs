<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";

if($_POST['actions']=="Add"){
	extract($_POST);

	if (trim($title)== ""){
		$msg .= "Tittle is required.<br />";
		$err++;
	}

	if (trim($product_id)== "0"){
		$msg .= "Product is required.<br />";
		$err++;
	}

	if (trim($blurb)== ""){
		$msg .= "Blurb is required.<br />";
		$err++;
	}


	/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
	if((!empty($_FILES["image"])) && ($_FILES['image']['error'] == 0)) {
		
		//Check if the file is JPEG image and it's size is less than 4mb

		$filename = basename($_FILES['image']['name']);
		$ext = substr($filename, strrpos($filename, '.') + 1);

		if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["image"]["size"] < 4194304)) {

		//Determine the path to which we want to save this file
		  $newname = SITE_PATH.'images/'.mktime().'-'.$filename;
		  $filename = '/images/'.mktime().'-'.$filename;
		  //Check if the file with the same name is already exists on the server
		  if (!file_exists($newname)) {
			//Attempt to move the uploaded file to it's new place
			if ((move_uploaded_file($_FILES['image']['tmp_name'],$newname))) {

				$image = $filename;

			} else {
			   $msg .= "Error: A problem occurred during file upload!";
			   $err++;
			}
		  } else {
			 $msg .= "Error: File ".$_FILES["image"]["name"]." already exists";
			 $err++;
		  }
		} else {
		 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
		 $err++;
		}
		
	} else {
		$msg .= "Inage is required.<br />";
		$err++;
	}				


	if ($err==0){

		//insert flashbox table
		$data = array('title'=>$title,
			'blurb'=>$blurb,
			'product_id'=>$product_id,
			'image'=>$image);
		$conn->insert($data,$maintable_prefix.'_featured_home','');	
		$id = $conn->get_last_id($maintable_prefix.'_featured_home','id');			

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["logo"])) && ($_FILES['logo']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['logo']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["logo"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/'.mktime().'-'.$filename;
			  $filename = '/images/'.mktime().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['logo']['tmp_name'],$newname))) {

					$logo = $filename;
					$data = array('logo'=>$logo);
					$conn->update_tbl($data,$maintable_prefix.'_featured_home',"id = '".$id."'");

				} else {
				   $msg .= "Error: A problem occurred during file upload!";
				   $err++;
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["logo"]["name"]." already exists";
				 $err++;
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			 $err++;
			}
			
		} else {

		}				

		$_SESSION['message'] = '<h4 class="widgettitle title-success">New featured added. '.$title.'</h4>';
		header("Location: index.php?nav=options");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=options");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);


	if (trim($title)== ""){
		$msg .= "Tittle is required.<br />";
		$err++;
	}


	if ($err==0){

		//update flashbox table
		$data = array('title'=>$title,
			'blurb'=>$blurb,
			'product_id'=>$product_id);
		$conn->update_tbl($data,$maintable_prefix.'_featured_home',"id = '".$id."'");		

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["image"])) && ($_FILES['image']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['image']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["image"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/'.mktime().'-'.$filename;
			  $filename = '/images/'.mktime().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['image']['tmp_name'],$newname))) {

					$image = $filename;
					$data = array('image'=>$image);
					$conn->update_tbl($data,$maintable_prefix.'_featured_home',"id = '".$id."'");

				} else {
				   $msg .= "Error: A problem occurred during file upload!";
				   $err++;
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["image"]["name"]." already exists";
				 $err++;
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			 $err++;
			}
			
		} else {

		}				

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["logo"])) && ($_FILES['logo']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['logo']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["logo"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/'.mktime().'-'.$filename;
			  $filename = '/images/'.mktime().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['logo']['tmp_name'],$newname))) {

					$logo = $filename;
					$data = array('logo'=>$logo);
					$conn->update_tbl($data,$maintable_prefix.'_featured_home',"id = '".$id."'");

				} else {
				   $msg .= "Error: A problem occurred during file upload!";
				   $err++;
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["logo"]["name"]." already exists";
				 $err++;
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			 $err++;
			}
			
		} else {

		}				
		
		$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$title.' Edited.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=options&id=".$id);	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=options&id=".$id);	
	}
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){
		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_featured_home where id ='".$id."'");
		echo '1';
	} else {
		echo '0';
	}

}