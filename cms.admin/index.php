<?php include('config/variables.php'); ?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title><?php echo ADMIN_TITLE; ?></title>

<style type="text/css">
    @import url('css/roboto.css');
    @import url('css/lato.css');
    body.loginpage {
        background: #0866c6;
        font-family: 'RobotoRegular', 'Helvetica Neue', Helvetica, sans-serif;
        font-size: 12px;
    }
    input, select, textarea, button {
        outline: none;
        font-size: 13px;
        font-family: 'RobotoRegular', 'Helvetica Neue', Helvetica, sans-serif;
    }
    .inputwrapper input {
        border: 0;
        padding: 10px;
        background: #fff;
    }
    .inputwrapper button {
        display: block;
        border: 1px solid #0c57a3;
        padding: 10px;
        background: #0972dd;
        width: 100%;
        color: #fff;
        text-transform: uppercase;
    }
    .inputwrapper label {
        display: inline-block;
        margin-top: 10px;
        color: rgba(255,255,255,0.8);
        font-size: 11px;
        vertical-align: middle;
    }
    .loginpanelinner {
        width: 270px;
        margin: 50px auto 0px;
    }
    .loginfooter {
        font-size: 11px;
        color: rgba(255,255,255,0.5);
        position: absolute;
        position: fixed;
        bottom: 0;
        left: 0;
        width: 100%;
        text-align: center;
        font-family: arial, sans-serif !important;
        padding: 5px 0;
    }

    #username, #password{
        width: 250px;
        margin-bottom: 10px;
    }
    .alert { border-color: #e4bf7f; color: #9c6c38; margin-bottom: 15px; background: rgb(246,237,186); }
    .alert .close { top: 0; right: -23px; color: #937f0e; }
    .alert h4 { color: #9c6c38; }

    .alert-error { border-color: #e18d9a; color:#da5251; background: rgb(246,216,216); }
    .alert-error .close, .alert-error h4 { color: #990000; }
    .alert-success { border-color: #b4da95; color: #468847; background: rgb(223,240,216); }
    .alert-success .close, .alert-success h4 { color: #468847; }
    .alert-info { border-color: #88c4e2; color: #3a87ad; background: rgb(217,237,247); }
    .alert-info .close, .alert-info h4 { color: #3a87ad; }

    .login-alert .alert {
        font-size: 11px;
        text-align: center;
        padding: 5px 0;
        border: 0;
    }


</style>

<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/jquery-1.9.1.min.js"></script>


</head>

<body class="loginpage">

<div class="loginpanel">
    <div class="loginpanelinner">
        <div class="logo"><!--img src="images/logo.png" alt="" /--></div>
        <form name="login" id="login" action="<?php echo ADMIN_LIBRARIES_DOMAIN.'Login.php'; ?>" method="post">
            <div class="inputwrapper login-alert" style="<?php if (isset($_SESSION['message'])){ echo 'display: block;'; } else { echo 'display: none;'; } ?>">
                <div class="alert alert-error">Invalid username or password</div>
            </div>
            <div class="inputwrapper">
                <input type="text" name="uname" id="username" placeholder="Enter any username" />
            </div>
            <div class="inputwrapper">
                <input type="password" name="pwrd" id="password" placeholder="Enter any password" />
            </div>
            <div class="inputwrapper">
                <button name="submit">Sign In</button>
                <input type="hidden" name="log" value="1">
            </div>
            <div class="inputwrapper">
                <label><input type="checkbox" class="remember" name="signin" /> Keep me sign in</label>
            </div>
            
        </form>
    </div><!--loginpanelinner-->
</div><!--loginpanel-->

<div class="loginfooter">
    <p>&copy; 2014. Oasis. All Rights Reserved.</p>
</div>

</body>
</html>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>