<?php
	 /** Created on: 09-01-2013 ram
	 *  ------------------------------------------------------------------
	 *  SOFTWARE NAME: Skills Collide backend
	 *  SOFTWARE RELEASE: 1.0 Beta
	 *  COPYRIGHT NOTICE: Copyright (C) 2013
	 *  -------------------------------------------------------------------
	 *
	 *  @filename	  Login.php
	 *  @description  Login Page
	 *  @author       ramilevangelista2009@gmail.com
	 *
	 */
	
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');


$err_msg = " ";
	if($_POST["log"]=="1"){
		$username = $_POST['uname'];
		$password = $_POST['pwrd'];
		$login = $conn->login($username,$password);
		if($login==1){
			//$logins = $conn->array_rs_single("select uNumLogins from ".USERSTBL." where uID = '$_SESSION[uID]'");
			//$count = $logins['uNumLogins'] + 1;
			//$data = array("uNumLogins"=>$count);
			//$conn->execute_sql("update ".USERSTBL." set uNumLogins = '$count' where uID = '$_SESSION[uID]'");
			if ($_SESSION['uSuper']!=0){
				header("Location: ".SITE_ADMIN_DOMAIN."dashboard.php");
			} else {
				$_SESSION['message'] = "Invalid username and/or password!";
				header("Location: ".SITE_ADMIN_DOMAIN."index.php");
			}	
			
		} else {
			$_SESSION['message'] = "Invalid username and/or password!";
			header("Location: ".SITE_ADMIN_DOMAIN."index.php");
		}
	
	}

?>