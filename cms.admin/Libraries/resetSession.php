<?php

	 /** Created on: 09-01-2013 ram
	 *  ------------------------------------------------------------------
	 *  SOFTWARE NAME: Skills Collide backend
	 *  SOFTWARE RELEASE: 1.0 Beta
	 *  COPYRIGHT NOTICE: Copyright (C) 2013
	 *  -------------------------------------------------------------------
	 *
	 *  @filename	  resetSession.php
	 *  @description  Unset Messages Session
	 *  @author       ramilevangelista2009@gmail.com
	 *
	 */
	
unset($_SESSION['message']);

?>