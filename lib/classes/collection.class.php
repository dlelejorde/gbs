<?php
class Collections{

	/*
	function name: getCollectionImage
	Params
		$brandID: brand_id
	Return
		collection array
	*/
	public function getCollectionImage($brandID){
		$strCollection = "SELECT 
			c.collection_brand,
			c.collection_name,
			c.collection_id,
			c.collection_slug,
			b.brand_name,
			p.prod_thumbs
			FROM ".DB_NAME.".".TABLE_PREFIX."_collection c
			LEFT JOIN ".DB_NAME.".".TABLE_PREFIX."_products p ON c.collection_id = p.collection_id
			LEFT JOIN ".DB_NAME.".".TABLE_PREFIX."_brand b ON c.collection_brand = b.brand_id
			WHERE c.collection_brand = ".$brandID."
			GROUP BY c.collection_id
			ORDER BY c.collection_id DESC";
		
		$resultCollection = Database::getRow($strCollection);
		
		return $resultCollection;
	}


	/*
	function name: getCollectionLatest
	Params
		$brandID: brand_id
	Return
		collection array
	*/
	public function getCollectionLatest(){
		$strCollection = "SELECT 
			c.collection_brand,
			c.collection_name,
			c.collection_id,
			c.collection_slug,
			b.brand_name,
			p.prod_thumbs
			FROM ".DB_NAME.".".TABLE_PREFIX."_collection c
			LEFT JOIN ".DB_NAME.".".TABLE_PREFIX."_products p ON c.collection_id = p.collection_id
			LEFT JOIN ".DB_NAME.".".TABLE_PREFIX."_brand b ON c.collection_brand = b.brand_id
			LEFT JOIN ".DB_NAME.".".TABLE_PREFIX."_collection_country cc ON c.collection_id = cc.collection_id
			WHERE 
			cc.country_id = ".$_SESSION['Country']."
			GROUP BY c.collection_id
			ORDER BY c.collection_id DESC LIMIT 8";
		
		$resultCollection = Database::getResult($strCollection);
		
		return $resultCollection;
	}


}
?>