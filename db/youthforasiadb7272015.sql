-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 27, 2015 at 02:57 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `youthforasiadb`
--

-- --------------------------------------------------------

--
-- Table structure for table `yfa_countries`
--

DROP TABLE IF EXISTS `yfa_countries`;
CREATE TABLE IF NOT EXISTS `yfa_countries` (
  `code` char(2) NOT NULL DEFAULT '',
  `name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Countries of the world';

--
-- Dumping data for table `yfa_countries`
--

INSERT INTO `yfa_countries` (`code`, `name`) VALUES
('AF', 'Afghanistan'),
('AL', 'Albania'),
('DZ', 'Algeria'),
('AD', 'Andorra'),
('AO', 'Angola'),
('AI', 'Anguilla'),
('AQ', 'Antarctica'),
('AG', 'Antigua and Barbuda'),
('AR', 'Argentina'),
('AM', 'Armenia'),
('AW', 'Aruba'),
('AU', 'Australia'),
('AT', 'Austria'),
('AZ', 'Azerbaijan'),
('BS', 'Bahamas'),
('BH', 'Bahrain'),
('BD', 'Bangladesh'),
('BB', 'Barbados'),
('BY', 'Belarus'),
('BE', 'Belgium'),
('BZ', 'Belize'),
('BJ', 'Benin'),
('BM', 'Bermuda'),
('BT', 'Bhutan'),
('BO', 'Bolivia'),
('BW', 'Botswana'),
('BV', 'Bouvet Island'),
('BR', 'Brazil'),
('IO', 'British Indian Ocean Territory'),
('BN', 'Brunei'),
('BG', 'Bulgaria'),
('BF', 'Burkina Faso'),
('BI', 'Burundi'),
('KH', 'Cambodia (Kampuchea)'),
('CM', 'Cameroon'),
('CA', 'Canada'),
('CV', 'Cape Verde'),
('KY', 'Cayman Islands'),
('CF', 'Central African Republic'),
('TD', 'Chad'),
('CL', 'Chile'),
('CN', 'China'),
('CX', 'Christmas Island'),
('CC', 'Cocos (Keeling) Islands'),
('CO', 'Colombia'),
('KM', 'Comoro Islands'),
('CG', 'Congo'),
('CK', 'Cook Islands'),
('CR', 'Costa Rica'),
('HR', 'Croatia'),
('CU', 'Cuba'),
('CY', 'Cyprus'),
('CZ', 'Czech Republic'),
('DK', 'Denmark'),
('DJ', 'Djibouti'),
('DM', 'Dominica'),
('DO', 'Dominican Republic'),
('EC', 'Ecuador'),
('EG', 'Egypt'),
('SV', 'El Salvador'),
('GQ', 'Equatorial Guinea'),
('EE', 'Estonia'),
('ET', 'Ethiopia'),
('FK', 'Falkland Islands (Malvinas)'),
('FO', 'Faroe Islands'),
('FJ', 'Fiji'),
('FI', 'Finland'),
('FR', 'France'),
('GA', 'Gabon'),
('GM', 'Gambia'),
('GE', 'Georgia'),
('DE', 'Germany'),
('GH', 'Ghana'),
('GI', 'Gibraltar'),
('GR', 'Greece'),
('GL', 'Greenland'),
('GD', 'Grenada'),
('GP', 'Guadeloupe'),
('GU', 'Guam'),
('GT', 'Guatemala'),
('GF', 'Guiana (French)'),
('GN', 'Guinea'),
('GW', 'Guinea Bissau'),
('GY', 'Guyana'),
('HT', 'Haiti'),
('HN', 'Honduras'),
('HK', 'Hong Kong'),
('HU', 'Hungary'),
('IS', 'Iceland'),
('IN', 'India'),
('ID', 'Indonesia'),
('IR', 'Iran'),
('IQ', 'Iraq'),
('IE', 'Ireland'),
('IL', 'Israel'),
('IT', 'Italy'),
('CI', 'Ivory Coast'),
('JM', 'Jamaica'),
('JP', 'Japan'),
('JT', 'Johnston Island'),
('JO', 'Jordan'),
('KZ', 'Kazakhstan'),
('KE', 'Kenya'),
('KI', 'Kiribati'),
('KP', 'Korea (North)'),
('KR', 'Korea (South)'),
('KW', 'Kuwait'),
('KG', 'Kyrgyzstan'),
('LA', 'Laos'),
('LV', 'Latvia'),
('LB', 'Lebanon'),
('LS', 'Lesotho'),
('LR', 'Liberia'),
('LY', 'Libya'),
('LI', 'Liechtenstein'),
('LT', 'Lithuania'),
('LU', 'Luxembourg'),
('MO', 'Macau'),
('MG', 'Madagascar'),
('MW', 'Malawi'),
('MY', 'Malaysia'),
('MV', 'Maldives'),
('ML', 'Mali'),
('MT', 'Malta'),
('MH', 'Marshall Islands'),
('MQ', 'Martinique'),
('MR', 'Mauritania'),
('MU', 'Mauritius'),
('MX', 'Mexico'),
('FM', 'Micronesia'),
('MI', 'Midway Islands'),
('MD', 'Moldavia'),
('MC', 'Monaco'),
('MN', 'Mongolia'),
('MS', 'Montserrat'),
('MA', 'Morocco'),
('MZ', 'Mozambique'),
('MM', 'Myanmar'),
('NA', 'Namibia'),
('NR', 'Nauru'),
('NP', 'Nepal'),
('NL', 'Netherlands'),
('AN', 'Netherlands Antilles'),
('NC', 'New Caledonia'),
('NZ', 'New Zealand'),
('NI', 'Nicaragua'),
('NE', 'Niger'),
('NG', 'Nigeria'),
('NU', 'Niue'),
('NF', 'Norfolk Island'),
('NO', 'Norway'),
('OM', 'Oman'),
('PC', 'Pacific Islands (US)'),
('PK', 'Pakistan'),
('PA', 'Panama'),
('PG', 'Papua New Guinea'),
('PY', 'Paraguay'),
('PE', 'Peru'),
('PH', 'Philippines'),
('PN', 'Pitcairn Islands'),
('PL', 'Poland'),
('PF', 'Polynesia (French)'),
('PT', 'Portugal'),
('PR', 'Puerto Rico'),
('QA', 'Qatar'),
('RE', 'Reunion'),
('RO', 'Romania'),
('RU', 'Russia'),
('RW', 'Rwanda'),
('EH', 'Sahara (Western)'),
('SH', 'Saint Helena'),
('KN', 'Saint Kitts and Nevis'),
('LC', 'Saint Lucia'),
('PM', 'Saint Pierre and Miquelon'),
('VC', 'Saint Vincent and Grenadines'),
('AS', 'Samoa (American)'),
('WS', 'Samoa (Western)'),
('SM', 'San Marino'),
('ST', 'Sao Tome and Principe'),
('SA', 'Saudi Arabia'),
('SN', 'Senegal'),
('SC', 'Seychelles'),
('SL', 'Sierra Leone'),
('SG', 'Singapore'),
('SK', 'Slovakia'),
('SI', 'Slovenia'),
('SB', 'Solomon Islands'),
('SO', 'Somalia'),
('ZA', 'South Africa'),
('ES', 'Spain'),
('LK', 'Sri Lanka'),
('SD', 'Sudan'),
('SR', 'Surinam'),
('SZ', 'Swaziland'),
('SE', 'Sweden'),
('CH', 'Switzerland'),
('SY', 'Syria'),
('TJ', 'Tadzhikistan'),
('TW', 'Taiwan'),
('TZ', 'Tanzania'),
('TH', 'Thailand'),
('TP', 'Timor (East)'),
('TG', 'Togo'),
('TK', 'Tokelau'),
('TO', 'Tonga'),
('TT', 'Trinidad and Tobago'),
('TN', 'Tunisia'),
('TR', 'Turkey'),
('TM', 'Turkmenistan'),
('TC', 'Turks and Caicos Islands'),
('TV', 'Tuvalu'),
('UG', 'Uganda'),
('UA', 'Ukraine'),
('AE', 'United Arab Emirates'),
('GB', 'United Kingdom'),
('US', 'United States'),
('UY', 'Uruguay'),
('UZ', 'Uzbekistan'),
('VU', 'Vanuatu'),
('VA', 'Vatican'),
('VE', 'Venezuela'),
('VN', 'Vietnam'),
('VG', 'Virgin Islands (British)'),
('VI', 'Virgin Islands (US)'),
('WK', 'Wake Island'),
('WF', 'Wallis and Futuna Islands'),
('YE', 'Yemen'),
('YU', 'Yugoslavia'),
('ZR', 'Zaire'),
('ZM', 'Zambia'),
('ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `yfa_focus_area`
--

DROP TABLE IF EXISTS `yfa_focus_area`;
CREATE TABLE IF NOT EXISTS `yfa_focus_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fa_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `yfa_focus_area`
--

INSERT INTO `yfa_focus_area` (`id`, `fa_name`) VALUES
(1, 'Health and Hygiene'),
(2, 'Education and Employment'),
(3, 'Environment, Water and Climate Change'),
(4, 'Citizenship and Governance'),
(5, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `yfa_forgot_password`
--

DROP TABLE IF EXISTS `yfa_forgot_password`;
CREATE TABLE IF NOT EXISTS `yfa_forgot_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `date_requested` varchar(50) NOT NULL,
  `used` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `yfa_organizations`
--

DROP TABLE IF EXISTS `yfa_organizations`;
CREATE TABLE IF NOT EXISTS `yfa_organizations` (
  `user_id` varchar(14) NOT NULL,
  `organization_name` varchar(400) NOT NULL,
  `organization_slug` varchar(255) NOT NULL,
  `organization_photo` varchar(800) NOT NULL,
  `organization_description` text NOT NULL,
  `year_established` mediumint(4) NOT NULL,
  `country` varchar(2) NOT NULL,
  `sector_id` tinyint(3) NOT NULL,
  `focus_area_id` tinyint(3) NOT NULL,
  `mission_vision` text NOT NULL,
  `more_information` text NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `position` varchar(150) NOT NULL,
  `phone_number` varchar(30) NOT NULL,
  `website` varchar(100) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `organization_name` (`organization_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `yfa_organizations`
--

INSERT INTO `yfa_organizations` (`user_id`, `organization_name`, `organization_slug`, `organization_photo`, `organization_description`, `year_established`, `country`, `sector_id`, `focus_area_id`, `mission_vision`, `more_information`, `contact_person`, `position`, `phone_number`, `website`) VALUES
('27580307152015', 'sample', 'sample', 'sample', 'sample', 2015, 'PH', 1, 1, 'sample', 'sample', 'sample', 'sample', '1234', 'sample.com'),
('48031307232015', 'ram orgs', 'ram-orgs', 'sdasdasdasd.jpg', 'Test Org', 2012, 'Ph', 2, 2, 'Test Org', 'Test Org', 'rassfv', 'Any', '+639228026684', 'www.test.com');

-- --------------------------------------------------------

--
-- Table structure for table `yfa_projects`
--

DROP TABLE IF EXISTS `yfa_projects`;
CREATE TABLE IF NOT EXISTS `yfa_projects` (
  `project_id` int(8) NOT NULL,
  `project_name` varchar(400) NOT NULL,
  `focus_area` varchar(250) NOT NULL,
  `country_id` mediumint(3) NOT NULL,
  `term_from` varchar(100) NOT NULL,
  `term_to` varchar(100) NOT NULL,
  `value_from` float NOT NULL,
  `value_to` float NOT NULL,
  `about` text NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `contact_email` varchar(150) NOT NULL,
  `position` varchar(150) NOT NULL,
  `phone_number` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `yfa_user_account_info`
--

DROP TABLE IF EXISTS `yfa_user_account_info`;
CREATE TABLE IF NOT EXISTS `yfa_user_account_info` (
  `user_id` varchar(14) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL,
  `confirmation_code` varchar(50) NOT NULL,
  `confirmation_status` varchar(5) NOT NULL,
  `date_confirmed` varchar(20) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `yfa_user_account_info`
--

INSERT INTO `yfa_user_account_info` (`user_id`, `username`, `password`, `email`, `confirmation_code`, `confirmation_status`, `date_confirmed`) VALUES
('27580307152015', 'sample@email.com', 'b28c4ae47e01047ed94b81dcae95d702', 'sample@email.com', 'fpkzb', '0', ''),
('48031307232015', 'ramilevangelista2009@gmail.com', '686f3e7c47d947d245723b3e483605c3', 'ramilevangelista2009@gmail.com', '7tvdm', '1', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
