<?php

if ($auth->haslogin()) {

    if (isset($_POST['submit'])) {
        extract($_POST);
        $err = 0;

        $saveSql = Database::execute("UPDATE ".DB_NAME.".".TABLE_PREFIX."organizations SET
            `organization_name` = '$organization_name',
            `organization_description` = '$organization_description',
            `year_established` = '$year_established',
            `country` = '$country',
            `sector` = '$sector',
            `focus_area` = '$focus_area',
            `mission_vision` = '$mission_vision',
            `more_information` = '$more_information',
            `contact_person` = '$contact_person',
            `position` = '$position',
            `phone_number` = '$phone_number',
            `website` = '$website'
            WHERE user_id = '".$_SESSION['P_Id']."'
        ");

        /* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
        if((!empty($_FILES["organization_photo"])) && ($_FILES['organization_photo']['error'] == 0)) {

            //Check if the file is JPEG image and it's size is less than 4mb

            $filename = basename($_FILES['organization_photo']['name']);
            $ext = substr($filename, strrpos($filename, '.') + 1);

            if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["organization_photo"]["size"] < 4194304)) {

            //Determine the path to which we want to save this file
              $newname = IMG_PATH.'organization/'.time().'-'.$filename;
              $filename = '/images/organization/'.time().'-'.$filename;
              //Check if the file with the same name is already exists on the server
              if (!file_exists($newname)) {
                //Attempt to move the uploaded file to it's new place
                if ((move_uploaded_file($_FILES['organization_photo']['tmp_name'],$newname))) {

                    $organization_photo = $filename;

                } else {
                   $msg .= "Error: A problem occurred during file upload!";
                   $err++;
                }
              } else {
                 $msg .= "Error: File ".$_FILES["organization_photo"]["name"]." already exists";
                 $err++;
              }
            } else {
             $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
             $err++;
            }

        } else {
            //$msg .= "Inage is required.<br />";
            //$err++;
        }


        $_SESSION['msg']['success'] = "Profile Updated";


        if($err>0){
            $_SESSION['msg']['error'] = "Error: ".$msg;
        } else {
            // save photo
            $saveSql = Database::execute("UPDATE ".DB_NAME.".".TABLE_PREFIX."organizations SET
                `organization_photo` = '$organization_photo'
                WHERE user_id = '".$_SESSION['P_Id']."'
            ");

        }

    }

    //get org
    $orgSql = "SELECT * FROM ".DB_NAME.".".TABLE_PREFIX."organizations WHERE user_id = '".$_SESSION['P_Id']."'";

    $getOrg = Database::getRow($orgSql);





} else {
    header("Location: ".HOST."register");
}
