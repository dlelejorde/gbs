<?php 
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php'); 
$conn->checklog();

include(ADMIN_TEMPLATE_PATH.'header.php'); 

$id = $_GET['id'];

$row_content = $conn->array_rs_single("select * from ".$maintable_prefix."_members WHERE member_id = '$id'");
?>
<div class="mainwrapper">
    
    <?php
        include(ADMIN_TEMPLATE_PATH.'mainHead.php');
        include(ADMIN_TEMPLATE_PATH.'navigation.php');
    ?>


    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="<?php echo SITE_ADMIN_DOMAIN; ?>dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Members</li>
        </ul>
        
        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <!--h5>Categories</h5-->
                <h1>Members</h1>
            </div>
        </div><!--pageheader-->
        
        <div class="maincontent">
            <div class="maincontentinner">
            <?php echo $_SESSION['message']; ?>
            <div class="widgetbox box-inverse">
                <h4 class="widgettitle"><?php echo $type; ?> Logo</h4>
                <div class="widgetcontent nopadding">
                	<form class="stdform stdform2" method="post" name="form1"  enctype="multipart/form-data">
		                <p>
		                    <label>Username</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['member_username'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Email</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['member_email'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Name</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['member_name'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Birthday</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['member_birthday'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Mobile</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['member_mobile'] ?>" /></span>
		                </p>
		                <p>
		                    <label>How long have you been surfing?</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['member_surfing_exp'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Where do you usually surf?</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['member_surfing_where'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Where is your home break?</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['member_surfing_home'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Facebook</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['member_fb'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Twitter</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['member_twitter'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Instagram</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['member_instagram'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Blog</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo $row_content['member_blog'] ?>" /></span>
		                </p>
		                <p>
		                    <label>Confirmed</label>
		                    <span class="field"><input type="text" class="input-xlarge" value="<?php echo ($row_content['member_confirmed']=='1') ? 'Yes' : 'No'; ?>" /></span>
		                </p>
	                </form>
                </div><!--widgetcontent-->
            </div><!--widget-->                
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->

</div><!--mainwrapper-->
<?php include(ADMIN_TEMPLATE_PATH.'footer.php'); ?>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>