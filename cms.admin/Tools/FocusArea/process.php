<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";

if($_POST['actions']=="Add"){
	extract($_POST);

	if (trim($fa_name)== ""){
		$msg .= "Name is required.<br />";
		$err++;
	}

	if ($err==0){

		//insert shipping table
		$data = array('fa_name'=>$fa_name);
		$conn->insert($data,$maintable_prefix.'_focus_area','');

		$_SESSION['message'] = '<h4 class="widgettitle title-success">New focus area added. '.$fa_name.'</h4>';
		header("Location: index.php?nav=focusarea");
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=focusarea");
	}

}

if($_POST['actions']=="Edit"){
	extract($_POST);


	if (trim($fa_name)== ""){
		$msg .= "Name is required.<br />";
		$err++;
	}


	if ($err==0){


		//update flashbox table
		$data = array('fa_name'=>$fa_name);
		$conn->update_tbl($data,$maintable_prefix.'_focus_area',"id = '".$id."'");


		$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$title.' Edited.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=focusarea&id=".$id);
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=focusarea&id=".$id);
	}
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){
		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_focus_area where id ='".$id."'");
		echo '1';
	} else {
		echo '0';
	}

}
