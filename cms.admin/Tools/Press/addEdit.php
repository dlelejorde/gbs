<?php 
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php'); 
$conn->checklog();
include(ADMIN_TEMPLATE_PATH.'header.php'); 

?>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/tinymce/jquery.tinymce.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/wysiwyg.js"></script>
<?php
$row_country = $conn->get_array_rs("select * from ".$maintable_prefix."_country");

$country = array();
if(isset($_GET['id'])){

    $id = $_GET['id'];

    if(trim($id)!=""){

        $row_content = $conn->array_rs_single("select * from ".$maintable_prefix."_press WHERE press_id = '$id' ");
        $row_country_coll = $conn->get_array_rs("select country_id from ".$maintable_prefix."_press_country WHERE press_id = '$id'");
        
        foreach ($row_country_coll as $key => $value) {
            foreach ($value as $val) {
                $country[] = $val;
            }
        }
        $type = "Edit";      
    } else {
        $type = "Add";
    }

} else {
     $type = "Add";
}


?>
<div class="mainwrapper">
    
    <?php
        include(ADMIN_TEMPLATE_PATH.'mainHead.php');
        include(ADMIN_TEMPLATE_PATH.'navigation.php');
    ?>


    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="<?php echo SITE_ADMIN_DOMAIN; ?>dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Promo</li>
            
            <li class="right">
                <a href="" data-toggle="dropdown" class="dropdown-toggle"><i class="icon-tint"></i> Color Skins</a>
                <ul class="dropdown-menu pull-right skin-color">
                    <li><a href="default">Default</a></li>
                    <li><a href="navyblue">Navy Blue</a></li>
                    <li><a href="palegreen">Pale Green</a></li>
                    <li><a href="red">Red</a></li>
                    <li><a href="green">Green</a></li>
                    <li><a href="brown">Brown</a></li>
                </ul>
            </li>
        </ul>
        
        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <!--h5>Categories</h5-->
                <h1>Press[<?php echo $type; ?>]</h1>
            </div>
        </div><!--pageheader-->
        
        <div class="maincontent">
            <div class="maincontentinner">
            <?php echo $_SESSION['message']; ?>
            <div class="widgetbox box-inverse">
                <h4 class="widgettitle"><?php echo $type; ?> Press</h4>
                <div class="widgetcontent nopadding">
                    <form class="stdform stdform2" method="post" name="form1" action="<?php echo SITE_ADMIN_DOMAIN; ?>Tools/Press/process.php" enctype="multipart/form-data">
                        <input type="hidden" name="press_id" id="press_id" value="<?php echo $row_content['press_id'] ?>" />
                        <input type="hidden" name="actions" id="actions" value="<?php echo $type; ?>" />
                            <p>
                                <label>Country*</label>
                                <span class="field">
                                    <?php foreach ($row_country as $key => $value) { ?>
                                        <span class=""><input type="checkbox" name="press_country[]" id="press_country" style="opacity: 0;" value="<?php echo $value['country_id'] ?>" <?php if(in_array($value['country_id'], $country)){ echo 'checked="checked"'; } ?>></span><?php echo $value['country_name'] ?><br>
                                    <?php } ?>
                                </span>
                            </p>
                            <p>
                                <label>Title*</label>
                                <span class="field"><input type="text" name="press_title" id="press_title" class="input-xxlarge" value="<?php echo $row_content['press_title'] ?>" /></span>
                            </p>
                            
                            <p>
                                <label>Meta Description <small>For SEO purposes.</small></label>
                                <span class="field"><input type="text" name="press_meta_description" id="content_meta_description" class="input-xxlarge" value="<?php echo $row_content['press_meta_description'] ?>" /></span>
                            </p>

                            <p>
                                <label>Meta Keywords <small>For SEO purposes.</small></label>
                                <span class="field"><input type="text" name="press_meta_keywords" id="press_meta_keywords" class="input-xxlarge" value="<?php echo $row_content['press_meta_keywords'] ?>" /></span>
                            </p>

                            <p>
                                <label>Blurb</label>
                                <span class="field"><input type="text" name="press_blurb" id="press_blurb" class="input-xxlarge" value="<?php echo $row_content['press_blurb'] ?>" /></span>
                            </p>
                            
                            <p>
                                <label>Text* <small>Promo main content.</small></label>
                                <span class="field">
                                    <textarea id="elm1" name="elm1" rows="15" cols="80" style="width: 80%" class="tinymce">
                                        <?php echo html_entity_decode($row_content['press_content']) ?>
                                    </textarea>
                                </span>
                            </p>

                            <p>
                                <label>Thumbs:</label>
                                <span class="field">
                                <input type="file" name="press_thumbs" class="input-xlarge" value="" /><br />
                                <?php 
                                    if($row_content['press_thumbs']!=""){
                                        echo '<img src="'.SITE_DOMAIN.$row_content['press_thumbs'].'" />';
                                    }
                                ?>
                                </span>
                            </p>


                            <p>
                                <label>Image:</label>
                                <span class="field">
                                <input type="file" name="press_image" class="input-xlarge" value="" />
                                <?php 
                                    if($row_content['press_image']!=""){
                                        echo '<img src="'.SITE_DOMAIN.$row_content['press_image'].'" />';
                                    }
                                ?>
                                </span>
                            </p>
                            
                            <p>
                                <label>Status</label>
                                <span class="field"><select name="press_status" id="selection2" class="uniformselect">
                                    <option value="0">Choose One</option>
                                    <option value="0" <?php if($row_content['press_status']=='0'){ echo 'selected="selected"'; } ?>>Draft</option>
                                    <option value="1" <?php if($row_content['press_status']=='1'){ echo 'selected="selected"'; } ?>>Publish</option>
                                </select></span>
                            </p>
                                                    
                            <p class="stdformbutton">
                                <input type="submit" class="btn btn-primary" value="Submit Button">
                                <!-- <button class="btn btn-primary">Submit Button</button> -->
                                <button type="reset" class="btn">Reset Button</button>
                            </p>
                    </form>
                </div><!--widgetcontent-->
            </div><!--widget-->                
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->

</div><!--mainwrapper-->
<?php include(ADMIN_TEMPLATE_PATH.'footer.php'); ?>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>