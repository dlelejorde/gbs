<?php 
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php'); 
$conn->checklog();
include(ADMIN_TEMPLATE_PATH.'header.php'); 

?>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/tinymce/jquery.tinymce.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/wysiwyg.js"></script>
<?php

$row_cat = $conn->get_array_rs("select * from ".$maintable_prefix."_category WHERE cat_parent = 0");
$row_country = $conn->get_array_rs("select * from ".$maintable_prefix."_country");

$country = array();
$cat = array();

if(isset($_GET['id'])){

    $id = $_GET['id'];

    if(trim($id)!=""){

        $row_content = $conn->array_rs_single("select * from ".$maintable_prefix."_brand WHERE brand_id = '$id' ");

        $row_cat_data = $conn->get_array_rs("select cat_id from ".$maintable_prefix."_brand_cat WHERE brand_id = '$id'");
        
        foreach ($row_cat_data as $key => $value) {
            foreach ($value as $val) {
                $cat[] = $val;
            }
        }

        $type = "Edit";      
    } else {
        $type = "Add";
    }

} else {
     $type = "Add";
}


?>
<div class="mainwrapper">
    
    <?php
        include(ADMIN_TEMPLATE_PATH.'mainHead.php');
        include(ADMIN_TEMPLATE_PATH.'navigation.php');
    ?>


    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="<?php echo SITE_ADMIN_DOMAIN; ?>dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Brands</li>
        </ul>
        
        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <!--h5>Categories</h5-->
                <h1>Brand[<?php echo $type; ?>]</h1>
            </div>
        </div><!--pageheader-->
        
        <div class="maincontent">
            <div class="maincontentinner">
            <?php echo $_SESSION['message']; ?>
            <div class="widgetbox box-inverse">
                <h4 class="widgettitle"><?php echo $type; ?> Brand</h4>
                <div class="widgetcontent nopadding">
                    <form class="stdform stdform2" method="post" name="form1" id="form1" action="<?php echo SITE_ADMIN_DOMAIN; ?>Tools/Brands/process.php" enctype="multipart/form-data">
                        <input type="hidden" name="brand_id" id="brand_id" value="<?php echo $row_content['brand_id'] ?>" />
                        <input type="hidden" name="actions" id="actions" value="<?php echo $type; ?>" />
                            <p>
                                <label>Category*</label>
                                <span class="field">
                                    <?php foreach ($row_cat as $key => $value) { ?>
                                        <span class=""><input type="checkbox" name="brand_cat[]" id="brand_cat" style="opacity: 0;" value="<?php echo $value['cat_id'] ?>" <?php if(in_array($value['cat_id'], $cat)){ echo 'checked="checked"'; } ?> <?php if($value['cat_id']==3) { echo 'disabled'; } ?>></span><?php echo $value['cat_name'] ?><br>
                                    <?php 
                                        $row_catSub = $conn->get_array_rs("select * from ".$maintable_prefix."_category WHERE cat_parent = '".$value['cat_id']."'"); 
                                        if(count($row_catSub)>0){
                                            foreach ($row_catSub as $valve) {
                                            
                                    ?>
                                        <span style="padding-left: 20px;"><input type="checkbox" name="brand_cat[]" id="brand_cat" style="opacity: 0;" value="<?php echo $valve['cat_id'] ?>" <?php if(in_array($valve['cat_id'], $cat)){ echo 'checked="checked"'; } ?>></span><?php echo $valve['cat_name'] ?><br>
                                    <?php
                                            }
                                        }
                                    } ?>
                                </span>
                            </p>
                            <p>
                                <label>Name*</label>
                                <span class="field"><input type="text" name="brand_name" id="brand_name" class="input-xxlarge" value="<?php echo $row_content['brand_name'] ?>" /></span>
                            </p>
                            
                            <p>
                                <label>Text* <small>Brand writeup.</small></label>
                                <span class="field">
                                    <textarea id="elm1" name="elm1" rows="15" cols="80" style="width: 80%" class="tinymce">
                                        <?php echo html_entity_decode($row_content['brand_writeup']) ?>
                                    </textarea>
                                </span>
                            </p>

                            <p>
                                <label>Image:</label>
                                <span class="field">
                                <input type="file" name="brand_logo" class="input-xlarge" value="" /><br />
                                <?php 
                                    if($row_content['brand_logo']!=""){
                                        echo '<img src="'.SITE_DOMAIN.$row_content['brand_logo'].'" />';
                                    }
                                ?>
                                </span>
                            </p>

                            <p>
                                <label>&nbsp;</label>
                                <span class="field">
                                    <input type="button" class="btn" id="addFB" value="Add Flashbox">
                                </span>
                            </p>

                            <div id="pD">
                                <?php 
                                    $row_prod = $conn->get_array_rs("select * from ".$maintable_prefix."_flashbox WHERE flashbox_cat = '".$_GET['id']."' ");
                                    $ctr = 1;
                                    if(isset($_GET['id']) && count($row_prod)>0){

                                        foreach ($row_prod as $key => $valP) {
                                ?>
                                    <div id="productBox" style="border-top: 1px solid #ddd;">
                                        <p>
                                            <label>Flashbox <span class="titleCount">1</span></label>
                                            <span class="field">
                                                <input type="hidden" name="prod_name[]" id="prod_name" value="" />
                                                <input type="file" name="flashbox_image[]" class="input-xlarge" value="" />
                                                <br />
                                                <?php 
                                                    if($valP['flashbox_image']!=""){
                                                        echo '<img src="'.SITE_DOMAIN.$valP['flashbox_image'].'" class="imgFB" />';
                                                    }
                                                ?>
                                                <input type="hidden" name="hflashbox_image[]" value="<?php echo $valP['flashbox_image'] ?>" />
                                            </span>
                                        </p>
                                        <p>
                                            <label>&nbsp;</label>
                                            <span class="field"><input type="button" class="btn deleteProd" value="Delete Flashbox"></span>
                                        </p>
                                    </div>
                                <?php
                                        $ctr++;
                                        }

                                ?>
                                <?php } else { ?>

                                    <div id="productBox" style="border-top: 1px solid #ddd;">
                                        <p>
                                            <label>Flashbox <span class="titleCount">1</span></label>
                                            <span class="field">
                                                <input type="hidden" name="prod_name[]" id="prod_name" value="" />
                                                <input type="file" name="flashbox_image[]" class="input-xlarge" value="" />
                                            </span>
                                        </p>
                                        <p>
                                            <label>&nbsp;</label>
                                            <span class="field"><input type="button" class="btn deleteProd" value="Delete Flashbox"></span>
                                        </p>
                                    </div>
                                <?php } ?>                        
                            </div>
                            

                                                    
                            <p class="stdformbutton">
                                <input type="submit" class="btn btn-primary" value="Submit Button">
                                <!-- <button class="btn btn-primary">Submit Button</button> -->
                                <button type="reset" class="btn">Reset Button</button>
                            </p>
                    </form>
                </div><!--widgetcontent-->
            </div><!--widget-->                
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->

</div><!--mainwrapper-->

<script type="text/javascript">
    var m = 1;
    jQuery("#addFB").click(function() {
        m++;
        jQuery("#productBox")
            .last()
            .clone()
            .appendTo(jQuery("#pD"))
            .find(".titleCount").html(m);
        return false;
        
    });

    jQuery(".deleteProd").click(function(){
        jQuery(this).parent().parent().parent().remove();
    });
</script>
<?php include(ADMIN_TEMPLATE_PATH.'footer.php'); ?>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>