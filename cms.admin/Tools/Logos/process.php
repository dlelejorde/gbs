<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";
$strtime = time();


if($_POST['actions']=="Add"){
	extract($_POST);

	if (trim($logo_name)== ""){
		$msg .= "name is required.<br />";
		$err++;
	}


	/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
	if((!empty($_FILES["logo_image"])) && ($_FILES['logo_image']['error'] == 0)) {
		
		//Check if the file is JPEG image and it's size is less than 4mb

		$filename = basename($_FILES['logo_image']['name']);
		$ext = substr($filename, strrpos($filename, '.') + 1);

		if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["logo_image"]["size"] < 4194304)) {

		//Determine the path to which we want to save this file
		  $newname = SITE_PATH.'images/logos/'.$strtime.'-'.$filename;
		  $filename = '/images/logos/'.$strtime.'-'.$filename;
		  //Check if the file with the same name is already exists on the server
		  if (!file_exists($newname)) {
			//Attempt to move the uploaded file to it's new place
			if ((move_uploaded_file($_FILES['logo_image']['tmp_name'],$newname))) {

				$logo_image = $filename;

			} else {
			   $msg .= "Error: A problem occurred during file upload!";
			   $err++;
			}
		  } else {
			 $msg .= "Error: File ".$_FILES["logo_image"]["name"]." already exists";
			 $err++;
		  }
		} else {
		 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
		 $err++;
		}
		
	} else {
		$msg .= "Inage is required.<br />";
		$err++;
	}							


	if ($err==0){

		//insert flashbox table
		$data = array('logo_name'=>htmlspecialchars($logo_name,ENT_QUOTES),
			'logo_link'=>$logo_link,
			'logo_image'=>$logo_image,
			'logo_added_by'=>$admin,
			'logo_date_added'=>strtotime("now"));
		$conn->insert($data,$maintable_prefix.'_logos','');		

		$_SESSION['message'] = '<h4 class="widgettitle title-success">New logo added. '.$logo_name.'</h4>';
		header("Location: index.php?nav=logo");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=logo");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);


	if (trim($logo_name)== ""){
		$msg .= "Name is required.<br />";
		$err++;
	}


	if ($err==0){

		//update flashbox table
		$data = array('logo_name'=>htmlspecialchars($logo_name,ENT_QUOTES),
			'logo_link'=>$logo_link,
			'logo_edited_by'=>$admin,
			'logo_date_edited'=>strtotime("now"));
		$conn->update_tbl($data,$maintable_prefix.'_logos',"logo_id = '".$logo_id."'");		

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["logo_image"])) && ($_FILES['logo_image']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['logo_image']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["logo_image"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/logos/'.$strtime.'-'.$filename;
			  $filename = '/images/logos/'.$strtime.'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['logo_image']['tmp_name'],$newname))) {

					$logo_image = $filename;
					$data = array('logo_image'=>$logo_image);
					$conn->update_tbl($data,$maintable_prefix.'_logos',"logo_id = '".$logo_id."'");

				} else {
				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["logo_image"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
		} else {

		}				


		$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$logo_name.' Edited.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=logo&id=".$logo_id);	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=logo&id=".$logo_id);	
	}
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){

		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_logos where logo_id ='".$id."'");
		echo '1';
	} else {
		echo '0';
	}

}