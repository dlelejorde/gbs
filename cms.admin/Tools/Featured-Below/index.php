<?php 
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php'); 
$conn->checklog();
include(ADMIN_TEMPLATE_PATH.'header.php'); 

$row_content = $conn->get_array_rs("select * from ".$maintable_prefix."_featured_home_below order by id desc");

?>
    <script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });
            
            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
            
        });
    </script>
<div class="mainwrapper">
    
    <?php
        include(ADMIN_TEMPLATE_PATH.'mainHead.php');
        include(ADMIN_TEMPLATE_PATH.'navigation.php');
    ?>

    
    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="<?php echo SITE_ADMIN_DOMAIN; ?>dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Featured on Homepage(Below)</li>
            
            <li class="right">
                <a href="" data-toggle="dropdown" class="dropdown-toggle"><i class="icon-tint"></i> Color Skins</a>
                <ul class="dropdown-menu pull-right skin-color">
                    <li><a href="default">Default</a></li>
                    <li><a href="navyblue">Navy Blue</a></li>
                    <li><a href="palegreen">Pale Green</a></li>
                    <li><a href="red">Red</a></li>
                    <li><a href="green">Green</a></li>
                    <li><a href="brown">Brown</a></li>
                </ul>
            </li>
        </ul>
        
        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-book"></span></div>
            <div class="pagetitle">
                <h1>Featured on Homepage(Below)</h1>
            </div>
        </div><!--pageheader-->
        
        <div class="maincontent">
            <div class="maincontentinner">
                <?php echo $_SESSION['message']; ?>

                
                <div class="headtitle" style="margin-bottom: 0px;">
                    <div class="btn-group">
                        <button data-toggle="dropdown" class="btn dropdown-toggle">ADD <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                          <li><a href="addEdit.php?nav=options">Add Image</a></li>
                        </ul>
                    </div>
                    <h4 class="widgettitle">Records</h4>
                </div>
                <table id="dyntable" class="table table-bordered responsive">
                    <colgroup>
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                    </colgroup>
                    <thead>
                        <tr>
                            <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                            <th class="head1">Title</th>
                            <th class="head1">Image</th>
                            <th class="head1">Product</th>
                            <th class="head0 centeralign" >Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($row_content as $value) { ?>
                        <tr class="gradeX"  data="<?php echo $value['id']; ?>">
                          <td class="aligncenter"><span class="center"><input type="checkbox" /></span></td>
                            <td><?php echo $value['title']; ?></td>
                            <td><img src="<?php echo SITE_DOMAIN.$value['image']; ?>" width="100" /></td>
                            <td><?php echo $value['product_id']; ?></td>
                            <td class="centeralign">
                                <a href="addEdit.php?nav=options&id=<?php echo $value['id']; ?>" class="editrow"><span class="icon-edit"></span></a>
                                <a href="" class="deleterow" data="<?php echo $value['id']; ?>"><span class="icon-trash"></span></a>
                            </td>
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                </table>
                
                <br /><br />
                <br /><br />
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->
    
</div><!--mainwrapper-->
<script type="text/javascript">
    jQuery(document).ready(function(){
        // delete row in a table
        if(jQuery('.deleterow').length > 0) {
            jQuery('.deleterow').click(function(){
                var conf = confirm('Continue delete?');

            if(conf)
                    jQuery(this).parents('tr').fadeOut(function(){

                var id = jQuery(this).attr('data');
                var parameter = "id="+ id +"&action=delete";
                var url = "process.php";
                
                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: parameter,
                    success: function(originalRequest){
                        if(originalRequest=='1'){
                            jQuery(this).remove();
                        } else {
                            alert('Error: Record cannot be deleted!');
                        }
                    }
                });
                
            });
            return false;
        }); 
        }  
    });
</script>
<?php include(ADMIN_TEMPLATE_PATH.'footer.php'); ?>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>