<?php
class Brands{

	/*
	function name: getBrandsByCat
	Params
		$catID: category_id
	Return
		Brand array
	*/
	public function getBrandsByCat($catID){
		$strBrand = "SELECT
			b.brand_id, 
			b.brand_name,
			b.brand_universal,
			b.brand_slug
			FROM ".DB_NAME.".".TABLE_PREFIX."_brand b, 
			".DB_NAME.".".TABLE_PREFIX."_brand_cat bc,
			".DB_NAME.".".TABLE_PREFIX."_brand_country bCoun
			WHERE b.brand_id = bc.brand_id 
			AND b.brand_id = bCoun.brand_id
			AND bc.cat_id = ".$catID."
			AND bCoun.country_id = ".$_SESSION['Country']."
			".$paramstr."
			ORDER BY b.brand_id ASC";
		
		$resultBrand = Database::getResult($strBrand);

		return $resultBrand;
	}

	/*
	function name: getBrandsByCat
	Params
		$catID: parent_id
	Return
		Brand array
	*/
	public function getSubNav($catID){
		$strBrand = "SELECT *
			FROM ".DB_NAME.".".TABLE_PREFIX."_category 
			WHERE 
			cat_parent = ".$catID."
			ORDER BY cat_id ASC";
		
		$resultBrand = Database::getResult($strBrand);

		return $resultBrand;
	}
	
	/*
	function name: getBrandsLogo
	Params

	Return
		Brand array
	*/
	public function getBrandsLogo(){
		$strBrand = "SELECT
			b.brand_logo, 
			b.brand_link,
			b.brand_name
			FROM ".DB_NAME.".".TABLE_PREFIX."_brand b, 
			".DB_NAME.".".TABLE_PREFIX."_brand_country bCoun
			WHERE b.brand_id = bCoun.brand_id
			AND bCoun.country_id = ".$_SESSION['Country']."
			ORDER BY b.brand_id ASC";
		
		$resultBrand = Database::getResult($strBrand);

		return $resultBrand;
	}

	/*
	function name: chunkBrandsSub
	Params
		$arr: Brand array
	Return
		Brand array(chunked, for menu)
	*/
	public function chunkBrandsSub($arr){
		
		$arrBrand = array();
		$counter = count($arr);
        if($counter<=10){
            $arrBrand[] = $arr;
        } else if($counter>10 && $counter<=20){
            $arrBrand = array_chunk($arr, 2, true);
        } else if($counter>20 && $counter<=30){
            $arrBrand = array_chunk($arr, 3, true);
        } 

        return $arrBrand;
	}

	/*
	function name: chunkBrands
	Params
		$arr: Brand array
	Return
		Brand array(chunked, for menu)
	*/
	public function chunkBrands($arr){
		
		$arrBrand = array();
		$counter = count($arr);
        if($counter<=4){
            $arrBrand[] = $arr;
        } else if($counter>4 && $counter<=8){
            $arrBrand = array_chunk($arr, 4, true);
        } else if($counter>8 && $counter<=12){
            $arrBrand = array_chunk($arr, 4, true);
        } else if ($counter>12){
            $arrBrand = array_chunk($arr, 6, true);
        }

        return $arrBrand;
	}



	/*
	function name: getBrandsByCat
	Params
		$catID: category_id
	Return
		Brand array
	*/
	public function getAllBrands($catID){
		$strBrand = "SELECT
			b.brand_id, 
			b.brand_name,
			b.brand_universal,
			b.brand_slug
			FROM ".DB_NAME.".".TABLE_PREFIX."_brand b, 
			".DB_NAME.".".TABLE_PREFIX."_brand_country bCoun
			WHERE b.brand_id = bc.brand_id 
			AND b.brand_id = bCoun.brand_id
			AND bCoun.country_id = ".$_SESSION['Country']."
			".$paramstr."
			ORDER BY b.brand_id ASC";
		
		$resultBrand = Database::getResult($strBrand);

		return $resultBrand;
	}
}


?>