<footer class="footer">
    <div class="default-width">
        <div class="copyright">
            Copyright © Golden Business Solutions 2015.
        </div>
        <div class="menu">
            <ul class="inline-container aleft">
                <li class="item"><a href="#">HOME</a></li>
                <li class="item"><a href="#">PROFILE</a></li>
                <li class="item"><a href="#">PRODUCTS</a></li>
                <li class="item"><a href="#">OPPORTUNITY</a></li>
                <li class="item"><a href="#">CONTACT US</a></li>
            </ul>
        </div>
    </div>
</footer>