<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");

if($_POST['actions']=="Add"){
	extract($_POST);
	if (trim($gallery_name)!= ""){
		//update gallery table
		$data = array('gallery_name'=>$gallery_name,
			'gallery_slug'=>makeUrlFriendly($gallery_name));
		$conn->insert($data,$maintable_prefix.'_gallery','');			
		$_SESSION['message'] = '<h4 class="widgettitle title-success">New category added. '.$gallery_name.'</h4>';
		header("Location: index.php?nav=gallery");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.</h4>';
		header("Location: addEdit.php?nav=gallery");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);
	//update gallery table
	$data = array('gallery_name'=>$gallery_name,
			'gallery_slug'=>makeUrlFriendly($gallery_name));
	$conn->update_tbl($data,$maintable_prefix.'_gallery',"gallery_id = '".$gallery_id."'");			
	$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$gallery_name.' Edited.</h4>';
	header("Location: index.php?nav=gallery");		
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){
		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_gallery where gallery_id ='".$id."'");
		echo '1';
	} else {
		echo '0';
	}

}