            <section class="padHead ">


                <div class="inner registration">
                    <div class="over pad">
                        <div class="result-title">Edit Profile</div>
                        <div class="wiggle-black"></div>

                        <!--div class="tabs">
                            <a href="javascript:void(0)" class="sel" data-target=".organztn">ORGANIZATION</a>
                        </div-->
                        <?php if(isset($_SESSION['msg']['success'])){
                            echo '<div class="msg success">'.$_SESSION['msg']['success'].'</div>';
                        } ?>
                        <?php if(isset($_SESSION['msg']['error'])){
                            echo '<div class="msg error">'.$_SESSION['msg']['error'].'</div>';
                        } ?>

                        <form id="registerFrm" class="form" enctype="multipart/form-data" method="post" action="">
                            <input type="hidden" name="type" value="organization">
                            <div class="regs organztn tab-content tab-content-active">
                                <div class="rcol-half">
                                    <label>Name of Organization</label>
                                    <input type="text" class="txt-half" name="organization_name" value="<?php echo $getOrg['organization_name'] ?>"/>
                                </div>
                                <div class="rcol-half">
                                    <label>Year of establishment</label>
                                    <input type="text" class="txt-half" name="year_established" value="<?php echo $getOrg['year_established'] ?>"/>
                                </div>

                                <div class="rcol-thrd">
                                    <label class="first">Country</label>
                                    <?php
                                        $data = array(
                                            'table' => TABLE_PREFIX."countries",
                                            'value' => 'code',
                                            'label' => 'name',
                                            'default' => $getOrg['country'],
                                            'name' => 'country',
                                            'id' => 'country',
                                            'class' => 'first',
                                            );
                                        echo createDropdown($data);
                                    ?>
                                    <!--input type="text" class="txt-thrd first" name="country" /-->
                                </div>

                                <div class="rcol-thrd">
                                    <label class="second">Sector</label>
                                    <?php
                                        $data = array(
                                            'table' => TABLE_PREFIX."sector",
                                            'value' => 'id',
                                            'label' => 'sector_name',
                                            'default' => $getOrg['sector'],
                                            'name' => 'sector',
                                            'id' => 'sector',
                                            'class' => 'txt-thrd txt-medium',
                                            );
                                        echo createDropdown($data);
                                    ?>
                                    <!--input type="text" class="txt-thrd" name="sector" /-->
                                </div>

                                <div class="rcol-thrd">
                                    <label>Focus Area</label>
                                    <?php
                                        $data = array(
                                            'table' => TABLE_PREFIX."focus_area",
                                            'value' => 'id',
                                            'label' => 'fa_name',
                                            'default' => $getOrg['focus_area'],
                                            'name' => 'focus_area',
                                            'id' => 'focus_area',
                                            'class' => 'txt-thrd txt-medium',
                                            );
                                        echo createDropdown($data);
                                    ?>
                                    <!--input type="text" class="txt-thrd txt-large" name="focus_area" /-->
                                </div>

                                <div class="rcol-wide">
                                    <label>About the organization</label>
                                    <textarea class="txt-area-full" name="organization_description"><?php echo $getOrg['organization_description'] ?></textarea>
                                </div>

                                <div class="rcol-wide">
                                    <label>Mission and Vision</label>
                                    <textarea class="txt-area-full" name="mission_vision"><?php echo $getOrg['mission_vision'] ?></textarea>
                                </div>

                                <div class="rcol-wide">
                                    <label>More information</label>
                                    <textarea class="txt-area-full" name="more_information"><?php echo $getOrg['more_information'] ?></textarea>
                                </div>
                                <div class="contact-section">
                                    <div class="rcol-half">
                                        <label>Contact Person</label>
                                        <input type="text" class="txt-half" name="contact_person" value="<?php echo $getOrg['contact_person'] ?>" />
                                    </div>

                                    <div class="rcol-thrd">
                                        <label class="first">Position</label>
                                        <input type="text" class="txt-thrd txt-medium" name="position" value="<?php echo $getOrg['position'] ?>" />
                                    </div>

                                    <div class="rcol-half">
                                        <label>Phone Number</label>
                                        <input type="text" class="txt-thrd txt-medium" name="phone_number" value="<?php echo $getOrg['phone_number'] ?>" />
                                    </div>

                                    <div class="rcol-half">
                                        <label>Website</label>
                                        <input type="text" class="txt-thrd txt-medium" name="website" value="<?php echo $getOrg['website'] ?>" />
                                    </div>

                                    <div class="rcol-wide">
                                        <label>Upload Photo</label>
                                        <input type="file" class="txt-large" name="organization_photo" /><br />

                                    </div>

                                    <div class="rcol-wide">
                                        <label>Photo</label>
                                        <img src="<?php echo HOST.$getOrg['organization_photo'] ?>" width="150px" />
                                    </div>
                                </div>

                                <input class="btn-submit" name="submit" type="submit" value="SUBMIT">
                            </div>
                        </form>
                        <!--iframe id="results" name="results" src="<?php echo HOST; ?>api/register.php" width="0" height="0" style="display: none;"></iframe-->
                    </div>


                </div>
            </section>
<?php
unset($_SESSION['msg']['success']);
unset($_SESSION['msg']['error']);
?>
