<?php 
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php'); 
$conn->checklog();
include(ADMIN_TEMPLATE_PATH.'header.php'); 
?>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/tinymce/jquery.tinymce.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/wysiwyg.js"></script>
<?php
$country = $_GET['ctry'];

$row_tab1 = $conn->array_rs_single("select * from ".$maintable_prefix."_options WHERE option_type = 'collage-1' AND option_country = '".$country."' ");
$row_tab2 = $conn->array_rs_single("select * from ".$maintable_prefix."_options WHERE option_type = 'collage-2' AND option_country = '".$country."' ");
$row_tab3 = $conn->array_rs_single("select * from ".$maintable_prefix."_options WHERE option_type = 'collage-3' AND option_country = '".$country."' ");
$row_tab4 = $conn->array_rs_single("select * from ".$maintable_prefix."_options WHERE option_type = 'collage-4' AND option_country = '".$country."' ");
$row_tab5 = $conn->array_rs_single("select * from ".$maintable_prefix."_options WHERE option_type = 'collage-5' AND option_country = '".$country."' ");

?>
<div class="mainwrapper">
    
    <?php
        include(ADMIN_TEMPLATE_PATH.'mainHead.php');
        include(ADMIN_TEMPLATE_PATH.'navigation.php');
    ?>


    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="<?php echo SITE_ADMIN_DOMAIN; ?>dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Homepage Collage[<?php echo getCountryName($country); ?>]</li>
            
            <li class="right">

            </li>
        </ul>
        
        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <!--h5>Categories</h5-->
                <h1>Homepage Collage[<?php echo getCountryName($country); ?>]</h1>
            </div>
        </div><!--pageheader-->
        
        <div class="maincontent">
            <div class="maincontentinner">
                <?php echo $_SESSION['message']; ?>

                <form class="stdform stdform2" method="post" name="form1" action="<?php echo SITE_ADMIN_DOMAIN; ?>Tools/Options/process.php" enctype="multipart/form-data">
                    <input type="hidden" name="actions" id="actions" value="tabs" />
                    <input type="hidden" name="ctry" id="ctry" value="<?php echo $country; ?>" />

                    <!-- tab 1 -->
                    <div class="widgetbox login-information">
                        <h4 class="widgettitle">Collage 1[portrait]</h4>
                        <div class="widgetcontent">
                            <p>
                                <label>Name*</label>
                                <span class="field"><input type="text" name="option_name1" id="option_name1" class="input-xxlarge" value="<?php echo $row_tab1['option_name'] ?>" /></span>
                            </p>
                            <p>
                                <label>Image *<small>size (332px x 421px)</small></label>
                                <span class="field">
                                <input type="file" name="option_image1" class="input-xlarge" value="" /><br />
                                <?php 
                                    if($row_tab1['option_image']!=""){
                                        echo '<img src="'.SITE_DOMAIN.$row_tab1['option_image'].'" />';
                                    }
                                ?>
                                </span>
                            </p>
                            <p>
                                <label>Link*</label>
                                <span class="field"><input type="text" name="option_value1" id="option_value1" class="input-xxlarge" value="<?php echo $row_tab1['option_value'] ?>" /></span>
                            </p>
                        </div>
                    </div>  

                    <!-- tab 2 -->
                    <div class="widgetbox login-information">
                        <h4 class="widgettitle">Collage 2[small]</h4>
                        <div class="widgetcontent">
                            <p>
                                <label>Name*</label>
                                <span class="field"><input type="text" name="option_name2" id="option_name2" class="input-xxlarge" value="<?php echo $row_tab2['option_name'] ?>" /></span>
                            </p>
                            <p>
                                <label>Image * <small>size (231px x 204px)</small></label>
                                <span class="field">
                                <input type="file" name="option_image2" class="input-xlarge" value="" /><br />
                                <?php 
                                    if($row_tab2['option_image']!=""){
                                        echo '<img src="'.SITE_DOMAIN.$row_tab2['option_image'].'" />';
                                    }
                                ?>
                                </span>
                            </p>
                            <p>
                                <label>Caption*</label>
                                <span class="field"><input type="text" name="option_value2" id="option_value2" class="input-xxlarge" value="<?php echo $row_tab2['option_value'] ?>" /></span>
                            </p>
                        </div>
                    </div>  

                    <!-- tab 3 -->
                    <div class="widgetbox login-information">
                        <h4 class="widgettitle">Collage 3[small]</h4>
                        <div class="widgetcontent">
                            <p>
                                <label>Name*</label>
                                <span class="field"><input type="text" name="option_name3" id="option_name3" class="input-xxlarge" value="<?php echo $row_tab3['option_name'] ?>" /></span>
                            </p>
                            <p>
                                <label>Image *<small>size (231px x 204px)</small></label>
                                <span class="field">
                                <input type="file" name="option_image3" class="input-xlarge" value="" /><br />
                                <?php 
                                    if($row_tab3['option_image']!=""){
                                        echo '<img src="'.SITE_DOMAIN.$row_tab3['option_image'].'" />';
                                    }
                                ?>
                                </span>
                            </p>
                            <p>
                                <label>Caption*</label>
                                <span class="field"><input type="text" name="option_value3" id="option_value3" class="input-xxlarge" value="<?php echo $row_tab3['option_value'] ?>" /></span>
                            </p>
                        </div>
                    </div>  

                    <!-- tab 4 -->
                    <div class="widgetbox login-information">
                        <h4 class="widgettitle">Collage 4[small]</h4>
                        <div class="widgetcontent">
                            <p>
                                <label>Name*</label>
                                <span class="field"><input type="text" name="option_name4" id="option_name4" class="input-xxlarge" value="<?php echo $row_tab4['option_name'] ?>" /></span>
                            </p>
                            <p>
                                <label>Image *<small>size (231px x 204px)</small></label>
                                <span class="field">
                                <input type="file" name="option_image4" class="input-xlarge" value="" /><br />
                                <?php 
                                    if($row_tab4['option_image']!=""){
                                        echo '<img src="'.SITE_DOMAIN.$row_tab4['option_image'].'" />';
                                    }
                                ?>
                                </span>
                            </p>
                            <p>
                                <label>Caption*</label>
                                <span class="field"><input type="text" name="option_value4" id="option_value4" class="input-xxlarge" value="<?php echo $row_tab4['option_value'] ?>" /></span>
                            </p>
                        </div>
                    </div>

                    <!-- tab 5 -->
                    <div class="widgetbox login-information">
                        <h4 class="widgettitle">Collage 5[long image]</h4>
                        <div class="widgetcontent">
                            <p>
                                <label>Name*</label>
                                <span class="field"><input type="text" name="option_name5" id="option_name5" class="input-xxlarge" value="<?php echo $row_tab5['option_name'] ?>" /></span>
                            </p>
                            <p>
                                <label>Image *<small>size (699px x 216px)</small></label>
                                <span class="field">
                                <input type="file" name="option_image5" class="input-xlarge" value="" /><br />
                                <?php 
                                    if($row_tab5['option_image']!=""){
                                        echo '<img src="'.SITE_DOMAIN.$row_tab5['option_image'].'" />';
                                    }
                                ?>
                                </span>
                            </p>
                            <p>
                                <label>Caption*</label>
                                <span class="field"><input type="text" name="option_value5" id="option_value5" class="input-xxlarge" value="<?php echo $row_tab5['option_value'] ?>" /></span>
                            </p>
                        </div>
                    </div>    


                    <p class="stdformbutton">
                        <input type="submit" class="btn btn-primary" value="Submit Button">
                        <!-- <button class="btn btn-primary">Submit Button</button> -->
                        <button type="reset" class="btn">Reset Button</button>
                    </p>          
                 </form>

            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->

</div><!--mainwrapper-->
<?php include(ADMIN_TEMPLATE_PATH.'footer.php'); ?>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>