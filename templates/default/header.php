<header class="header">
    <div class="menu-top">
        <div class="default-width inline-container aright">
            <div class="item register">
                Register
            </div>
            <div class="item login">
                Login
            </div>
        </div>
    </div>
    <div class="main-menu">
        <div class="default-width">
            <div class="logo"><img src="<?php echo TEMPLATE_DOMAIN_IMAGES; ?>logo.png" width="100%"></div>
            <ul class="menu inline-container aleft">
                <li class="item"><a href="#">HOME</a></li>
                <li class="item"><a href="#">PROFILE</a></li>
                <li class="item">
                    <a href="#">PRODUCTS</a>
                    <!-- <ul class="sub-menu">
                        <li>HEALTH & WELLNESS</li>
                        <li>BEAUTY</li>
                        <li>HOME ESSENTIALS</li>
                    </ul> -->
                </li>
                <li class="item"><a href="#">OPPORTUNITY</a></li>
                <li class="item"><a href="#">CONTACT US</a></li>
            </ul>
        </div>
    </div>
</header>
