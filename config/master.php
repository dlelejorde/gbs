<?php
	$maindb_prefix = "youthforasiadb";
	$maintable_prefix = "yfa_";

	if(!defined('DB_SERVER')){
		define("DB_SERVER", 'localhost');
		define("DB_USER", 'root');
		define("DB_PASS", '');
		define("DB_NAME", $maindb_prefix."");
		define("TABLE_PREFIX", $maintable_prefix);
	}
?>
