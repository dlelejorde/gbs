<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";

if($_POST['actions']=="Add"){
	extract($_POST);

	if (trim($cat_name)== ""){
		$msg .= "name is required.<br />";
		$err++;
	}

	if ($err==0){


		$data = array('cat_parent'=>"'".$cat_parent."'",
			'cat_name'=>htmlspecialchars($cat_name,ENT_QUOTES),
			'cat_slug'=>makeUrlFriendly(strtolower($cat_name)),
			'cat_added_by'=>$admin,
			'cat_date_added'=>strtotime("now"));
		$conn->insert($data,$maintable_prefix.'_category','');		

		$_SESSION['message'] = '<h4 class="widgettitle title-success">New category added. '.$cat_name.'</h4>';
		header("Location: index.php");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);


	if (trim($cat_name)== ""){
		$msg .= "Name is required.<br />";
		$err++;
	}


	if ($err==0){

		//update flashbox table
		$data = array('cat_parent'=>"'".$cat_parent."'",
			'cat_name'=>htmlspecialchars($cat_name,ENT_QUOTES),
			'cat_slug'=>makeUrlFriendly(strtolower($cat_name)),
			'cat_edited_by'=>$admin,
			'cat_date_edited'=>strtotime("now"));
		$conn->update_tbl($data,$maintable_prefix.'_category',"cat_id = '".$cat_id."'");		

		$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$cat_name.' Edited.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?id=".$cat_id);	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?id=".$cat_id);	
	}
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){

		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_category where cat_id ='".$id."'");
		echo '1';
	} else {
		echo '0';
	}

}