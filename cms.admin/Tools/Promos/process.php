<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";

if($_POST['actions']=="Add"){
	extract($_POST);

	if (trim($promo_title)== ""){
		$msg .= "Tittle is required.<br />";
		$err++;
	}

	if (trim($elm1)== ""){
		$msg .= "Text is required.<br />";
		$err++;
	}

	if ($err==0){
		//update products table
		$data = array('promo_title'=>htmlspecialchars($promo_title,ENT_QUOTES),
			'promo_slug'=>makeUrlFriendly(strtolower($promo_title)),
			'promo_meta_description'=>$promo_meta_description,
			'promo_meta_keywords'=>$promo_meta_keywords,
			'promo_blurb'=>$promo_blurb,
			'promo_content'=>htmlspecialchars($elm1,ENT_QUOTES),
			'promo_status'=>$promo_status,
			'promo_added_by'=>$admin,
			'promo_date_added'=>strtotime("now"));
		$conn->insert($data,$maintable_prefix.'_promos','');	
		$id = $conn->get_last_id($maintable_prefix.'_promos','promo_id');

		foreach ($promo_country as $key => $value) {
			$data = array('promo_id'=>$id,
				'country_id'=>$value);
			$conn->insert($data,$maintable_prefix.'_promos_country','');	
		}	

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["promo_image"])) && ($_FILES['promo_image']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['promo_image']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["promo_image"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/promo/'.strtotime($date).'-'.$filename;
			  $filename = '/images/promo/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['promo_image']['tmp_name'],$newname))) {

					$data = array('promo_image'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_promos',"promo_id = '".$id."'");

				} else {

				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["promo_image"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}				

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["promo_thumbs"])) && ($_FILES['promo_thumbs']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['promo_thumbs']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["promo_thumbs"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/promo/'.time().'-'.$filename;
			  $filename = '/images/promo/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['promo_thumbs']['tmp_name'],$newname))) {

					$data = array('promo_thumbs'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_promos',"promo_id = '".$id."'");

				} else {

				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["promo_thumbs"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}		

		$_SESSION['message'] = '<h4 class="widgettitle title-success">New promo added. '.$promo_title.'<br />'.$msg.'</h4>';
		header("Location: index.php");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);

	if (trim($promo_title)== ""){
		$msg .= "Tittle is required.<br />";
		$err++;
	}

	if (trim($elm1)== ""){
		$msg .= "Text is required.<br />";
		$err++;
	}

	if ($err==0){

		extract($_POST);
		//update products table 
		$data = array('promo_title'=>htmlspecialchars($promo_title,ENT_QUOTES),
			'promo_slug'=>makeUrlFriendly(strtolower($promo_title)),
			'promo_meta_description'=>$promo_meta_description,
			'promo_meta_keywords'=>$promo_meta_keywords,
			'promo_blurb'=>$promo_blurb,
			'promo_content'=>htmlspecialchars($elm1,ENT_QUOTES),
			'promo_status'=>$promo_status,
			'promo_edited_by'=>$admin,
			'promo_date_edited'=>strtotime("now"));
		$conn->update_tbl($data,$maintable_prefix.'_promos',"promo_id = '".$promo_id."'");	

		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_promos_country where promo_id ='".$promo_id."'");

		foreach ($promo_country as $key => $value) {
			$data = array('promo_id'=>$promo_id,
				'country_id'=>$value);
			$conn->insert($data,$maintable_prefix.'_promos_country','');	
		}	

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["promo_image"])) && ($_FILES['promo_image']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['promo_image']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["promo_image"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/promo/'.time().'-'.$filename;
			  $filename = '/images/promo/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['promo_image']['tmp_name'],$newname))) {

					$data = array('promo_image'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_promos',"promo_id = '".$promo_id."'");

				} else {

				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["promo_image"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}				


		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["promo_thumbs"])) && ($_FILES['promo_thumbs']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['promo_thumbs']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["promo_thumbs"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/promo/'.time().'-'.$filename;
			  $filename = '/images/promo/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['promo_thumbs']['tmp_name'],$newname))) {

					$data = array('promo_thumbs'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_promos',"promo_id = '".$promo_id."'");

				} else {

				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["promo_thumbs"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}	
		
		$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$promo_title.' Edited.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?id=".$promo_id);	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?id=".$promo_id);	
	}
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){
		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_promos where promo_id ='".$id."'");
		echo '1';
	} else {
		echo '0';
	}

}