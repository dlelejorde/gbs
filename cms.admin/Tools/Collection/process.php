<?php

include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";


if($_POST['actions']=="Add"){
	extract($_POST);

	if (trim($prod_name)== ""){
		$msg .= "Tittle is required.<br />";
		$err++;
	}

	if ($prod_price==""){
		$msg .= "Price is required.<br />";
		$err++;
	}

	if ($err==0){
		//update products table
		$data = array('prod_name'=>htmlspecialchars($prod_name,ENT_QUOTES),
			'prod_slug'=>makeUrlFriendly(strtolower($prod_name)),
			'prod_price'=>$prod_price,
			'prod_sale'=>$prod_sale,
			'prod_stock'=>$prod_stock,
			'prod_price_sale'=>$prod_price_sale,
			'prod_outofstock'=>$prod_outofstock,
			'prod_details'=>htmlspecialchars($prod_details,ENT_QUOTES),
			'prod_note'=>htmlspecialchars($prod_note,ENT_QUOTES),
			'prod_popular'=>$prod_popular,
			'prod_rated'=>$prod_rated,
			'prod_feature'=>$prod_feature,
			'prod_added_by'=>$admin,
			'prod_date_added'=>strtotime("now"));
		$conn->insert($data,$maintable_prefix.'_products','');	
		$id = $conn->get_last_id($maintable_prefix.'_products','prod_id');	

		foreach ($cat_id as $key => $value) {
			$data = array('prod_id'=>$id,
				'cat_id'=>$value);
			$conn->insert($data,$maintable_prefix.'_product_cat','');	
		}	

		if(count($sizes)>0){
			foreach ($sizes as $key => $value) {
				$data = array('prod_id'=>$id,
					'size'=>$value,
					'quantity'=>$pQty[$key]
					);
				$conn->insert($data,$maintable_prefix.'_products_sizes','');	
			}				
		}

		$countProd = count($_POST['prod_alt']);

		for($x=0;$x<$countProd;$x++){

			$data = array('prod_alt'=>htmlspecialchars($_POST['prod_name'][$x],ENT_QUOTES),
				'prod_id' => $id
			);	
			$conn->insert($data,$maintable_prefix.'_products_images','');	
			$pid = $conn->get_last_id($maintable_prefix.'_products_images','prod_image_id');	

			$pDate = strtotime("now");

			/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
			if((!empty($_FILES["prod_thumbs"]["name"][$x])) && ($_FILES['prod_thumbs']['error'][$x] == 0)) {
				
				//Check if the file is JPEG image and it's size is less than 4mb

				$filename = basename($_FILES['prod_thumbs']['name'][$x]);
				$ext = substr($filename, strrpos($filename, '.') + 1);

				if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["prod_thumbs"]["size"][$x] < 4194304)) {

				//Determine the path to which we want to save this file
				  $newname = SITE_PATH.'images/prod/th/'.$pDate.'-'.$filename;
				  $filename = '/images/prod/th/'.$pDate.'-'.$filename;
				  //Check if the file with the same name is already exists on the server
				  if (!file_exists($newname)) {
					//Attempt to move the uploaded file to it's new place
					if ((move_uploaded_file($_FILES['prod_thumbs']['tmp_name'][$x],$newname))) {

						$data = array('prod_thumbs'=>$filename);
						$conn->update_tbl($data,$maintable_prefix.'_products_images',"prod_image_id = '".$pid."'");

					} else {

					   $msg .= "Error: A problem occurred during file upload!";
					}
				  } else {
					 $msg .= "Error: File ".$_FILES["prod_thumbs"]["name"][$x]." already exists";
				  }
				} else {
				 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
				}
				
			} else {
				//no else
			}	

			/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
			if((!empty($_FILES["prod_image"]["name"][$x])) && ($_FILES['prod_image']['error'][$x] == 0)) {
				
				//Check if the file is JPEG image and it's size is less than 4mb

				$filename = basename($_FILES['prod_image']['name'][$x]);
				$ext = substr($filename, strrpos($filename, '.') + 1);

				if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["prod_image"]["size"][$x] < 4194304)) {

				//Determine the path to which we want to save this file
				  $newname = SITE_PATH.'images/prod/'.$pDate.'-'.$filename;
				  $filename = '/images/prod/'.$pDate.'-'.$filename;
				  //Check if the file with the same name is already exists on the server
				  if (!file_exists($newname)) {
					//Attempt to move the uploaded file to it's new place
					if ((move_uploaded_file($_FILES['prod_image']['tmp_name'][$x],$newname))) {

						$data = array('prod_image'=>$filename);
						$conn->update_tbl($data,$maintable_prefix.'_products_images',"prod_image_id = '".$pid."'");

					} else {

					   $msg .= "Error: A problem occurred during file upload!";
					}
				  } else {
					 $msg .= "Error: File ".$_FILES["prod_image"]["name"][$x]." already exists";
				  }
				} else {
				 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
				}
				
			} else {
				//no else
			}	

		}			


		$_SESSION['message'] = '<h4 class="widgettitle title-success">New product added. '.$prod_name.'<br />'.$msg.'</h4>';
		header("Location: index.php");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);

	if (trim($prod_name)== ""){
		$msg .= "Tittle is required.<br />";
		$err++;
	}

	if ($prod_price==""){
		$msg .= "Price is required.<br />";
		$err++;
	}


	if ($err==0){

		$data = array('prod_name'=>htmlspecialchars($prod_name,ENT_QUOTES),
			'prod_slug'=>makeUrlFriendly(strtolower($prod_name)),
			'prod_price'=>$prod_price,
			'prod_sale'=>$prod_sale,
			'prod_stock'=>$prod_stock,
			'prod_price_sale'=>$prod_price_sale,
			'prod_outofstock'=>$prod_outofstock,
			'prod_details'=>htmlspecialchars($prod_details,ENT_QUOTES),
			'prod_note'=>htmlspecialchars($prod_note,ENT_QUOTES),
			'prod_popular'=>$prod_popular,
			'prod_rated'=>$prod_rated,
			'prod_feature'=>$prod_feature,
			'prod_edited_by'=>$admin,
			'prod_date_edited'=>strtotime("now"));
		$conn->update_tbl($data,$maintable_prefix.'_products',"prod_id = '".$prod_id."'");	


		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_product_cat where prod_id = '".$prod_id."'");

		foreach ($cat_id as $key => $value) {
			$data = array('prod_id'=>$prod_id,
				'cat_id'=>$value);
			$conn->insert($data,$maintable_prefix.'_product_cat','');	
		}	

		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_products_sizes where prod_id = '".$prod_id."'");

		if(count($sizes)>0){
			foreach ($sizes as $key => $value) {
				$data = array('prod_id'=>$prod_id,
					'size'=>$value,
					'quantity'=>$pQty[$key]);
				$conn->insert($data,$maintable_prefix.'_products_sizes','');	
			}	
		}

		$countProd = count($_POST['prod_alt']);

		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_products_images where prod_id = '".$prod_id."'");

		for($x=0;$x<$countProd;$x++){

			$data = array('prod_alt'=>htmlspecialchars($_POST['prod_alt'][$x],ENT_QUOTES),
				'prod_id' => $prod_id,
				'prod_thumbs' => $hprod_thumbs[$x],
				'prod_image' => $hprod_image[$x]
			);	
			$conn->insert($data,$maintable_prefix.'_products_images','');	
			$pid = $conn->get_last_id($maintable_prefix.'_products_images','prod_image_id');	

			$pDate = strtotime("now");

			/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
			if((!empty($_FILES["prod_thumbs"]["name"][$x])) && ($_FILES['prod_thumbs']['error'][$x] == 0)) {
				
				//Check if the file is JPEG image and it's size is less than 4mb

				$filename = basename($_FILES['prod_thumbs']['name'][$x]);
				$ext = substr($filename, strrpos($filename, '.') + 1);

				if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["prod_thumbs"]["size"][$x] < 4194304)) {

				//Determine the path to which we want to save this file
				  $newname = SITE_PATH.'images/prod/th/'.$pDate.'-'.$filename;
				  $filename = '/images/prod/th/'.$pDate.'-'.$filename;
				  //Check if the file with the same name is already exists on the server
				  if (!file_exists($newname)) {
					//Attempt to move the uploaded file to it's new place
					if ((move_uploaded_file($_FILES['prod_thumbs']['tmp_name'][$x],$newname))) {

						$data = array('prod_thumbs'=>$filename);
						$conn->update_tbl($data,$maintable_prefix.'_products_images',"prod_image_id = '".$pid."'");

					} else {

					   $msg .= "Error: A problem occurred during file upload!";
					}
				  } else {
					 $msg .= "Error: File ".$_FILES["prod_thumbs"]["name"][$x]." already exists";
				  }
				} else {
				 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
				}
				
			} else {
				//no else
			}	

			/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
			if((!empty($_FILES["prod_image"]["name"][$x])) && ($_FILES['prod_image']['error'][$x] == 0)) {
				
				//Check if the file is JPEG image and it's size is less than 4mb

				$filename = basename($_FILES['prod_image']['name'][$x]);
				$ext = substr($filename, strrpos($filename, '.') + 1);

				if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["prod_image"]["size"][$x] < 4194304)) {

				//Determine the path to which we want to save this file
				  $newname = SITE_PATH.'images/prod/'.$pDate.'-'.$filename;
				  $filename = '/images/prod/'.$pDate.'-'.$filename;
				  //Check if the file with the same name is already exists on the server
				  if (!file_exists($newname)) {
					//Attempt to move the uploaded file to it's new place
					if ((move_uploaded_file($_FILES['prod_image']['tmp_name'][$x],$newname))) {

						$data = array('prod_image'=>$filename);
						$conn->update_tbl($data,$maintable_prefix.'_products_images',"prod_image_id = '".$pid."'");

					} else {

					   $msg .= "Error: A problem occurred during file upload!";
					}
				  } else {
					 $msg .= "Error: File ".$_FILES["prod_image"]["name"][$x]." already exists";
				  }
				} else {
				 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
				}
				
			} else {
				//no else
			}	


		}		
		
		$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$prod_name.' Edited.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?id=".$prod_id);	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?id=".$prod_id);	
	}
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){
		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_products where prod_id ='".$id."'");
		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_product_cat where prod_id = '".$id."'");
		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_products_sizes where prod_id = '".$id."'");
		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_products_images where prod_id = '".$id."'");

		echo '1';
	} else {
		echo '0';
	}

}