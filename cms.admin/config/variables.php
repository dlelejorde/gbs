<?php
session_start();
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
include($_SERVER['DOCUMENT_ROOT'].'/config/master.php');
$maintable_prefix = 'yfa';

define('ADMIN_TITLE',"Admin CMS");
define('SITE_DOMAIN',"http://youth4asia/");
define('SITE_ADMIN_DOMAIN',"http://youth4asia/cms.admin/");
define('SITE_PATH',"C:/xampp/htdocs/youth4asia/");
define('SITE_ADMIN_PATH',"C:/xampp/htdocs/youth4asia/cms.admin/");

define('ADMIN_TEMPLATE_DOMAIN', SITE_ADMIN_DOMAIN."Views/default/");
define('ADMIN_TEMPLATE_PATH', SITE_ADMIN_PATH."Views/default/");
define('ADMIN_LIBRARIES_DOMAIN', SITE_ADMIN_DOMAIN."Libraries/");
define('ADMIN_LIBRARIES_PATH', SITE_ADMIN_PATH."Libraries/");
define('ADMIN_TOOLS_DOMAIN', SITE_ADMIN_DOMAIN."Tools/");
define('ADMIN_TOOLS_PATH', SITE_ADMIN_PATH."Tools/");

define('USERSTBL', $maintable_prefix."_adminusers");

include(ADMIN_LIBRARIES_PATH."DBConn_MySQL.class.php");
include(ADMIN_LIBRARIES_PATH."functions.php");

$conn = new MySQL_Conn();
$connection = $conn->DBC0nn(DB_USER,DB_PASS,DB_NAME,DB_SERVER);

$GLOBALS['Stautus'] = array('0'=>'Draft','1'=>'Published','2'=>'Featured');


?>
