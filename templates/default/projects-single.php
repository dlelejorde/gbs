            <section class="padHead organization">
                <div class="banner-org">
                    <div class="inner">
                        <h2>Project Name</h2>
                        <div class="wiggle-white"></div>
                        <div class="banner-logo"><img src="<?php echo TEMPLATE_DOMAIN_IMAGES; ?>org-head-icon.png" /></div>
                    </div>
                </div>

                <div class="inner org">
                    <div class="pad over">
                        <div class="left">
                            <img src="<?php echo TEMPLATE_DOMAIN_IMAGES; ?>projects-banner.png" />
                            
                            <div class="padNe">
                                <table cellpadding="0" cellspacing="0" border="1" border-color="#c2c2c2">
                                    <tr>
                                        <td>Health and Hygienge</td>
                                        <td>2012-2015</td>
                                    </tr>
                                    <tr>
                                        <td>Philippines</td>
                                        <td>$2,000,000</td>
                                    </tr>
                                </table>

                                <p class="content-title">About the Project</p>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. </p>

                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. </p>
                            </div>
                        </div>

                        <div class="right">
                            <ul>
                                <li>
                                    Jane Doe<br />
                                    Associate Sector Head
                                </li>
                                <li>
                                    organization@info.com
                                </li>
                                <li>
                                    +63 2 911 86 54
                                </li>
                                <li>
                                    <a href="#">www.organization.com</a><br /><br />
                                    <a href="#"><i class="fa fa-facebook"></i>facebook/orgname</a><br /><br />
                                    <a href="#"><i class="fa fa-twitter"></i>twitter/orgname</a>
                                </li>
                                <li class="over">
                                    <span class="download"></span>
                                    <a class="downloadTxt">Download</a>
                                </li>
                            </ul>
                        </div>
                    </div>


                </div>
            </section>
