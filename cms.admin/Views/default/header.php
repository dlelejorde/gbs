<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title><?php echo ADMIN_TITLE; ?></title>
<link rel="stylesheet" href="<?php echo SITE_ADMIN_DOMAIN; ?>css/style.default.css" type="text/css" />

<link rel="stylesheet" href="<?php echo SITE_ADMIN_DOMAIN; ?>css/responsive-tables.css">
<script type="text/javascript">
	var publicDomain = '<?php echo SITE_ADMIN_DOMAIN; ?>';

</script>

<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/modernizr.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/responsive-tables.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/custom.js"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->

<script type="text/javascript">
	jQuery(document).ready(function(){
	
		// Date Picker
		jQuery("#datepicker").datepicker({ dateFormat: 'yy-mm-dd' });

	});
</script>
</head>
