<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";

if($_POST['actions']=="Add"){
	extract($_POST);

	if (trim($os_name)==""){
		$msg .= "Name is required.<br />";
		$err++;
	}

	if (trim($os_address)==""){
		$msg .= "Address is required.<br />";
		$err++;
	}

	if ($err==0){
		//update products table
		$data = array('os_name'=>htmlspecialchars($os_name,ENT_QUOTES),
			'os_address'=>$os_address,
			'os_google_maps'=>htmlspecialchars($os_google_maps),
			'os_country'=>$os_country,
			'os_added_by'=>$admin,
			'os_date_added'=>strtotime("now"));
		$conn->insert($data,$maintable_prefix.'_our_stores','');	

		$_SESSION['message'] = '<h4 class="widgettitle title-success">New store added. '.$os_name.'<br />'.$msg.'</h4>';
		header("Location: index.php");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);

	if (trim($os_name)== ""){
		$msg .= "Name is required.<br />";
		$err++;
	}

	if (trim($os_address)== ""){
		$msg .= "Address is required.<br />";
		$err++;
	}

	if ($err==0){

		extract($_POST);
		//update products table 
		$data = array('os_name'=>htmlspecialchars($os_name,ENT_QUOTES),
			'os_address'=>$os_address,
			'os_google_maps'=>htmlspecialchars($os_google_maps),
			'os_country'=>$os_country,
			'os_edited_by'=>$admin,
			'os_date_edited'=>strtotime("now"));
		$conn->update_tbl($data,$maintable_prefix.'_our_stores',"os_id = '".$os_id."'");	

		$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$os_title.' Edited.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?id=".$os_id);	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?id=".$os_id);	
	}
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){
		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_our_stores where os_id ='".$id."'");
		echo '1';
	} else {
		echo '0';
	}

}