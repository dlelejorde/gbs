<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();
$admin = $_SESSION['uID'];
$date = date("Y-m-d G:i:s");
$err = 0;
$msg = "";

if($_POST['actions']=="Add"){
	extract($_POST);

	if (trim($username)== ""){
		$msg .= "Username is required.<br />";
		$err++;
	}

	if (trim($password)== ""){
		$msg .= "Password is required.<br />";
		$err++;
	}

	if (trim($name)== ""){
		$msg .= "Name is required.<br />";
		$err++;
	}

	if (trim($email)== ""){
		$msg .= "Email is required.<br />";
		$err++;
	}

	if ($err==0){
		//add member table
		$data = array('username'=>$username,
			'email'=>$email,
			'password'=>md5($password),
			'userid'=>md5($username),
			'name'=>$name,
			'timestamp'=>time(),
			'userlevel'=>1);
		$conn->insert($data,$maintable_prefix.'_adminusers','');			
		$id = $conn->get_last_id($maintable_prefix.'_adminusers','member_id');

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["photos"])) && ($_FILES['photos']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['photos']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["photos"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_ADMIN_PATH.'images/avatar/'.strtotime($date).'-'.$filename;
			  $filename = '/images/avatar/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['photos']['tmp_name'],$newname))) {

					$data = array('photo'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_adminusers',"member_id = '".$id."'");

				} else {

				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["photos"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}				

		$_SESSION['message'] = '<h4 class="widgettitle title-success">New member added. '.$username.' <br />'.$msg.'</h4>';
		header("Location: index.php?nav=users");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=users");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);

	if (trim($username)== ""){
		$msg .= "Username is required.<br />";
		$err++;
	}

	if (trim($name)== ""){
		$msg .= "Name is required.<br />";
		$err++;
	}

	if (trim($email)== ""){
		$msg .= "Email is required.<br />";
		$err++;
	}

	if ($err==0){
		//update member table
		$data = array('username'=>$username,
			'email'=>$email,
			'userid'=>md5($username),
			'name'=>$name);
		$conn->update_tbl($data,$maintable_prefix.'_adminusers',"member_id = '".$member_id."'");			

		if(trim($password)!=""){
			$data = array('password'=>md5($password),);	
			$conn->update_tbl($data,$maintable_prefix.'_adminusers',"member_id = '".$member_id."'");			
		}		

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["photos"])) && ($_FILES['photos']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['photos']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["photos"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_ADMIN_PATH.'images/avatar/'.strtotime($date).'-'.$filename;
			  $filename = '/images/avatar/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['photos']['tmp_name'],$newname))) {
				  
					$data = array('photo'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_adminusers',"member_id = '".$member_id."'");			

				} else {
				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["photos"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}		

		$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$username.' Edited.<br />'.$msg.'</h4>';
		header("Location: index.php?nav=users");		

	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=users");	
	} 
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){

		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_adminusers where member_id ='".$id."'");

		echo '1';
	} else {
		echo '0';
	}

}