<?php 
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php'); 
$conn->checklog();
include(ADMIN_TEMPLATE_PATH.'header.php'); 
?>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/tinymce/jquery.tinymce.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/wysiwyg.js"></script>
<?php



$row_tab1 = $conn->array_rs_single("select * from ".$maintable_prefix."_options WHERE option_type = 'facebook' ");
$row_tab2 = $conn->array_rs_single("select * from ".$maintable_prefix."_options WHERE option_type = 'twitter' ");


?>
<div class="mainwrapper">
    
    <?php
        include(ADMIN_TEMPLATE_PATH.'mainHead.php');
        include(ADMIN_TEMPLATE_PATH.'navigation.php');
    ?>


    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="<?php echo SITE_ADMIN_DOMAIN; ?>dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Flashbox</li>
            
            <li class="right">
                <a href="" data-toggle="dropdown" class="dropdown-toggle"><i class="icon-tint"></i> Color Skins</a>
                <ul class="dropdown-menu pull-right skin-color">
                    <li><a href="default">Default</a></li>
                    <li><a href="navyblue">Navy Blue</a></li>
                    <li><a href="palegreen">Pale Green</a></li>
                    <li><a href="red">Red</a></li>
                    <li><a href="green">Green</a></li>
                    <li><a href="brown">Brown</a></li>
                </ul>
            </li>
        </ul>
        
        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <!--h5>Categories</h5-->
                <h1>Social Media Links</h1>
            </div>
        </div><!--pageheader-->
        
        <div class="maincontent">
            <div class="maincontentinner">
                <?php echo $_SESSION['message']; ?>

                <form class="stdform stdform2" method="post" name="form1" action="<?php echo SITE_ADMIN_DOMAIN; ?>Tools/Options/process.php" enctype="multipart/form-data">
                    <input type="hidden" name="actions" id="actions" value="social" />

                    <!-- facebook -->
                    <div class="widgetbox login-information">
                        <h4 class="widgettitle">Facebook Link</h4>
                        <div class="widgetcontent">
                            <p>
                                <label>Link*</label>
                                <span class="field"><input type="text" name="option_link1" id="option_link1" class="input-xxlarge" value="<?php echo $row_tab1['option_link'] ?>" /></span>
                            </p>
                        </div>
                    </div>  

                    <!-- twitter -->
                    <div class="widgetbox login-information">
                        <h4 class="widgettitle">Twitter Link</h4>
                        <div class="widgetcontent">
                            <p>
                                <label>Link*</label>
                                <span class="field"><input type="text" name="option_link2" id="option_link2" class="input-xxlarge" value="<?php echo $row_tab2['option_link'] ?>" /></span>
                            </p>
                        </div>
                    </div>  


                    <p class="stdformbutton">
                        <input type="submit" class="btn btn-primary" value="Submit Button">
                        <!-- <button class="btn btn-primary">Submit Button</button> -->
                        <button type="reset" class="btn">Reset Button</button>
                    </p>          
                 </form>

            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->

</div><!--mainwrapper-->
<?php include(ADMIN_TEMPLATE_PATH.'footer.php'); ?>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>