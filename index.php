<?php
    include($_SERVER['DOCUMENT_ROOT'].'/config/variables.php');

    include LIB_PATH."LoginClass.php";
    $auth = new Login;

    //include required functions and classes
    require_once(LIB_PATH.'public.functions.php');
    require_once(LIB_PATH.'pagination.class.php');

    //get all $_GET variables
    $_GET['channel'] = (isset($_GET['channel'])) ? $_GET['channel'] : '';

    $channel = clean_variable($_GET['channel'],2);
    $subchannel = '';
    if(trim($channel) == ''){ $channel = 'home'; }
    if(isset($_GET['subchannel'])){ $subchannel = clean_variable($_GET['subchannel'],2); }
    if(isset($_GET['slug'])){ $slug = clean_variable($_GET['slug'],2); }

    $contentBased = array("home","products","projects","country-profile","register","edit-profile");


    //template
    if(in_array($channel, $contentBased)){
        $contentTPL = $channel.'.php';
        if (file_exists(TEMPLATE_PATH.'/css/'.$channel.'.css')) {
            $css = '<link rel="stylesheet" href="'.TEMPLATE_DOMAIN_CSS.$channel.'.css?v='.$globals['css_version'].'" type="text/css" media="screen, print" />';
        }
        if (file_exists(TEMPLATE_PATH.'/js/'.$channel.'.js')) {
            $javascripts .= '<script src="'.TEMPLATE_DOMAIN_JS.''.$channel.'.js" type="text/javascript"></script>';
        }
        if (file_exists(OBJECTS_TEMPLATE_PATH.'/'.$channel.'/'.$channel.'.obj.php')) {
            include(OBJECTS_TEMPLATE_PATH.'/'.$channel.'/'.$channel.'.obj.php');
        }
    } else {
        $contentTPL = '404.php';
    }

    include(TEMPLATE_PATH.'/template.php');

?>
