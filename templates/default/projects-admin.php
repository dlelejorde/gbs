            <section class="padHead ">


                <div class="inner registration">
                    <div class="over pad">
                        <div class="result-title">Projects</div>
                        <div class="wiggle-black"></div>


						
                        <div class="results projects">
                        	<div class="result-container">
                        	<?php foreach($projects as $project) { ?>
								<div class="result-list">
                                    <a href="#" class="media">
                                        <div class="img"><img src="<?php echo IMG_URI.$project['photo']; ?>" /></div>
                                        <div class="bd">
                                            <div class="org-name"><?php echo $project['project_name']; ?></div>
                                            <div class="org-details f16">
                                                <span class="bold">Country:</span> <?php echo $project['country']; ?><br />
                                                <span class="bold">Focus Area:</span> <?php echo $project['focusArea']; ?>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                        	<?php } ?>
                        	</div>
                        	<div class="result-pagination textCenter f16 bold">
                                <a href="#"><span>1</span></a>
                                <a href="#"><span>2</span></a>
                                <a href="#"><span>3</span></a>
                                <a href="#"><span>4</span></a>
                                <a href="#"><span>5</span></a>
                            </div>
                        </div>
                    </div>


                </div>
            </section>