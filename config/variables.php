<?php
	session_start();
	//include db config
	include($_SERVER['DOCUMENT_ROOT']."/config/master.php");

	//page load timer initialization
	$startTime = explode(' ', microtime());
	$startTime = $startTime[1] + $startTime[0];

	include $_SERVER['DOCUMENT_ROOT'].'/lib/Database.php'; //pdo class
	Database::setConnectionInfo(DB_NAME, DB_USER, DB_PASS, 'mysql', DB_SERVER);

	include $_SERVER['DOCUMENT_ROOT'].'/lib/public.functions.php'; 

	// $conn = mysql_connect(DB_SERVER,DB_USER,DB_PASS) or die ("Could not connect to MySQL");
	// if (!$conn) {echo "Login failed.";exit;}
	// mysql_select_db(DB_MAIN_NAME) or die ("Could not select ".DB_MAIN_NAME." database");

	$title = "Golden Biz Solutions";
	$metaDesc = "Golden Biz Solutions";
	$metaKeywords = "Golden Biz Solutions";

	//removed when in live
	$config['template'] = 'default';
	$config['publicdomain'] = 'http://gbs/';
	$config['publicpath'] = $_SERVER['DOCUMENT_ROOT'].'/';
	
	define("OBJECTS_PATH", $config['publicpath']."objects/");
	define("OBJECTS_TEMPLATE_PATH", $config['publicpath']."objects/".$config['template']);
	define("OBJECTS_URI", $config['publicdomain']."objects/");
	define("OBJECTS_TEMPLATE_URI", $config['publicdomain']."objects/".$config['template'].'/');

	define("LIB_PATH", $config['publicpath']."lib/");
	define("LIB_URI", $config['publicdomain']."lib/");

	define("IMG_PATH", $config['publicpath']."images/");
	define("IMG_URI", $config['publicdomain']."images/");

	define("TEMPLATE_PATH", $_SERVER['DOCUMENT_ROOT'].'/templates/'.$config['template']);
	define("TEMPLATE_DOMAIN", $config['publicdomain'].'templates/'.$config['template'].'/');
	define("TEMPLATE_DOMAIN_CSS", $config['publicdomain'].'templates/'.$config['template'].'/css/');
	define("TEMPLATE_DOMAIN_JS", $config['publicdomain'].'templates/'.$config['template'].'/js/');
	define("TEMPLATE_DOMAIN_IMAGES", $config['publicdomain'].'templates/'.$config['template'].'/images/');

	define("ADMIN_PATH", $config['publicpath']."admin/");
	define("ADMIN_URI", $config['publicdomain']."admin/");
	define('HOST',$config['publicdomain']);
	$GLOBALS['publicpath'] =  $config['publicpath']."objects/";

	$today = date("Y-m-d");
	$prevmonth =  date("Y"). date("m")-1 . date("d");

	//css settings
	$globals['css_version'] = '1.01';

	$GLOBALS['verified_forgot'] = 0;

	//recaptcha keys
	define('PublicKey','6Lfr5-4SAAAAAIg5aC8X2fFg437yQdAoDVkxomcZ');
	define('PrivateKey','6Lfr5-4SAAAAADzjd6yv1rFJqdmygG-pKEO9Cxn4');

?>