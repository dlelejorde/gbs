<?php 
	
	include($_SERVER['DOCUMENT_ROOT'].'/config/variables.php');
	
	$userFields = array(
		'password',
		'cpassword',
		'email'
	);

	if($_POST['type'] == 'organization'){

		$id   = date('s').date('i').date('H').date('m').date('d').date('Y');
		
		$slug = makeUrlFriendly($_POST['organization_name']);

		Database::setFetchMode(PDO::FETCH_COLUMN);
		$organizationFields = Database::getResult("DESCRIBE ".TABLE_PREFIX."organizations");

		$orgQueryString  = "INSERT into ".TABLE_PREFIX."organizations SET user_id='".$id."', ";

		foreach ($_POST as $key => $value) {
			if(empty($value)){
				echo 'Please complete all fields.';
				return;
			} else {
				if(!in_array($key, $userFields) && in_array($key, $organizationFields)){
					$orgQueryString .= $key." = '".$value."', ";
				} 
			}
		}

		$orgQueryString .= ("organization_slug = '".$slug."',");

		if($_POST['password'] != $_POST['cpassword']){
			echo 'Passwords do not match.';
			return;
		}

		/***** Organization photo file upload *****/
		$folderName    = '/organization/';
		$target_dir    = IMG_PATH.$folderName;
		$fileName      = basename($_FILES["organization_photo"]["name"]);
		$uploadOk      = 0;
		$target_file   = $target_dir.time().'-'.$fileName;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		$fileSizeLimit = 4194304; // 4MB in bytes

		if(isset($fileName) && !empty($fileName)){

			$tempName = $_FILES["organization_photo"]["tmp_name"];

			// Check if image file is a actual image or fake image
		    $check = getimagesize($tempName);
		    if($check !== false) {
		        //echo "File is an image - " . $check["mime"] . ".";
		        $uploadOk = 1;
		    } else {
		        echo "File is not an image.";
		        return;
		    }

			// Check file size
			if ($_FILES["organization_photo"]["size"] > $fileSizeLimit) {
			    echo "Sorry, your file is too large. 2MB maximum limit exceeded.";
			    return;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
			    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			    return;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
			    echo "Sorry, your file was not uploaded.";
			    return;
			// if everything is ok, try to upload file
			} else {
			    if (move_uploaded_file($tempName, $target_file)) {
			        echo "The file ".basename($fileName). " has been uploaded.";
			        $orgQueryString .= ("organization_photo = '".$target_file."',");
			    } else {
			        echo "Sorry, there was an error uploading your file.";
			        return;
			    }
			}
		} else {
			echo 'Select a photo to upload.';
			return;
		}

		$password_salt = md5('y4a2015');	
		$password      = md5($_POST['email'].$_POST['password'].$password_salt);
		$code          = generateCode(5);

		$userQueryString = "INSERT into ".TABLE_PREFIX."user_account_info SET user_id='".$id."', username='".$_POST['email']."', password='".$password."', confirmation_code='".$code."', email='".$_POST['email']."', confirmation_status='0'";
		$orgQueryString  = substr_replace($orgQueryString, '', strrpos($orgQueryString, ','), 1);

		$userResult = Database::execute($userQueryString);
		$orgResult  = Database::execute($orgQueryString);

		if($userResult && $orgResult){
			echo 'Success!';
		} else {
			echo 'Failed.';
		}
	} else if($_POST['type'] == 'projects'){

		$organizationId = $_POST['organization_id'];

		Database::setFetchMode(PDO::FETCH_COLUMN);
		$projectFields = Database::getResult("DESCRIBE ".TABLE_PREFIX."projects");

		$queryString = "INSERT INTO ".TABLE_PREFIX."projects SET ";

		

		$_POST['status'] = ($_POST['status'] == 'PUBLISH') ? '1' : '0';

		foreach ($_POST as $key => $value) {
			if(empty($value) && $value==''){
				echo 'Please complete all fields.';
				return;
			} else {
				if(in_array($key, $projectFields)){
					$queryString .= $key." = '".$value."', ";
				} 
			}
		}

		/***** Project photo file upload *****/
		$folderName    = '/project_photos/';
		$target_dir    = IMG_PATH.$folderName;
		$fileName      = $_FILES["photo"]["name"];
		$uploadOk      = 0;
		$target_file   = $target_dir.basename($fileName);
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		$fileSizeLimit = 2000000; // 2MB in bytes

		if(isset($fileName) && !empty($fileName)){

			$tempName = $_FILES["photo"]["tmp_name"];

			// Check if image file is a actual image or fake image
		    $check = getimagesize($tempName);
		    if($check !== false) {
		        $uploadOk = 1;
		    } else {
		        echo "File is not an image.";
		        return;
		    }

			// Check file size
			if ($_FILES["photo"]["size"] > $fileSizeLimit) {
			    echo "Sorry, your file is too large. 2MB maximum limit exceeded.";
			    return;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
			    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			    return;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
			    echo "Sorry, your file was not uploaded.";
			    return;
			// if everything is ok, try to upload file
			} else {
			    if (move_uploaded_file($tempName, $target_file)) {
			        echo "The file ".basename($fileName). " has been uploaded.";
			         $queryString .= ("photo = 'images/".$folderName.$fileName."',");
			    } else {
			        echo "Sorry, there was an error uploading your file.";
			        return;
			    }
			}
		} else {
			echo 'Select a photo to upload.';
			return;
		}

		$queryString = substr_replace($queryString, '', strrpos($queryString, ','), 1);
		$result      = Database::execute($queryString);

		$queryString2 = "INSERT INTO ".TABLE_PREFIX."projects_organization SET project_id = LAST_INSERT_ID(), organization_id='".$organizationId."'";
		$result2      = Database::execute($queryString2);

		if($result && $result2){
			echo 'Success!';
		} else {
			echo 'Failed.';
		}
	}
	
?>