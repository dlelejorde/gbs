$(document).ready(function() {

    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 100,
      values: [ 0, 100 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) + " - $" + $( "#slider-range" ).slider( "values", 1 ) );

    $(document).on('click', '.main-menu-item, .menu a', function(){
        var data = $(this).data();
        $(data.target).slideToggle();

        $(data.target).on('click', function(e){
    		e.stopPropagation();
    	});
    });

    $('.form').on('submit', function(e){
    	e.preventDefault();

    	var 
			form     = $(this),
			formData = form.serialize(),
			data     = form.data(),
			targetEl = data.target;

    	$.ajax({
    		url: form[0].action,
    		type: form[0].method,
    		data: formData,
    		success: function(response){
    			$(targetEl).html(response);
    		}
    	});
    });

});