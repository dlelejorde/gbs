<?php
require_once('../config/variables.php');

include_once "LoginClass.php";
$auth = new Login;
session_start();

session_destroy();
header("Location: ".$config['publicdomain']);