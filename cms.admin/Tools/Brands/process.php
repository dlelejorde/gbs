<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";

if($_POST['actions']=="Add"){
	extract($_POST);

	if (trim($brand_name)== ""){
		$msg .= "Tittle is required.<br />";
		$err++;
	}

	if (trim($elm1)== ""){
		$msg .= "Text is required.<br />";
		$err++;
	}

	if ($err==0){
		//update products table
		$data = array('brand_name'=>htmlspecialchars($brand_name,ENT_QUOTES),
			'brand_slug'=>makeUrlFriendly(strtolower($brand_name)),
			'brand_writeup'=>htmlspecialchars($elm1,ENT_QUOTES),
			'brand_added_by'=>$admin,
			'brand_date_added'=>strtotime("now"));
		$conn->insert($data,$maintable_prefix.'_brand','');	
		$id = $conn->get_last_id($maintable_prefix.'_brand','brand_id');	

		foreach ($brand_cat as $key => $value) {
			$data = array('brand_id'=>$id,
				'cat_id'=>$value);
			$conn->insert($data,$maintable_prefix.'_brand_cat','');	
		}	

		foreach ($brand_country as $key => $value) {
			$data = array('brand_id'=>$id,
				'country_id'=>$value);
			$conn->insert($data,$maintable_prefix.'_brand_country','');	
		}	


		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["brand_logo"])) && ($_FILES['brand_logo']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['brand_logo']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["brand_logo"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/brands/'.time().'-'.$filename;
			  $filename = '/images/brands/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['brand_logo']['tmp_name'],$newname))) {

					$data = array('brand_logo'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_brand',"brand_id = '".$id."'");

				} else {

				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["brand_logo"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}				


		$countProd = count($_POST['prod_name']);

		for($x=0;$x<$countProd;$x++){

			/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
			if((!empty($_FILES["flashbox_image"]["name"][$x])) && ($_FILES['flashbox_image']['error'][$x] == 0)) {
				
				//Check if the file is JPEG image and it's size is less than 4mb

				$filename = basename($_FILES['flashbox_image']['name'][$x]);
				$ext = substr($filename, strrpos($filename, '.') + 1);

				if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["flashbox_image"]["size"][$x] < 4194304)) {

				//Determine the path to which we want to save this file
				  $newname = SITE_PATH.'images/brands/fb/'.strtotime("now").'-'.$filename;
				  $filename = '/images/brands/fb/'.strtotime("now").'-'.$filename;
				  //Check if the file with the same name is already exists on the server
				  if (!file_exists($newname)) {
					//Attempt to move the uploaded file to it's new place
					if ((move_uploaded_file($_FILES['flashbox_image']['tmp_name'][$x],$newname))) {

						$data = array(
							'flashbox_image'=>$filename,
							'flashbox_cat'=>$id
						);
						$conn->insert($data,$maintable_prefix.'_flashbox',"");

					} else {

					   $msg .= "Error: A problem occurred during file upload!";
					}
				  } else {
					 $msg .= "Error: File ".$_FILES["flashbox_image"]["name"][$x]." already exists";
				  }
				} else {
				 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
				}
				
			} else {
				//no else
			}
		}

		$_SESSION['message'] = '<h4 class="widgettitle title-success">New brand added. '.$brand_name.'<br />'.$msg.'</h4>';
		header("Location: index.php");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);

	if (trim($brand_name)== ""){
		$msg .= "Tittle is required.<br />";
		$err++;
	}

	if (trim($elm1)== ""){
		$msg .= "Text is required.<br />";
		$err++;
	}

	if ($err==0){

		extract($_POST);
		//update products table 
		$data = array('brand_name'=>htmlspecialchars($brand_name,ENT_QUOTES),
			'brand_slug'=>makeUrlFriendly(strtolower($brand_name)),
			'brand_writeup'=>htmlspecialchars($elm1,ENT_QUOTES),
			'brand_edited_by'=>$admin,
			'brand_date_edited'=>strtotime("now"));
		$conn->update_tbl($data,$maintable_prefix.'_brand',"brand_id = '".$brand_id."'");			

		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_brand_cat where brand_id ='".$brand_id."'");
		
		foreach ($brand_cat as $key => $value) {
			$data = array('brand_id'=>$brand_id,
				'cat_id'=>$value);
			$conn->insert($data,$maintable_prefix.'_brand_cat','');	
		}	

		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_brand_country where brand_id ='".$brand_id."'");

		foreach ($brand_country as $key => $value) {
			$data = array('brand_id'=>$brand_id,
				'country_id'=>$value);
			$conn->insert($data,$maintable_prefix.'_brand_country','');	
		}	


		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["brand_logo"])) && ($_FILES['brand_logo']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['brand_logo']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["brand_logo"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/brands/'.time().'-'.$filename;
			  $filename = '/images/brands/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['brand_logo']['tmp_name'],$newname))) {

					$data = array('brand_logo'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_brand',"brand_id = '".$brand_id."'");

				} else {

				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["brand_logo"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}				

		$countProd = count($_POST['prod_name']);

		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_flashbox where flashbox_cat ='".$brand_id."'");

		for($x=0;$x<$countProd;$x++){

			/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
			if((!empty($_FILES["flashbox_image"]["name"][$x])) && ($_FILES['flashbox_image']['error'][$x] == 0)) {
				
				//Check if the file is JPEG image and it's size is less than 4mb

				$filename = basename($_FILES['flashbox_image']['name'][$x]);
				$ext = substr($filename, strrpos($filename, '.') + 1);

				if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["flashbox_image"]["size"][$x] < 4194304)) {

				//Determine the path to which we want to save this file
				  $time = time();
				  $newname = SITE_PATH.'images/brands/fb/'.$time.'-'.$filename;
				  $filename = '/images/brands/fb/'.$time.'-'.$filename;
				  //Check if the file with the same name is already exists on the server
				  if (!file_exists($newname)) {
					//Attempt to move the uploaded file to it's new place
					if ((move_uploaded_file($_FILES['flashbox_image']['tmp_name'][$x],$newname))) {

						$data = array(
							'flashbox_image'=>$filename,
							'flashbox_cat'=>$brand_id
						);
						$conn->insert($data,$maintable_prefix.'_flashbox',"");

					} else {

					   $msg .= "Error: A problem occurred during file upload!";
					}
				  } else {
					 $msg .= "Error: File ".$_FILES["flashbox_image"]["name"][$x]." already exists";
				  }
				} else {
				 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
				}
				
			} else {
				//no else
			}
		}
		
		$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$brand_name.' Edited.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?id=".$brand_id);	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?id=".$brand_id);	
	}
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){
		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_brand where brand_id ='".$id."'");
		echo '1';
	} else {
		echo '0';
	}

}