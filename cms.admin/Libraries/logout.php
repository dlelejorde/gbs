<?php
	session_start();
	include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');

	session_destroy();
	setcookie("tab", "", time()-3600);
	header("Location: ".SITE_ADMIN_DOMAIN."index.php");
?>