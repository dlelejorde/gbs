<?php 
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php'); 
$conn->checklog();
include(ADMIN_TEMPLATE_PATH.'header.php'); 


?>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/tinymce/jquery.tinymce.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/wysiwyg.js"></script>
<?php
$row_country = $conn->get_array_rs("select * from ".$maintable_prefix."_country");

$country = array();
if(isset($_GET['id'])){

    $id = $_GET['id'];

    if(trim($id)!=""){

        $row_content = $conn->array_rs_single("select * from ".$maintable_prefix."_our_stores WHERE os_id = '$id' ");

        $type = "Edit";      
    } else {
        $type = "Add";
    }

} else {
     $type = "Add";
}


?>
<div class="mainwrapper">
    
    <?php
        include(ADMIN_TEMPLATE_PATH.'mainHead.php');
        include(ADMIN_TEMPLATE_PATH.'navigation.php');
    ?>


    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="<?php echo SITE_ADMIN_DOMAIN; ?>dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Our Stores</li>
            
            <li class="right">

            </li>
        </ul>
        
        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <!--h5>Categories</h5-->
                <h1>Store[<?php echo $type; ?>]</h1>
            </div>
        </div><!--pageheader-->
        
        <div class="maincontent">
            <div class="maincontentinner">
            <?php echo $_SESSION['message']; ?>
            <div class="widgetbox box-inverse">
                <h4 class="widgettitle"><?php echo $type; ?> Store</h4>
                <div class="widgetcontent nopadding">
                    <form class="stdform stdform2" method="post" name="form1" action="<?php echo SITE_ADMIN_DOMAIN; ?>Tools/Our-Stores/process.php" enctype="multipart/form-data">
                        <input type="hidden" name="os_id" id="os_id" value="<?php echo $row_content['os_id'] ?>" />
                        <input type="hidden" name="actions" id="actions" value="<?php echo $type; ?>" />
                            <p>
                                <label>Country*</label>
                                <span class="field">
                                    <?php foreach ($row_country as $key => $value) { ?>
                                        <span class=""><input type="checkbox" name="os_country" id="os_country" style="opacity: 0;" value="<?php echo $value['country_id'] ?>" <?php if($value['country_id']==$row_content['os_country']){ echo 'checked="checked"'; } ?>></span><?php echo $value['country_name'] ?><br>
                                    <?php } ?>
                                </span>
                            </p>
                            <p>
                                <label>Name*</label>
                                <span class="field"><input type="text" name="os_name" id="os_name" class="input-xxlarge" value="<?php echo $row_content['os_name'] ?>" /></span>
                            </p>

                            <p>
                                <label>Address</label>
                                <span class="field">
                                    <textarea id="os_address" name="os_address" rows="15" cols="80" style="width: 80%" ><?php echo $row_content['os_address']; ?></textarea>
                                </span>
                            </p>
                            
                            <p>
                                <label>Text* <small>Promo main content.</small></label>
                                <span class="field">
                                    <textarea id="os_google_maps" name="os_google_maps" rows="15" cols="80" style="width: 80%" ><?php echo $row_content['os_google_maps'] ?></textarea>
                                </span>
                            </p>

                                                    
                            <p class="stdformbutton">
                                <input type="submit" class="btn btn-primary" value="Submit Button">
                                <!-- <button class="btn btn-primary">Submit Button</button> -->
                                <button type="reset" class="btn">Reset Button</button>
                            </p>
                    </form>
                </div><!--widgetcontent-->
            </div><!--widget-->                
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->

</div><!--mainwrapper-->
<?php include(ADMIN_TEMPLATE_PATH.'footer.php'); ?>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>