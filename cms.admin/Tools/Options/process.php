<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";


if($_POST['actions']=="tabs"){
	extract($_POST);

	for($x=1;$x<=5;$x++){

		//tab 1
		$data = array('option_name'=>htmlspecialchars($_POST['option_name'.$x],ENT_QUOTES),
			'option_value'=>htmlspecialchars($_POST['option_value'.$x],ENT_QUOTES),
			'option_edited_by'=>$admin,
			'option_date_edited'=>strtotime("now"));
		$conn->update_tbl($data,$maintable_prefix.'_options',"option_type = 'collage-".$x."' AND option_country = '".$ctry."'");		

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["option_image".$x])) && ($_FILES['option_image'.$x]['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['option_image'.$x]['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["option_image".$x]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/'.time().'-'.$filename;
			  $filename = '/images/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['option_image'.$x]['tmp_name'],$newname))) {

					$option_image = $filename;
					$data = array('option_image'=>$option_image);
					$conn->update_tbl($data,$maintable_prefix.'_options',"option_type = 'collage-".$x."' AND option_country = '".$ctry."'");

				} else {
				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["option_image".$x]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
		} else {

		}		


	}

	$_SESSION['message'] = '<h4 class="widgettitle title-success">Edited.<br />'.$msg.'</h4>';
	header("Location: tabsEdit.php?nav=options&ctry=".$ctry);	

	
}


if($_POST['actions']=="social"){
	extract($_POST);

	//facebook
	$data = array('option_link'=>$option_link1,
		'option_edited_by'=>$admin,
		'option_date_edited'=>strtotime("now"));
	$conn->update_tbl($data,$maintable_prefix.'_options',"option_type = 'facebook'");

	//twitter
	$data = array('option_link'=>$option_link2,
		'option_edited_by'=>$admin,
		'option_date_edited'=>strtotime("now"));
	$conn->update_tbl($data,$maintable_prefix.'_options',"option_type = 'twitter'");

	$_SESSION['message'] = '<h4 class="widgettitle title-success">Edited.</h4>';
	header("Location: linksEdit.php?nav=options");	
}

?>