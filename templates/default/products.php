            <section class="products">
                <div class="products-banner">
                    <div class="banner-container">
                        <img src="<?php echo TEMPLATE_DOMAIN_IMAGES; ?>products-banner.jpg" width="100%">
                    </div>
                    <div class="products-section-label">
                        <p><strong>HEALTH</strong></p>
                        <p>AND</p>
                        <p><strong>WELLNESS</strong></p>
                        <hr>
                    </div>
                </div>
                <div class="products-wrapper default-width">
                    <div class="product-item">
                        <div class="card stack">
                            <div class="figure">
                                <span>View</span>
                            </div>
                            <span class="bd">Product 1</span>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="card stack">
                            <div class="figure">
                                <span>View</span>
                            </div>
                            <span class="bd">Product 1</span>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="card stack">
                            <div class="figure">
                                <span>View</span>
                            </div>
                            <span class="bd">Product 1</span>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="card stack">
                            <div class="figure">
                                <span>View</span>
                            </div>
                            <span class="bd">Product 1</span>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="card stack">
                            <div class="figure">
                                <span>View</span>
                            </div>
                            <span class="bd">Product 1</span>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="card stack">
                            <div class="figure">
                                <span>View</span>
                            </div>
                            <span class="bd">Product 1</span>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="card stack">
                            <div class="figure">
                                <span>View</span>
                            </div>
                            <span class="bd">Product 1</span>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="card stack">
                            <div class="figure">
                                <span>View</span>
                            </div>
                            <span class="bd">Product 1</span>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="card stack">
                            <div class="figure">
                                <span>View</span>
                            </div>
                            <span class="bd">Product 1</span>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="card stack">
                            <div class="figure">
                                <span>View</span>
                            </div>
                            <span class="bd">Product 1</span>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="card stack">
                            <div class="figure">
                                <span>View</span>
                            </div>
                            <span class="bd">Product 1</span>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="card stack">
                            <div class="figure">
                                <span>View</span>
                            </div>
                            <span class="bd">Product 1</span>
                        </div>
                    </div>
                </div>

                <!-- <div class="opportunity-wrapper align-center">
                    <div class="default-width">
                        <p class="label">OPPORTUNITY</p>
                        <p class="small-line"></p>
                        <p class="content">
                            Whether you are just starting out in the world of business, Golden Business Solutions’ goal is to help you earn thru our product sales distribution, traditional business education partnership, get potential partners for longterm ventures, train you from ground to startup and most importantly in the future to market your own chosen business from wide variety of choices. You are here because you know this is the best time to start your own business and your right! Always remember, There is an entrepreneur inside every single one of us. Whether we like it or not, we are all capable of innovating, of revolutionizing whatever we put our minds to. We want to open that lock by giving you options from preparation to execution. Your Success will be our final product and legacy and every life we touch to financial independence is our Trophy.
                        </p>
                    </div>
                </div>
                <div class="products-wrapper align-center">
                    <div class="default-width">
                        <p class="label">PRODUCTS</p>
                        <p class="small-line"></p>
                        <div class="images-container">
                            <div class="product-image">Health & Wellness</div>
                            <div class="product-image">Beauty</div>
                            <div class="product-image">Home Essentials</div>
                        </div>
                    </div>
                </div>
                <div class="vision-wrapper">
                    <div class="default-width inline-container">
                        <div class="content-left left">

                        </div>
                        <div class="content-right left">
                            <p class="label">VISION</p>
                            <p class="content">
                                The Company's vision is to foster a society of economically independent individuals who are engaged citizens in their communities. In service of this vision, we focus our grant making and operations on three areas—education, product positioning/branding and entrepreneurship. A quality education is the foundation for self-sufficiency, preparing our clients for success and in life. In the future we will create balance in unemployment thru businesses started by entrepreneurs. We will create a Big Game Changer in the industry not only in Direct Sales but in providing jobs and wealth for society.
                            </p>
                        </div>

                        <div class="subscribe right">
                            <input class="join-us-btn" type="button" value="JOIN US NOW">
                        </div>
                    </div>
                </div> -->
            </section>
