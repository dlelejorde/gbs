<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";

if($_POST['actions']=="Add"){
	extract($_POST);

	if (trim($title)== ""){
		$msg .= "Name is required.<br />";
		$err++;
	}

	if (trim($cost)== ""){
		$msg .= "Cost is required.<br />";
		$err++;
	}


	if ($err==0){

		//insert shipping table
		$data = array('shipping_name'=>$title,
			'shipping_cost'=>$cost);
		$conn->insert($data,$maintable_prefix.'_shipping','');			

		$_SESSION['message'] = '<h4 class="widgettitle title-success">New shipping added. '.$title.'</h4>';
		header("Location: index.php?nav=options");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=options");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);


	if (trim($title)== ""){
		$msg .= "Name is required.<br />";
		$err++;
	}


	if ($err==0){


		//update flashbox table
		$data = array('shipping_name'=>$title,
			'shipping_cost'=>$cost);
		$conn->update_tbl($data,$maintable_prefix.'_shipping',"id = '".$id."'");		

		
		$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$title.' Edited.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=options&id=".$id);	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=options&id=".$id);	
	}
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){
		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_shipping where id ='".$id."'");
		echo '1';
	} else {
		echo '0';
	}

}