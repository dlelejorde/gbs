<?php 
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php'); 
$conn->checklog();
include(ADMIN_TEMPLATE_PATH.'header.php'); 

if(isset($_GET['id'])){

    $id = (int)$_GET['id'];

    if($id>0){

        $row_content = $conn->array_rs_single("select * from ".USERSTBL."  WHERE member_id = '$id'");

        $type = "Edit";      
    } else {
        $type = "Add";
    }

} else {
     $type = "Add";
}


?>
<div class="mainwrapper">
    
    <?php
        include(ADMIN_TEMPLATE_PATH.'mainHead.php');
        include(ADMIN_TEMPLATE_PATH.'navigation.php');
    ?>

    
    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="<?php echo SITE_ADMIN_DOMAIN; ?>dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Member</li>
            
            <li class="right">
                <a href="" data-toggle="dropdown" class="dropdown-toggle"><i class="icon-tint"></i> Color Skins</a>
                <ul class="dropdown-menu pull-right skin-color">
                    <li><a href="default">Default</a></li>
                    <li><a href="navyblue">Navy Blue</a></li>
                    <li><a href="palegreen">Pale Green</a></li>
                    <li><a href="red">Red</a></li>
                    <li><a href="green">Green</a></li>
                    <li><a href="brown">Brown</a></li>
                </ul>
            </li>
        </ul>
        
        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <h1>Member[<?php echo $type; ?>]</h1>
            </div>
        </div><!--pageheader-->
        
        <div class="maincontent">
            <div class="maincontentinner">
            <?php echo $_SESSION['message']; ?>

                <div class="row-fluid">
                    <div class="span8">
                        <form action="process.php" class="editprofileform" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="member_id" value="<?php echo $row_content['member_id']; ?>" />
                            <input type="hidden" name="actions" id="actions" value="<?php echo $type; ?>" />
                            <div class="widgetbox login-information">
                                <h4 class="widgettitle">Login Information</h4>
                                <div class="widgetcontent">
                                    <p>
                                        <label>Username:</label>
                                        <input type="text" name="username" class="input-xlarge" value="<?php echo $row_content['username']; ?>" />
                                    </p>
                                    <p>
                                        <label>Email:</label>
                                        <input type="text" name="email" class="input-xlarge" value="<?php echo $row_content['email']; ?>" />
                                    </p>
                                    <p>
                                        <label>Password:</label>
                                        <input type="text" name="password" class="input-xlarge" value="" />
                                    </p>
                                    <p>
                                        <label>Name:</label>
                                        <input type="text" name="name" class="input-xlarge" value="<?php echo $row_content['name']; ?>" />
                                    </p>
                                    <p>
                                        <label>Photo:</label>
                                        <input type="file" name="photos" class="input-xlarge" value="" />
                                        <?php 
                                            if($row_content['photo']!=""){
                                                echo '<img src="'.SITE_ADMIN_DOMAIN.$row_content['photo'].'" />';
                                            }
                                        ?>
                                    </p>
                                </div>
                            </div>
                        
                            
                            <p>
                                <?php if($type=="Add"){ ?>
                                <button type="submit" class="btn btn-primary">Add Member</button>
                                <?php } else { ?>
                                <button type="submit" class="btn btn-primary">Update Profile</button> &nbsp; <a href="">Deactivate this account</a>
                                <?php } ?>
                            </p>
                            
                        </form>
                    </div><!--span8-->
                </div><!--row-fluid-->              
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->
    
</div><!--mainwrapper-->
<?php include(ADMIN_TEMPLATE_PATH.'footer.php'); ?>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>