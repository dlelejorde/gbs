<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";

if($_POST['actions']=="Add"){
	extract($_POST);

	if (trim($photo_name)== ""){
		$msg .= "name is required.<br />";
		$err++;
	}


	/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
	if((!empty($_FILES["photo_url"])) && ($_FILES['photo_url']['error'] == 0)) {
		
		//Check if the file is JPEG image and it's size is less than 4mb

		$filename = basename($_FILES['photo_url']['name']);
		$ext = substr($filename, strrpos($filename, '.') + 1);

		if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["photo_url"]["size"] < 4194304)) {

		//Determine the path to which we want to save this file
		  $newname = SITE_PATH.'images/'.time().'-'.$filename;
		  $filename = '/images/'.time().'-'.$filename;
		  //Check if the file with the same name is already exists on the server
		  if (!file_exists($newname)) {
			//Attempt to move the uploaded file to it's new place
			if ((move_uploaded_file($_FILES['photo_url']['tmp_name'],$newname))) {

				$photo_url = $filename;

			} else {
			   $msg .= "Error: A problem occurred during file upload!";
			   $err++;
			}
		  } else {
			 $msg .= "Error: File ".$_FILES["photo_url"]["name"]." already exists";
			 $err++;
		  }
		} else {
		 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
		 $err++;
		}
		
	} else {
		$msg .= "Inage is required.<br />";
		$err++;
	}				

	/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
	if((!empty($_FILES["photo_thumbs"])) && ($_FILES['photo_thumbs']['error'] == 0)) {
		
		//Check if the file is JPEG image and it's size is less than 4mb

		$filename = basename($_FILES['photo_thumbs']['name']);
		$ext = substr($filename, strrpos($filename, '.') + 1);

		if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["photo_thumbs"]["size"] < 4194304)) {

		//Determine the path to which we want to save this file
		  $newname = SITE_PATH.'images/thumbs/'.time().'-'.$filename;
		  $filename = '/images/thumbs/'.time().'-'.$filename;
		  //Check if the file with the same name is already exists on the server
		  if (!file_exists($newname)) {
			//Attempt to move the uploaded file to it's new place
			if ((move_uploaded_file($_FILES['photo_thumbs']['tmp_name'],$newname))) {

				$photo_thumbs = $filename;

			} else {
			   $msg .= "Error: A problem occurred during file upload!";
			}
		  } else {
			 $msg .= "Error: File ".$_FILES["photo_thumbs"]["name"]." already exists";
		  }
		} else {
		 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
		}
		
	} else {
		$msg .= "";
	}				


	if ($err==0){

		//insert flashbox table
		$data = array('photo_name'=>htmlspecialchars($photo_name,ENT_QUOTES),
			'gallery_id'=>$gallery_id,
			'photo_status'=>$photo_status,
			'photo_order'=>$photo_order,
			'photo_url'=>$photo_url,
			'photo_thumbs'=>$photo_thumbs,
			'photo_added_by'=>$admin,
			'photo_date_added'=>strtotime("now"));
		$conn->insert($data,$maintable_prefix.'_photos','');		

		$_SESSION['message'] = '<h4 class="widgettitle title-success">New iamge added. '.$photo_name.'</h4>';
		header("Location: index.php?nav=gallery");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=gallery");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);


	if (trim($photo_name)== ""){
		$msg .= "Name is required.<br />";
		$err++;
	}


	if ($err==0){

		//update flashbox table
		$data = array('photo_name'=>htmlspecialchars($photo_name,ENT_QUOTES),
			'gallery_id'=>$gallery_id,
			'photo_status'=>$photo_status,
			'photo_order'=>$photo_order,
			'photo_edited_by'=>$admin,
			'photo_date_edited'=>strtotime("now"));
		$conn->update_tbl($data,$maintable_prefix.'_photos',"photo_id = '".$photo_id."'");		

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["photo_url"])) && ($_FILES['photo_url']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['photo_url']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["photo_url"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/'.time().'-'.$filename;
			  $filename = '/images/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['photo_url']['tmp_name'],$newname))) {

					$photo_url = $filename;
					$data = array('photo_url'=>$photo_url);
					$conn->update_tbl($data,$maintable_prefix.'_photos',"photo_id = '".$photo_id."'");

				} else {
				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["photo_url"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
		} else {

		}				

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["photo_thumbs"])) && ($_FILES['photo_thumbs']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['photo_thumbs']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["photo_thumbs"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/thumbs/'.time().'-'.$filename;
			  $filename = '/images/thumbs/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['photo_thumbs']['tmp_name'],$newname))) {

					$photo_thumbs = $filename;
					$data = array('photo_thumbs'=>$photo_thumbs);
					$conn->update_tbl($data,$maintable_prefix.'_photos',"photo_id = '".$photo_id."'");


				} else {
				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["photo_thumbs"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			$msg .= "";
		}				



		$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$flashbox_name.' Edited.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=gallery&id=".$photo_id);	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?nav=gallery&id=".$photo_id);	
	}
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){

		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_photos where photo_id ='".$id."'");
		echo '1';
	} else {
		echo '0';
	}

}