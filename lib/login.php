<?php 
require_once('../config/variables.php');

include_once "LoginClass.php";
$auth = new Login;

$username = $_POST['username'];
$password = $_POST['password'];
$pagereferer = $_POST['pagereferrer'];

session_start(); //require

$auth->checkLogin($username,$password);

if ($auth->haslogin()) {

	$pagereferer = $config['publicdomain'];
} else {
	$_SESSION['loginmsg'] = 'Username/password is incorrect!';
	$pagereferer = $config['publicdomain'];
}

header("Location: ".$pagereferer);