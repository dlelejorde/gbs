            <section class="padHead ">


                <div class="inner registration">
                    <div class="over pad">
                        <div class="result-title">Edit Project</div>
                        <div class="wiggle-black"></div>
                        
                        <form id="registerFrm2" class="form" enctype="multipart/form-data" method="post"action="<?php echo HOST; ?>api/register.php" target="results" data-type="projects">
                            <input type="hidden" name="type" value="projects">
                            <div class="regs projects">
                                <input type="hidden" name="organization_id" value="27580307152015">
                                <div class="rcol-half">
                                    <label>Project Name</label>
                                    <input type="text" class="txt-half" name="project_name" />
                                </div>
                                <div class="rcol-half">
                                    <label>Focus Area</label>
                                    <input type="text" class="txt-half" name="focus_area" />
                                </div> 
                                <div class="rcol-half">
                                    <label>Country</label>
                                    <input type="text" class="txt-half" name="country" />
                                </div>
                                <div class="rcol-half">
                                    <label>Term</label>
                                    <select name="term_from" class="term">
                                        <option value="">Year From...</option>
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                    </select>
                                    <select name="term_to" class="term">
                                        <option value="">Year To...</option>
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                    </select>
                                </div> 
                                <div class="rcol-wide">
                                    <label>&nbsp;</label>
                                    <div class="slider-container"> 
                                        <input type="hidden" id="min_value" name="value_from" value="">
                                        <input type="hidden" id="max_value" name="value_to" value="">
                                        <div id="slider-range"></div>
                                        <div class="value-label">
                                            <p class="fLeft">50$</p>
                                            <p class="fRight">100$</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="rcol-wide">
                                    <label>About the project</label>
                                    <textarea class="txt-area-full" name="about"></textarea>
                                </div>
                                <div class="rcol-wide">
                                    <label>Project Photo</label>
                                     <input type="file" name="photo" />
                                </div>
                                <div class="contact-section">
                                        <div class="rcol-half">
                                            <label>Contact Person</label>
                                            <input type="text" class="txt-half" name="contact_person" />
                                        </div>
                                        <div class="rcol-half">
                                            <label>Email Address</label>
                                            <input type="email" class="txt-half" name="contact_email" />
                                        </div>

                                        <div class="rcol-thrd">
                                            <label class="first">Position</label>
                                            <input type="text" class="txt-thrd txt-medium" name="position" />
                                        </div>

                                        <div class="rcol-thrd">
                                            <label>Phone Number</label>
                                            <input type="text" class="txt-thrd txt-medium" name="phone_number" />
                                        </div>

                                        <div class="rcol-thrd">
                                            <label>Website</label>
                                            <input type="text" class="txt-thrd txt-medium" name="website" />
                                        </div>
                                    </div>

                                    <input class="btn-submit" type="submit" value="PUBLISH" name="status">
                                    <input class="btn-submit" type="submit" value="SAVE AS DRAFT" name="status">
                                </div>
                            </div>
                        </form>

                        <iframe id="results" name="results" src="<?php echo HOST; ?>api/register.php" width="100%" height="0" style="display: none;"></iframe>
                    </div>


                </div>
            </section>