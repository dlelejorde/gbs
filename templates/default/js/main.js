$(document).ready(function() {

    $( "#slider-range" ).slider({
      range: true,
      min: 50,
      max: 100,
      values: [ 50, 100 ],
      slide: function( event, ui ) {
        $( "#min_value" ).val(ui.values[ 0 ]);
        $( "#max_value" ).val(ui.values[ 1 ]);
        $('.value_from-value').val(ui.values[ 0 ]);
        $('.value_to-value').val(ui.values[ 1 ]);
      }
    });

    $(document).on('click', '.main-menu-item, .menu a', function(){
      var data = $(this).data();

      $('.main-menu-item[status="active"]').removeAttr('status');

      $(this).attr('status', 'active');
      $(data.target).slideToggle();

      $(data.target).on('click', function(e){
    		e.stopPropagation();
    	});
    });

    var 
      tabs       = $('.tabs a'),
      tabContent = $('.tab-content');
  
    $(tabs).on('click', function(){
      var data  = $(this).data();
      var index = $(this).index();
      
      tabs.each(function(){ 
        $(this).removeClass('sel');
        $(tabContent).eq($(this).index()).removeClass('tab-content-active');
      });

      $(this).toggleClass('sel');
      $(data.target).toggleClass('tab-content-active');
    });

    var 
      menu     = $('.sub-menu'),
      menuItem = $('.sub-menu li');

    $(menuItem).on('click', function(){
      var 
        data       = $(this).data(),
        value      = $(this).html(),
        menuActive = $('.main-menu-item[status="active"]');

      $(data.target).val(value);
      $(data.value).val(data.id);
      $(menuActive).trigger('click');
      $(menuActive).attr('status', '');
    });

    var
      searchBox = $('#search');

    $('.form-search').on('submit', function(e){
      e.preventDefault();

      $.ajax({
        url: 'api/search.php',
        type: 'POST',
        data: $(this).serialize() + '&q=' + searchBox.val()
      })
      .done(function(response){
        $('.result-container').html(response);
      });
    });

    $('.search-btn').on('click', function(){
      $('.form-search').trigger('submit');
      return;
    });
});