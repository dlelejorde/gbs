<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo  $title; ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo TEMPLATE_DOMAIN_CSS; ?>normalize.css">
        <link rel="stylesheet" href="<?php echo TEMPLATE_DOMAIN_CSS; ?>main.css">
        <link href='https://fonts.googleapis.com/css?family=Asap:400,700' rel='stylesheet' type='text/css'>
        <!-- <link rel="stylesheet" href="<?php echo TEMPLATE_DOMAIN_CSS; ?>home.css"> -->
        <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <!-- <link rel="stylesheet" media="(min-width: 961px) and (max-width: 1160px)" rel="stylesheet" href="css/1160px.css">
        <link rel="stylesheet" media="(min-width: 730px) and (max-width: 960px)" rel="stylesheet" href="css/960px.css">
        <link rel="stylesheet" media="all and (max-width: 729px)" rel="stylesheet" href="css/320px.css"> -->
        <!--script src="js/vendor/modernizr-2.6.2.min.js"></script-->
    </head>
    <body>

        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <div class="wrapper" id="panel" >
            <?php include(TEMPLATE_PATH.'/header.php'); ?>

            <?php include(TEMPLATE_PATH.'/'.$contentTPL); ?>

            <?php include(TEMPLATE_PATH.'/footer.php'); ?>
        </div>
    </body>
</html>
