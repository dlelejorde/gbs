<?php
function string_limit($string, $limit){
		$chars_num = strlen($string);
		$string_array = str_split($string);
		$string_strip = "";
		for($x=0;$x<=$limit;$x++){
			if($x == $limit){
				if($limit < $chars_num){
					$string_strip = $string_strip . "...";
				}
			}
			else{
				$string_strip = $string_strip . $string_array[$x];
			}
		}
		return $string_strip;
	}
function word_limit($string, $limit){
	$string_ex = explode(" ", $string);
	$string_strip = "";
	for($x=0;$x<=$limit;$x++){
		$string_strip = $string_strip . $string_ex[$x].' ';
	}
	return $string_strip. "...";
}

function htmlentities2($myHTML) {
   $translation_table=get_html_translation_table (HTML_ENTITIES,ENT_QUOTES);
   $translation_table[chr(38)] = '&';
   return preg_replace("/&(?![A-Za-z]{0,4}\w{2,3};|#[0-9]{2,3};)/","&amp;" , strtr($myHTML, $translation_table));
}
function convert_illegalchars($str){
	$illegalchars = array("�","�","'","�","�","�",'"');
	$replacement = array("&#039;","&#039;","&#039;",'&#34;','&#34;','&#45;','&#34;');
 	$converted_string = str_replace($illegalchars, $replacement, $str);
	return $converted_string;
}

function illegalchars_decode($str){
	$illegalchars = array("�","�","'","�","�","�",'"');
	$replacement = array("&#039;","&#039;","&#039;",'&#34;','&#34;','&#45;','&#34;');
	$converted_string = str_replace($illegalchars, $replacement, $str);
	return $converted_string;
}


function makeUrlFriendly($s) {
	$s = convert_high_ascii($s);  ## convert high-ASCII chars to 7bit.
	$s = strtolower($s);           ## lower-case.
	$s = strip_tags($s);       ## remove HTML tags.
	$s = preg_replace('!&[^;\s]+;!','',$s);         ## remove HTML entities.
	$s = preg_replace('![^\w\s]!','',$s);           ## remove non-word/space chars.
	$s = preg_replace('!\s+!','-',$s);               ## change space chars to hyphens.
	return $s;
}

function convert_high_ascii($s) {
	$HighASCII = array(
		"!\xc0!" => 'A',    # A`
		"!\xe0!" => 'a',    # a`
		"!\xc1!" => 'A',    # A'
		"!\xe1!" => 'a',    # a'
		"!\xc2!" => 'A',    # A^
		"!\xe2!" => 'a',    # a^
		"!\xc4!" => 'Ae',   # A:
		"!\xe4!" => 'ae',   # a:
		"!\xc3!" => 'A',    # A~
		"!\xe3!" => 'a',    # a~
		"!\xc8!" => 'E',    # E`
		"!\xe8!" => 'e',    # e`
		"!\xc9!" => 'E',    # E'
		"!\xe9!" => 'e',    # e'
		"!\xca!" => 'E',    # E^
		"!\xea!" => 'e',    # e^
		"!\xcb!" => 'Ee',   # E:
		"!\xeb!" => 'ee',   # e:
		"!\xcc!" => 'I',    # I`
		"!\xec!" => 'i',    # i`
		"!\xcd!" => 'I',    # I'
		"!\xed!" => 'i',    # i'
		"!\xce!" => 'I',    # I^
		"!\xee!" => 'i',    # i^
		"!\xcf!" => 'Ie',   # I:
		"!\xef!" => 'ie',   # i:
		"!\xd2!" => 'O',    # O`
		"!\xf2!" => 'o',    # o`
		"!\xd3!" => 'O',    # O'
		"!\xf3!" => 'o',    # o'
		"!\xd4!" => 'O',    # O^
		"!\xf4!" => 'o',    # o^
		"!\xd6!" => 'Oe',   # O:
		"!\xf6!" => 'oe',   # o:
		"!\xd5!" => 'O',    # O~
		"!\xf5!" => 'o',    # o~
		"!\xd8!" => 'Oe',   # O/
		"!\xf8!" => 'oe',   # o/
		"!\xd9!" => 'U',    # U`
		"!\xf9!" => 'u',    # u`
		"!\xda!" => 'U',    # U'
		"!\xfa!" => 'u',    # u'
		"!\xdb!" => 'U',    # U^
		"!\xfb!" => 'u',    # u^
		"!\xdc!" => 'Ue',   # U:
		"!\xfc!" => 'ue',   # u:
		"!\xc7!" => 'C',    # ,C
		"!\xe7!" => 'c',    # ,c
		"!\xd1!" => 'N',    # N~
		"!\xf1!" => 'n',    # n~
		"!\xdf!" => 'ss'
	);
	$find = array_keys($HighASCII);
	$replace = array_values($HighASCII);
	$s = preg_replace($find,$replace,$s);
	return $s;
}


function allowedtags($articleText){
	$ttt = 0;
	$allowed_tags = '<b><i><em><strong><u><br>';
	$decodedBlurb = htmlspecialchars_decode($articleText);
	$pos = strpos($decodedBlurb, '<em>');
	if ($pos>0) {
		$ttt = 1;
	}
	$pos1 = strpos($decodedBlurb, '<b>');
	if ($pos1>0) {
		$ttt = 1;
	}
	$pos2 = strpos($decodedBlurb, '<i>');
	if ($pos2>0) {
		$ttt = 1;
	}
	$pos3 = strpos($decodedBlurb, '<strong>');
	if ($pos3>0) {
		$ttt = 1;
	}
	$pos4 = strpos($decodedBlurb, '<u>');
	if ($pos4>0) {
		$ttt = 1;
	}
	$pos5 = strpos($decodedBlurb, '<br>');
	if ($pos5>0) {
		$ttt = 1;
	}

	if ($ttt == 0) {
		$text = '<em>'.strip_tags($decodedBlurb).'</em>';
	} else {
		$text = strip_tags($decodedBlurb,$allowed_tags);
		//$rep1 = str_replace("<em>","</em--->",$decodedBlurb);
		//$rep1 = str_replace("</em>","<em>",$rep1);
		//$rep1 = str_replace("</em--->","</em>",$rep1);
		//$text = '<em>'.$rep1.'</em>';
	}
	return $text;
}

/*
Function: clean_variable
Author: Jose Isleta
*/
function clean_variable($var, $type = 0)
{
	  if (get_magic_quotes_gpc())
	  {
			$newvar = stripslashes($var);
	  }
	  else
	  {
			$newvar = $var;
	  }

	  if ($type == 1) //numeric (such as ID)
	  {
			$newvar = mysql_real_escape_string($newvar);
			$newvar = (int)$newvar;
			return $newvar;
	  }
	  elseif ($type == 2) //alphanum with dash (such as postname or slug)
	  {
			$newvar = preg_replace("/[^a-zA-Z0-9-_]/", "", $newvar);
			$newvar = strtolower($newvar);
			//return mysql_real_escape_string($newvar);
			return $newvar;
	  }
	  elseif ($type == 3) // block some MySQL parameter
	  {
			$sqlword = array("/scheme/i","/delete/i", "/update/i","/union/i","/insert/i","/drop/i","/http/i","/--/i");
			$newvar = preg_replace($sqlword, "", $newvar);
			//return mysql_real_escape_string($newvar);
			return $newvar;
	  }
	  else // simple... MySQL Real Escape String only
	  {
			//return mysql_real_escape_string($newvar);
			return $newvar;
	  }

}


function wordCut($text, $limit, $msg){
	if (strlen($text) > $limit){
		$txt1 = wordwrap($text, $limit, '[cut]');
		$txt2 = explode('[cut]', $txt1);
		$ourTxt = $txt2[0];
		$finalTxt = $ourTxt.$msg;
	}else{
		$finalTxt = $text;
	}
	return $finalTxt;
}

function takeOutCaptions($sentens){
	$stop = 0;
	$s = 0;

	while($stop == 0){
		if(strpos($sentens, "[caption") === false && strpos($sentens, "[/caption]") === false){
			$stop = 1;
		}else{
			$sentens1 = "";
			$sentens2 = "";

			if(strpos($sentens, "[caption")){
				$sentens1 = substr($sentens, 0, strpos($sentens, "[caption"));
			}

			if(strpos($sentens, "[/caption]")){
				$sentens2 = substr($sentens, strpos($sentens, "[/caption]") + 10);
			}

			$sentens = ($sentens1 != ""?$sentens1:"") . ($sentens2 != ""?$sentens2:"");
		}

		$s++;
	}

	return $sentens;
}

function CropSentence ($strText, $intLength, $strTrail = "..."){
	$wsCount = 0;
	$intTempSize = 0;
	$intTotalLen = 0;
	$intLength = $intLength - strlen($strTrail);
	$strTemp = "";

	if (strlen($strText) > $intLength) {
		$arrTemp = explode(" ", $strText);

		foreach ($arrTemp as $x) {
			if (strlen($strTemp) <= $intLength) $strTemp .= " " . $x;
		}

		$CropSentence = $strTemp . $strTrail;
	} else {
		$CropSentence = $strText;
	}

	return $CropSentence;
}

function csubstr($string, $start, $length=false) {
	$pattern = '/(\[\w+[^\]]*?\]|\[\/\w+\]|<\w+[^>]*?>|<\/\w+>)/i';
	$clean = preg_replace($pattern, chr(1), $string);

	if(!$length) $str = substr($clean, $start);
	else{
		$str = substr($clean, $start, $length);
		$str = substr($clean, $start, $length + substr_count($str, chr(1)));
	}

	$pattern = str_replace(chr(1),'(.*?)',preg_quote($clean));

	if(preg_match('/'.$pattern.'/is', $string, $matched)) return $matched[0];

	return $string;
}

function html_substr($posttext, $minimum_length, $length_offset) {
   // The approximate length you want the concatenated text to be
   $minimum_length = 200;
   // The variation in how long the text can be
   // in this example text length will be between 200-10=190 characters
   // and the character where the last tag ends
   $length_offset = 10;
   // Reset tag counter & quote checker
   $tag_counter = 0;
   $quotes_on = FALSE;
   // Check if the text is too long
   if (strlen($posttext) > $minimum_length) {
       // Reset the tag_counter and pass through (part of) the entire text
       for ($i = 0; $i < strlen($posttext); $i++) {
           // Load the current character and the next one
           // if the string has not arrived at the last character
           $current_char = substr($posttext,$i,1);
           if ($i < strlen($posttext) - 1) {
               $next_char = substr($posttext,$i + 1,1);
           }
           else {
               $next_char = "";
           }
           // First check if quotes are on
           if (!$quotes_on) {
               // Check if it's a tag
               // On a "<" add 3 if it's an opening tag (like <a href...)
               // or add only 1 if it's an ending tag (like </a>)
               if ($current_char == "<") {
                   if ($next_char == "/") {
                                       $tag_counter++;
                   }
                   else {
                       $tag_counter = $tag_counter + 3;
                   }
               }
               // Slash signifies an ending (like </a> or ... />)
               // substract 2
               if ($current_char == "/") $tag_counter = $tag_counter - 2;
               // On a ">" substract 1
               if ($current_char == ">") $tag_counter--;
               // If quotes are encountered, start ignoring the tags
               // (for directory slashes)
               if ($current_char == "\"") $quotes_on = TRUE;
           }
           else {
               // IF quotes are encountered again, turn it back off
               if ($current_char == "\"") $quotes_on = FALSE;
           }

           // Check if the counter has reached the minimum length yet,
           // then wait for the tag_counter to become 0, and chop the string there
           if ($i > $minimum_length - $length_offset && $tag_counter == 0) {
               $posttext = substr($posttext,0,$i + 1) . "...";
               return $posttext;
           }
       }
   }
             return $posttext;
}

function remove_tag($str, $remove)  // $remove can be scalar or array
{
    while ((strpos($str, '< ') !== false) || (strpos($str, '/ ') !== false)) {
        $str = str_replace(array('< ', '/ '), array('<', '/'), $str);
    }
    foreach ((array) $remove as $tag) {
        $search_arr = array('<'  . strtolower($tag), '<'  . strtoupper($tag),
                            '</' . strtolower($tag), '</' . strtoupper($tag));
        foreach ($search_arr as $search) {
            $start_pos = 0;
            while (($start_pos = strpos($str, $search, $start_pos)) !== false) {
                $end_pos = strpos($str, '>', $start_pos);
                $len = $end_pos - $start_pos + 1;
                $str = substr_replace($str, '', $start_pos, $len);
            }
        }
    }
    return $str;
}

/*function htmlentities2($myHTML) {
	$translation_table=get_html_translation_table (HTML_ENTITIES,ENT_QUOTES);
	$translation_table[chr(38)] = '&';

	return preg_replace("/&(?![A-Za-z]{0,4}\w{2,3};|#[0-9]{2,3};)/","&amp;" , strtr($myHTML, $translation_table));
}*/

function addPostSyntax(&$val,$key){
	$val = "'".htmlentities(strip_tags($_POST[$val]))."'";
}


function getAge($_dob){
	$dob = date("Y-m-d",strtotime($_dob));
	$ageparts = explode("-",$dob);
	$age = date("Y-m-d")-$dob;

	return (date("nd") < $ageparts[1].str_pad($ageparts[2],2,'0',STR_PAD_LEFT)) ? $age-=1 : $age;
}


function doMo2(){
	$monthlist = array("","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	$str .= "\t<select id=\"month\" name=\"month\" class=\"required\" >\n";
	$str .= "\t\t<option value=\"\"></option>\n";
	for($i=1;$i<=12;$i++){
		$str .= "\t\t<option value=\"".substr($i+100,1)."\" ";
		if ($selected == substr($i+100,1)){ $str .= "selected=\"selected\" ";}
		$str .= ">$monthlist[$i]</option>\n";
	}
	$str .= "\t</select>\n";
	return $str;
}

function doYr2(){
	$yr = date("Y");
	$str .= "\t<select id=\"year\" name=\"year\" class=\"required\" >\n";
	$str .= "\t\t<option value=\"\"></option>\n";
	for($i=$yr;$i>=($yr-30);$i--){
		$str .= "\t\t<option value=\"$i\" ";
		if ($selected == $i){ $str .= "selected=\"selected\" ";}
		$str .= ">$i</option>\n";
	}
	$str .= "\t</select>\n";
	return $str;
}

function getfilesize($size) {
	$units = array(' B', ' KB', ' MB', ' GB', ' TB');
	for ($i = 0; $size > 1024; $i++) { $size /= 1024; }
	return round($size, 2).$units[$i];
}


// new template functions

function cleanstr($str){
	$cleanstr = htmlspecialchars_decode(htmlentities(illegalchars_decode(html_entity_decode($str,ENT_QUOTES)),ENT_QUOTES));
	return $cleanstr;
}

function convertmkTime($time){
	return date("M. j,Y",$time);
}

function replaceAnd($string){
	return str_replace('and', '<span class="pink">&amp;</span>', $string);
}

function rand_string( $length ) {
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	$size = strlen( $chars );
	for( $i = 0; $i < $length; $i++ ) {
		$str .= $chars[ rand( 0, $size - 1 ) ];
	}

	return $str;
}

function getMembersData($user){
	$strUser = "SELECT member_name FROM ".DB_NAME.".".TABLE_PREFIX."_members
				WHERE member_id ='".$user."' LIMIT 1 ";
	$rowUser = Database::getValue($strUser);
	return $rowUser;
}

function generateCode($length){
	$code = '';
	for($i=0;$i<$length;$i++){
		$code .= substr("abcdefghijkmnopqrstuvwxyz023456789", rand(0,33) , 1);
	}
	return $code;
}

function getCountry($code){
	$strCtry = "SELECT name FROM ".DB_NAME.".".TABLE_PREFIX."countries
				WHERE code ='".$code."' LIMIT 1 ";
	$rowCtry = Database::getValue($strCtry);
	return $rowCtry;
}

function getFocusArea($id){
	$strFa = "SELECT fa_name FROM ".DB_NAME.".".TABLE_PREFIX."focus_area
				WHERE id ='".$id."' LIMIT 1 ";
	$rowFa = Database::getValue($strFa);
	return $rowFa;
}


/**
 * [createDropdown create a database driven dropdown]
 * @param  array  $data [must set before the function call]
 * @return [string]       [html select block]
 */
function createDropdown($data = array()){
    $str = '';
    $strCtry = "SELECT * FROM ".DB_NAME.".".$data['table'];
    $rowCtry = Database::getResult($strCtry);

    $str .= "\t<select id=\"".$data['id']."\" name=\"".$data['name']."\" class=\"".$data['class']."\" >\n";
    $str .= "\t\t<option value=\"\"></option>\n";
    foreach ($rowCtry as $key => $value) {
        $str .= "\t\t<option value=\"".$value[$data['value']]."\" ";
        if ($data['default'] == $value[$data['value']]){ $str .= "selected=\"selected\" ";}
        $str .= ">".$value[$data['label']]."</option>\n";
    }
    $str .= "\t</select>\n";
    return $str;
}


?>
