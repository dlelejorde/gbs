<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();

$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");
$err = 0;
$msg = "";

if($_POST['actions']=="Add"){
	extract($_POST);

	if (trim($press_title)== ""){
		$msg .= "Tittle is required.<br />";
		$err++;
	}

	if (trim($elm1)== ""){
		$msg .= "Text is required.<br />";
		$err++;
	}

	if ($err==0){
		//update products table
		$data = array('press_title'=>htmlspecialchars($press_title,ENT_QUOTES),
			'press_slug'=>makeUrlFriendly(strtolower($press_title)),
			'press_meta_description'=>$press_meta_description,
			'press_meta_keywords'=>$press_meta_keywords,
			'press_blurb'=>$press_blurb,
			'press_content'=>htmlspecialchars($elm1,ENT_QUOTES),
			'press_status'=>$press_status,
			'press_added_by'=>$admin,
			'press_date_added'=>strtotime("now"));
		$conn->insert($data,$maintable_prefix.'_press','');	
		$id = $conn->get_last_id($maintable_prefix.'_press','press_id');		

		foreach ($press_country as $key => $value) {
			$data = array('press_id'=>$id,
				'country_id'=>$value);
			$conn->insert($data,$maintable_prefix.'_press_country','');	
		}	

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["press_image"])) && ($_FILES['press_image']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['press_image']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["press_image"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/press/'.strtotime($date).'-'.$filename;
			  $filename = '/images/press/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['press_image']['tmp_name'],$newname))) {

					$data = array('press_image'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_press',"press_id = '".$id."'");

				} else {

				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["press_image"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}				

		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["press_thumbs"])) && ($_FILES['press_thumbs']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['press_thumbs']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["press_thumbs"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/press/'.time().'-'.$filename;
			  $filename = '/images/press/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['press_thumbs']['tmp_name'],$newname))) {

					$data = array('press_thumbs'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_press',"press_id = '".$id."'");

				} else {

				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["press_thumbs"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}		

		$_SESSION['message'] = '<h4 class="widgettitle title-success">New press added. '.$press_title.'<br />'.$msg.'</h4>';
		header("Location: index.php");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);

	if (trim($press_title)== ""){
		$msg .= "Tittle is required.<br />";
		$err++;
	}

	if (trim($elm1)== ""){
		$msg .= "Text is required.<br />";
		$err++;
	}

	if ($err==0){

		extract($_POST);
		//update products table 
		$data = array('press_title'=>htmlspecialchars($press_title,ENT_QUOTES),
			'press_slug'=>makeUrlFriendly(strtolower($press_title)),
			'press_meta_description'=>$press_meta_description,
			'press_meta_keywords'=>$press_meta_keywords,
			'press_blurb'=>$press_blurb,
			'press_content'=>htmlspecialchars($elm1,ENT_QUOTES),
			'press_status'=>$press_status,
			'press_edited_by'=>$admin,
			'press_date_edited'=>strtotime("now"));
		$conn->update_tbl($data,$maintable_prefix.'_press',"press_id = '".$press_id."'");	

		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_press_country where press_id ='".$press_id."'");

		foreach ($press_country as $key => $value) {
			$data = array('press_id'=>$press_id,
				'country_id'=>$value);
			$conn->insert($data,$maintable_prefix.'_press_country','');	
		}	
		
		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["press_image"])) && ($_FILES['press_image']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['press_image']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["press_image"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/press/'.time().'-'.$filename;
			  $filename = '/images/press/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['press_image']['tmp_name'],$newname))) {

					$data = array('press_image'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_press',"press_id = '".$press_id."'");

				} else {

				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["press_image"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}				


		/* upload checking && ($_FILES["filesend"]["type"] == "image/jpeg") */
		if((!empty($_FILES["press_thumbs"])) && ($_FILES['press_thumbs']['error'] == 0)) {
			
			//Check if the file is JPEG image and it's size is less than 4mb

			$filename = basename($_FILES['press_thumbs']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);

			if ((($ext == "jpg") || ($ext == "png") || ($ext == "gif") || ($ext == "JPG") || ($ext == "PNG") || ($ext == "GIF")) && ($_FILES["press_thumbs"]["size"] < 4194304)) {

			//Determine the path to which we want to save this file
			  $newname = SITE_PATH.'images/press/'.time().'-'.$filename;
			  $filename = '/images/press/'.time().'-'.$filename;
			  //Check if the file with the same name is already exists on the server
			  if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['press_thumbs']['tmp_name'],$newname))) {

					$data = array('press_thumbs'=>$filename);
					$conn->update_tbl($data,$maintable_prefix.'_press',"press_id = '".$press_id."'");

				} else {

				   $msg .= "Error: A problem occurred during file upload!";
				}
			  } else {
				 $msg .= "Error: File ".$_FILES["press_thumbs"]["name"]." already exists";
			  }
			} else {
			 $msg .=  "Error: Only .jpg, .png, .gif file extensions under 4mb are accepted for upload";
			}
			
		} else {
			//no else
		}	
		
		$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$press_title.' Edited.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?id=".$press_id);	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.<br />'.$msg.'</h4>';
		header("Location: addEdit.php?id=".$press_id);	
	}
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){
		$del_qry = $conn->execute_sql("delete from ".$maintable_prefix."_press where press_id ='".$id."'");
		echo '1';
	} else {
		echo '0';
	}

}