<?php 
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php'); 
$conn->checklog();
include(ADMIN_TEMPLATE_PATH.'header.php'); 
?>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/tinymce/jquery.tinymce.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/wysiwyg.js"></script>
<?php

$row_products = $conn->get_array_rs("select prod_id, prod_name from ".$maintable_prefix."_products");

if(isset($_GET['id'])){

    $id = $_GET['id'];

    if(trim($id)!=""){

        $row_content = $conn->array_rs_single("select * from ".$maintable_prefix."_featured_home_below WHERE id = '$id' ");

        $type = "Edit";      
    } else {
        $type = "Add";
    }

} else {
     $type = "Add";
}


?>
<div class="mainwrapper">
    
    <?php
        include(ADMIN_TEMPLATE_PATH.'mainHead.php');
        include(ADMIN_TEMPLATE_PATH.'navigation.php');
    ?>


    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="<?php echo SITE_ADMIN_DOMAIN; ?>dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Featured on Homepage(Below)</li>
            
            <li class="right">
            </li>
        </ul>
        
        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <!--h5>Categories</h5-->
                <h1>Featured on Homepage(Below) [<?php echo $type; ?>]</h1>
            </div>
        </div><!--pageheader-->
        
        <div class="maincontent">
            <div class="maincontentinner">
            <?php echo $_SESSION['message']; ?>
            <div class="widgetbox box-inverse">
                <h4 class="widgettitle"><?php echo $type; ?> Featured on Homepage(Below)</h4>
                <div class="widgetcontent nopadding">
                    <form class="stdform stdform2" method="post" name="form1" action="<?php echo SITE_ADMIN_DOMAIN; ?>Tools/Featured-Below/process.php" enctype="multipart/form-data">
                        <input type="hidden" name="id" id="id" value="<?php echo $row_content['id'] ?>" />
                        <input type="hidden" name="actions" id="actions" value="<?php echo $type; ?>" />
                            <p>
                                <label>Select Product*</label>
                                <span class="field"><select name="product_id" id="product_id" class="uniformselect">
                                    <option value="0">Choose One</option>
                                    <?php foreach ($row_products as $key => $value) { ?>
                                        <option value="<?php echo $value['prod_id']; ?>" <?php if($value['prod_id']==$row_content['product_id']){ echo 'selected="selected"'; } ?>><?php echo $value['prod_name']; ?></option>
                                    <?php } ?>
                                </select></span>
                            </p>
                            <p>
                                <label>Title*</label>
                                <span class="field"><input type="text" name="title" id="title" class="input-xxlarge" value="<?php echo $row_content['title'] ?>" /></span>
                            </p>
                            <p>
                                <label>Blurb*</label>
                                <span class="field"><input type="text" name="blurb" id="blurb" class="input-xxlarge" value="<?php echo $row_content['blurb'] ?>" /></span>
                            </p>

                            <p>
                                <label>Image *</label>
                                <span class="field">
                                <input type="file" name="image" class="input-xlarge" value="" />
                                <?php 
                                    if($row_content['image']!=""){
                                        echo '<img src="'.SITE_DOMAIN.$row_content['image'].'" />';
                                    }
                                ?>
                                </span>
                            </p>
                                                
                            <p class="stdformbutton">
                                <input type="submit" class="btn btn-primary" value="Submit Button">
                                <!-- <button class="btn btn-primary">Submit Button</button> -->
                                <button type="reset" class="btn">Reset Button</button>
                            </p>
                    </form>
                </div><!--widgetcontent-->
            </div><!--widget-->                
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->

</div><!--mainwrapper-->
<?php include(ADMIN_TEMPLATE_PATH.'footer.php'); ?>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>