                        <section class="padHead organization sectionLanding">

                <div class="fullInner">
                    <div class="orgImg"><img src="<?php echo TEMPLATE_DOMAIN_IMAGES; ?>landing-banner.jpg" /></div>
                    <div class="orgDetails">
                        <div class="fTitle">ORGANIZATION</div>
                        <p>
                            Holisticly target cross-platform web-readiness for user-centric leadership. Phosfluorescently orchestrate open-source growth strategies before highly efficient systems.
                        </p>
                    </div>

                </div>



                <div class="inner org">

                    <div class="padTB">

                        <div class="results organization">
                            <!--div class="result-title">Projects</div>
                            <div class="wiggle-black"></div-->

                            <div class="result-container">
                                <?php
                                    if(count($getOrg)>0){
                                        foreach ($getOrg as $key => $org) {
                                ?>
                                        <div class="result-list">
                                            <a href="<?php echo HOST.'organization/'.$org['organization_slug']; ?>" class="media">
                                                <div class="img"><img src="<?php echo HOST.$org['organization_photo'] ?>" width="128px" height="106px" /></div>
                                                <div class="bd">
                                                    <div class="org-name"><?php echo $org['organization_name'] ?></div>
                                                    <div class="org-details f16">
                                                        <span class="bold">Country:</span> <?php echo getCountry($org['country']) ?><br />
                                                        <span class="bold">Focus Area:</span> <?php echo getFocusArea($org['focus_area']) ?>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                <?php
                                        }
                                    }
                                ?>


                            </div>
                            <!--div class="result-pagination textCenter f16 bold">
                                <a href="#"><span>1</span></a>
                                <a href="#"><span>2</span></a>
                                <a href="#"><span>3</span></a>
                                <a href="#"><span>4</span></a>
                                <a href="#"><span>5</span></a>
                            </div-->
                        </div>

                    </div>

                </div>
            </section>
