<?php

function makeUrlFriendly($output) {
	$output = preg_replace('/\\s/', '_', $output); // Replace spaces with underscore, hyphen work around
	$output = preg_replace('/\\W/' , '', $output); // Remove non-word characters
	$output = strtolower(str_replace("_" , "-", $output));// finally replace underscores with hyphens and convert string to lower case
	return $output;
}

function getMembersData($id){
	global $conn;

	$row_content = $conn->array_rs_single("select * from tbl_members_info WHERE member_id = '$id'");
	return $row_content;
}

function getCategoryName($id){
	global $conn;

	$row_content = $conn->array_rs_single("select * from bs_surf_category WHERE cat_id = '$id'");

	return $row_content['cat_name'];
}

function getBrandName($id){
	global $conn;

	$row_content = $conn->array_rs_single("select * from bs_brand WHERE brand_id = '$id'");

	return $row_content['brand_name'];
}

function getCountryName($id){
	global $conn;

	$row_content = $conn->array_rs_single("select * from yfa_countries WHERE code = '$id'");

	return $row_content['name'];
}

function getBrandCategoryName($id){
	global $conn;

	$counter = $conn->count_records("select * from bs_category WHERE cat_id = '$id'");
	if($counter>0){
		$row_content = $conn->array_rs_single("select * from bs_category WHERE cat_id = '$id'");
		return $row_content['cat_name'];
	} else {
		return 'Parent';
	}
}

function getMemberData($id, $field){
	global $conn;

	$row_content = $conn->array_rs_single("select $field from yfa_organizations WHERE user_id = '$id'");

	return $row_content[$field];
}

function getTableSingleData($table, $column, $value, $field){
	global $conn;

	$row_content = $conn->array_rs_single("select $field from $table WHERE $column = '$value'");

	return $row_content[$field];
}


?>
