            <section class="padHead ">


                <div class="inner registration">
                    <div class="over pad">
                        <div class="result-title">Register</div>
                        <div class="wiggle-black"></div>

                        <div class="tabs">
                            <a href="javascript:void(0)" class="sel" data-target=".organztn">ORGANIZATION</a>
                        </div>
                        
                        <form id="registerFrm" class="form" enctype="multipart/form-data" method="post" action="<?php echo HOST; ?>api/register.php" target="results" data-target=".results" data-type="organization">
                            <input type="hidden" name="type" value="organization">
                            <div class="regs organztn tab-content tab-content-active">
                                <div class="rcol-half">
                                    <label>Name of Organization</label>
                                    <input type="text" class="txt-half" name="organization_name" />
                                </div>
                                <div class="rcol-half">
                                    <label>Year of establishment</label>
                                    <input type="text" class="txt-half" name="year_established" />
                                </div>

                                <div class="rcol-thrd">
                                    <label class="first">Country</label>
                                    <input type="text" class="txt-thrd first" name="country" />
                                </div>

                                <div class="rcol-thrd">
                                    <label class="second">Sector</label>
                                    <?php
                                        $data = array(
                                            'table' => TABLE_PREFIX."sector",
                                            'value' => 'id',
                                            'label' => 'sector_name',
                                            'default' => '',
                                            'name' => 'sector',
                                            'id' => 'sector',
                                            'class' => 'txt-thrd txt-small',
                                            );
                                        echo createDropdown($data);
                                    ?>
                                </div>

                                <div class="rcol-thrd">
                                    <label>Focus Area</label>
                                    <?php
                                        $data = array(
                                            'table' => TABLE_PREFIX."focus_area",
                                            'value' => 'id',
                                            'label' => 'fa_name',
                                            'default' => '',
                                            'name' => 'focus_area',
                                            'id' => 'focus_area',
                                            'class' => 'txt-thrd txt-large',
                                            );
                                        echo createDropdown($data);
                                    ?>
                                </div>

                                <div class="rcol-wide">
                                    <label>About the organization</label>
                                    <textarea class="txt-area-full" name="organization_description"></textarea>
                                </div>
                                
                                <div class="rcol-wide">
                                    <label>Mission and Vision</label>
                                    <textarea class="txt-area-full" name="mission_vision"></textarea>
                                </div>

                                <div class="rcol-wide">
                                    <label>More information</label>
                                    <textarea class="txt-area-full" name="more_information"></textarea>
                                </div>
                                <div class="contact-section">
                                    <div class="rcol-half">
                                        <label>Contact Person</label>
                                        <input type="text" class="txt-half" name="contact_person" />
                                    </div>
                                    <div class="rcol-half">
                                        <label>Email Address</label>
                                        <input type="text" class="txt-half" name="email" />
                                    </div>

                                     <div class="rcol-half">
                                        <label>Password</label>
                                        <input type="password" class="txt-half" name="password" />
                                    </div>
                                    <div class="rcol-half">
                                        <label>Confirm Password</label>
                                        <input type="password" class="txt-half" name="cpassword" />
                                    </div>

                                    <div class="rcol-thrd">
                                        <label class="first">Position</label>
                                        <input type="text" class="txt-thrd txt-medium" name="position" />
                                    </div>

                                    <div class="rcol-thrd">
                                        <label>Phone Number</label>
                                        <input type="text" class="txt-thrd txt-medium" name="phone_number" />
                                    </div>

                                    <div class="rcol-thrd">
                                        <label>Website</label>
                                        <input type="text" class="txt-thrd txt-medium" name="website" />
                                    </div>

                                    <div class="rcol-wide">
                                        <label>Upload Photo</label>
                                        <input type="file" class="txt-large" name="organization_photo" />
                                    </div>
                                </div>

                                <input class="btn-submit" type="submit" value="SUBMIT">
                            </div>
                        </form> 
                        <iframe id="results" name="results" src="<?php echo HOST; ?>api/register.php" width="100%" height="0" style="display: none;"></iframe>
                    </div>


                </div>
            </section>