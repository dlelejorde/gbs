<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();
$admin = $_SESSION['uID'];
$date = date("m-d-Y G:i:s");

if($_POST['actions']=="Add"){
	extract($_POST);
	if (trim($channels_name)!= ""){
	//update products table
	$data = array('channels_name'=>$channels_name,
			'channels_parent_id'=>$channels_parent_id,
			'channels_slug'=>makeUrlFriendly($channels_name),
			'channels_title'=>$channels_title,
			'channels_description'=>$channels_description,
			'channels_keywords'=>$channels_keywords,
			'channels_status'=>$channels_status,
			'channels_createdby'=>$admin,
			'channels_datemodified'=>strtotime($date));
		$conn->insert($data,'tbl_channels','');			
		$_SESSION['message'] = '<h4 class="widgettitle title-success">New category added. '.$channels_name.'</h4>';
		header("Location: index.php?nav=cat");	
	} else {
		$_SESSION['message'] = '<h4 class="widgettitle title-danger">Error: Fill up the required fields.</h4>';
		header("Location: addEdit.php?nav=cat");	
	}
	
}

if($_POST['actions']=="Edit"){
	extract($_POST);
	//update products table
	$data = array('channels_name'=>$channels_name,
			'channels_parent_id'=>$channels_parent_id,
			'channels_slug'=>makeUrlFriendly($channels_name),
			'channels_title'=>$channels_title,
			'channels_description'=>$channels_description,
			'channels_keywords'=>$channels_keywords,
			'channels_status'=>$channels_status,
			'channels_modifiedby'=>$admin,
			'channels_datemodified'=>strtotime($date));
	$conn->update_tbl($data,'tbl_channels',"channels_id = '".$channels_id."'");			
	$_SESSION['message'] = '<h4 class="widgettitle title-success">'.$channels_name.' Edited.</h4>';
	header("Location: index.php?nav=cat");		
}

if($_POST['action']=="delete"){

	$id = (int)$_POST['id'];

	if($id>0){
		$del_qry = $conn->execute_sql("delete from tbl_channels where channels_id ='".$id."'");
		echo '1';
	} else {
		echo '0';
	}

}