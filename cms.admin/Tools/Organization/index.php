<?php
include($_SERVER['DOCUMENT_ROOT'].'/cms.admin/config/variables.php');
$conn->checklog();
include(ADMIN_TEMPLATE_PATH.'header.php');

$row_content = $conn->get_array_rs("select * from ".$maintable_prefix."_user_account_info order by user_id desc");

?>
    <script type="text/javascript" src="<?php echo SITE_ADMIN_DOMAIN; ?>js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });

        });
    </script>
<div class="mainwrapper">

    <?php
        include(ADMIN_TEMPLATE_PATH.'mainHead.php');
        include(ADMIN_TEMPLATE_PATH.'navigation.php');
    ?>


    <div class="rightpanel">

        <ul class="breadcrumbs">
            <li><a href="<?php echo SITE_ADMIN_DOMAIN; ?>dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Organizations</li>

            <li class="right">
            </li>
        </ul>

        <div class="pageheader">
            <div class="pageicon"><span class="iconfa-book"></span></div>
            <div class="pagetitle">
                <h1>Organizations</h1>
            </div>
        </div><!--pageheader-->

        <div class="maincontent">
            <div class="maincontentinner">
                <?php echo $_SESSION['message']; ?>

                <div class="headtitle" style="margin-bottom: 0px;">
                    <h4 class="widgettitle">Records</h4>
                </div>
                <table id="dyntable" class="table table-bordered responsive">
                    <colgroup>
                        <col class="con0"/>
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                    </colgroup>
                    <thead>
                        <tr>
                            <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                            <th class="head1">Username</th>
                            <th class="head1">Email</th>
                            <th class="head1">Name</th>
                            <th class="head1">Confirmed</th>
                            <th class="head1">Profile</th>
                            <th class="head1 centeralign">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($row_content as $value) { ?>
                        <tr class="gradeX"  data="<?php echo $value['user_id']; ?>">
                          <td class="aligncenter"><span class="center"><input type="checkbox" /></span></td>
                            <td><?php echo $value['username']; ?></td>
                            <td><?php echo $value['email']; ?></td>
                            <td><?php echo getMemberData($value['user_id'], 'organization_name'); ?></td>
                            <td><?php echo ($value['confirmation_status']==1) ?  'Yes' : 'No'; ?></td>
                            <td><a href="view.php?id=<?php echo $value['user_id']; ?>">View</a></td>
                            <td class="centeralign"><a href="" class="deleterow" data="<?php echo $value['user_id']; ?>"><span class="icon-trash"></span></a></td>
                        </tr>
                        <?php } ?>

                    </tbody>
                </table>

                <br /><br />
                <br /><br />


            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
<script type="text/javascript">
    jQuery(document).ready(function(){
        // delete row in a table
        if(jQuery('.deleterow').length > 0) {
            jQuery('.deleterow').click(function(){
                var conf = confirm('Continue delete?');

            if(conf)
                    jQuery(this).parents('tr').fadeOut(function(){

                var id = jQuery(this).attr('data');
                var parameter = "id="+ id +"&action=delete";
                var url = "process.php";

                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: parameter,
                    success: function(originalRequest){
                        if(originalRequest=='1'){
                            jQuery(this).remove();
                        } else {
                            alert('Error: Record cannot be deleted!');
                        }
                    }
                });

            });
            return false;
        });
        }
    });
</script>
<?php include(ADMIN_TEMPLATE_PATH.'footer.php'); ?>
<?php include(ADMIN_LIBRARIES_PATH.'resetSession.php'); ?>
